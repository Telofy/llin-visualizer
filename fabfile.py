# -*- encoding: utf-8 -*-
from __future__ import (unicode_literals, absolute_import,
                        print_function, division)
from functools import partial
from fabric.api import env, run, cd
import fabric.api as fab


CONTEXTS = {
#    'prod': 'The production context (forthcoming)',
    'dev': 'The development/staging context',
}

SETTINGS = {
    '__doc__': 'The LLIN Visualizer server',
    '__defaults__': {
        'git_url': 'git@bitbucket.org:Telofy/llin-visualizer.git',
        'base_dir': '~/llin-visualizer',
    },
    'dev': {
        'hosts': [
            'telofy@claviger.net',
        ]
    },
}


# Validating the input

if env.tasks:
    assert len(env.tasks) > 1, (
        'Please specify context and action in that order.\n'
        'Supported contexts: ' + ', '.join(CONTEXTS.keys()))
    assert env.tasks[0] in CONTEXTS, (
        'Please specify the context as first task.\n'
        'Supported contexts: ' + ', '.join(CONTEXTS.keys()))


# Creating the context configuration tasks

def setcontext(context):
    setattr(env, 'context', context)
    settings = SETTINGS['__defaults__']
    settings.update(SETTINGS[context])
    for key, value in settings.items():
        setattr(env, key, value)

for context, description in CONTEXTS.items():
    task = partial(setcontext, context)
    task.__doc__ = description
    globals()[context] = fab.task(name=context)(task)


# Action tasks

@fab.task
def install(packages):
    run('sudo aptitude install {}'.format(packages))

@fab.task
def clone():
    run('git clone {} {}'.format(env.git_url, env.base_dir))

@fab.task
def pull():
    with cd(env.base_dir):
        run('git pull')

@fab.task
def bootstrap(args=''):
    with cd(env.base_dir):
        run('python2.7 -S bootstrap.py -c production.cfg {}'.format(args))

@fab.task
def buildout(args=''):
    with cd(env.base_dir):
        run('CONTEXT={} bin/buildout -c production.cfg {}' \
            .format(env.context, args))

@fab.task
def develop(args):
    with cd(env.base_dir):
        run('CONTEXT={context} bin/develop {args}'.format(context=env.context, args=args))

@fab.task(alias='sv')
def supervisorctl(cmd):
    with cd(env.base_dir):
        run('bin/supervisorctl ' + cmd)

@fab.task
def supervisord():
    with cd(env.base_dir):
        run('bin/supervisord')

@fab.task
def shutdown():
    with cd(env.base_dir):
        run('bin/supervisorctl shutdown')

@fab.task
def start(process='all'):
    with cd(env.base_dir):
        run('bin/supervisorctl start {}'.format(process))

@fab.task
def stop(process='all'):
    with cd(env.base_dir):
        run('bin/supervisorctl stop {}'.format(process))

@fab.task
def restart(process='all'):
    with cd(env.base_dir):
        run('bin/supervisorctl restart {}'.format(process))

@fab.task
def update():
    with cd(env.base_dir):
        run('bin/supervisorctl update')

@fab.task
def status():
    """Displays the Supervisor status"""
    with cd(env.base_dir):
        run('bin/supervisorctl status')

@fab.task
def stash():
    with cd(env.base_dir):
        run('git stash')

@fab.task
def unstash():
    with cd(env.base_dir):
        run('git stash pop')

@fab.task
def tail(process):
    with cd(env.base_dir):
        run('bin/supervisorctl tail {} stderr'.format(process))

@fab.task
def django(command):
    with cd(env.base_dir):
        run('bin/django {}'.format(command))
