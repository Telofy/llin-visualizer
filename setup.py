#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))
README = open(os.path.join(here, 'README.md')).read()


setup(
    name='llinvisualizer',
    version='0.0.1',
    description=(
        'LLIN Visualizer (formerly “Net Analytics”) visualizes data on '
        'distributions of long-lasting insecticide-treated mosquito nets '
        'on a map in the context of several important indicators.'),
    long_description=README,
    author='Denis Drescher',
    author_email='drescher@claviger.net',
    include_package_data=True,
    url='https://bitbucket.org/Telofy/llin-visualizer/',
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Framework :: Django',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
    install_requires=[
        'django-jinja',
        'django-flat-theme',
        'django_compressor',
        'Django',
        'psycopg2',
        'pytz',
        'unicodecsv',
        'utilofies',
        'sphinx',
        'requests',
        'django-geojson',
        'django-countries',
        'xmltodict',
        'beautifulsoup4',
    ],
    zip_safe=False,
    entry_points={
        'console_scripts': []
    }
)
