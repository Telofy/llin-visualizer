# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
from django.contrib import admin
from django.conf import settings
from django.contrib.auth import views as auth_views
from .analytics import urls as analytics_urls

admin.autodiscover()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'llinvisualizer.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', RedirectView.as_view(pattern_name='analytics-map-view')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^analytics/', include(analytics_urls)),
    url(r'^accounts/login/$',
        auth_views.login,
        {'template_name': 'registration/login.jinja'},
        name='login'),
    url(r'^accounts/logout/$',
        auth_views.logout,
        {'template_name': 'registration/logged_out.jinja'},
        name='logout'),
    url(r'^accounts/password_change/$',
        auth_views.password_change,
        {'template_name': 'registration/password_change_form.jinja'},
        name='password_change'),
    url(r'^accounts/password_change/done/$',
        auth_views.password_change_done,
        {'template_name': 'registration/password_change_done.jinja'},
        name='password_change_done'),
    url(r'^accounts/password_reset/$',
        auth_views.password_reset,
        {'template_name': 'registration/password_reset_form.jinja'},
        name='password_reset'),
    url(r'^accounts/password_reset/done/$',
        auth_views.password_reset_done,
        {'template_name': 'registration/password_reset_done.jinja'},
        name='password_reset_done'),
    url((r'^accounts/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/'
         r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$'),
        auth_views.password_reset_confirm,
        {'template_name': 'registration/password_reset_confirm.jinja'},
        name='password_reset_confirm'),
    url(r'^accounts/reset/done/$',
        auth_views.password_reset_complete,
        {'template_name': 'registration/password_reset_complete.jinja'},
        name='password_reset_complete'),
)


urlpatterns += patterns('',
    url(r'^{url}(?P<path>.*)'.format(url=settings.STATIC_URL[1:]),
        'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT}),
)
