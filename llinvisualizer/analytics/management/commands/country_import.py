# -*- encoding: utf-8 -*-
"""
Management command to import countries from a GeoJSON representation of the world
augmented with data from the django-countries library.
"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import logging
import re
import json
import unicodecsv
import requests
from copy import copy
from datetime import datetime, date
from django_countries import countries
from django.utils.translation import ugettext as _
from django.db import transaction
from django.core import management
from django.contrib.gis.geos import GEOSGeometry
from ... import models


URL = 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'


class Command(management.base.BaseCommand):
    help = ('Imports countries by joining an online GeoJSON source with data '
            'from the django-countries library')

    def add_arguments(self, parser):
        parser.add_argument('-f', '--file', help='Name of the input file')

    def handle(self, *args, **options):
        if options['file']:
            with open(options['file']) as jsonfile:
                country_geojson = json.load(jsonfile)
        else:
            response = requests.get(URL, timeout=10)
            country_geojson = response.json()
        shapes = {country['id']: GEOSGeometry(json.dumps(country['geometry']))
                  for country in country_geojson['features']}
        for code, name in countries:
            geom = shapes.get(countries.alpha3(code))
            models.Country.objects.update_or_create(
                name=name,
                defaults={'numeric': countries.numeric(code),
                          'alpha2': countries.alpha2(code),
                          'alpha3': countries.alpha3(code),
                          'geom': geom.wkt if geom else None})
