# -*- encoding: utf-8 -*-
"""
Management command to import CSV data on net gaps.

The net gap data have come in tables that can be converted to CSV files. A dedicated job imports data on nets needed, financed, and net gaps severally. The user needs to specify the type of data (out of those three types) and the year of the estimate, so that LLIN Visualizer can choose the newer estimate if there are several for a given year, country, and type.
"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import logging
import re
import unicodecsv
from copy import copy
from datetime import datetime, date
from django.utils.translation import ugettext as _
from django.db import transaction
from django.db.utils import IntegrityError
from django.core import management
from django.forms import ValidationError
from django.contrib.gis.geos import Point
from ... import models


logger = logging.getLogger(__name__)
countries = {country.name: country for country in models.Country.objects.all()}
synonyms = {'C.A.R.': countries['Central African Republic'],
            "Cote d'Ivoire": countries["Côte d'Ivoire"],
            'DRC': countries['Congo (the Democratic Republic of the)'],
            'Sudan S': countries['South Sudan'],
            'Zanzibar': None,
            'Guinea Bissau': countries['Guinea-Bissau']}
countries.update(synonyms)


class Command(management.base.BaseCommand):
    help = 'Imports CSV data on net gaps'

    def add_arguments(self, parser):
        parser.add_argument('file', help='Name of the input file')
        parser.add_argument('--date', default=date.today(),
                            help='Date in ISO 8601')
        parser.add_argument('--needed', action='store_true', default=False,
                            help='Data on nets needed.')
        parser.add_argument('--financed', action='store_true', default=False,
                            help='Data on nets financed.')
        parser.add_argument('--gap', action='store_true', default=False,
                            help='Data on net gaps.')

    def process(self, iterable, field, defaults):
        """
        Cleans and transforms the raw data to give it the right shape for the database.
        """
        for row in iterable:
            item = copy(defaults)
            item['country'] = countries[row['Country']]
            for year, count in [(year, count.replace(',', ''))
                                for year, count in row.items()
                                if year.isdecimal()]:
                yield dict(item, **{'year': year, field: count or None})

    @transaction.atomic()
    def atomic_writer(self, items):
        """
        Writes distributions to the database in one transaction
        to save any clean-up if the import fails.
        """
        unique_together = models.NetGap._meta.unique_together[0]
        for index, item in enumerate(items, 1):
            keys = {key: value for key, value in item.items()
                    if key in unique_together}
            netgap, created = models.NetGap.objects.update_or_create(
                defaults=item, **keys)
        return index

    def handle(self, *args, **options):
        defaults = {'estimate_date': options['date']}
        field = [key for key, value in options.items()
                 if value and key in ['needed', 'financed', 'gap']][0]
        with open(options['file']) as csvfile:
            reader = unicodecsv.DictReader(csvfile, encoding='utf-8', delimiter=b'\t')
            count = self.atomic_writer(self.process(reader, field, defaults))
            print('{} row(s) imported'.format(count))
