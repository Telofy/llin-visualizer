# -*- encoding: utf-8 -*-
"""
Management command to import distributions and distribution campaigns from
HTML and KMZ files that AMF provides.
"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import logging
import json
import re
import urlparse
import sys
import requests
import xmltodict
from datetime import date, datetime, timedelta
from itertools import groupby
from bs4 import BeautifulSoup
from django.db import transaction
from django.db.utils import IntegrityError
from django.core import management
from django.contrib.gis.geos import Point
from ...models import Country, Distribution, DistributionCampaign
from .survey_import import CSVParser

logger = logging.getLogger(__name__)
countries = Country.objects.all()


def parse_date(date_string):
    """
    Takes a string and returns a date range tuple.

    Dates in the source data come in three different formats:
        (1) an abbreviated month name and a two-digit year that counts the year of the
            September 11th attacks as year one,
        (2) two abbriviated months connected by a hyphen and a year in the above format,
        (3) two dates of in the first format with the first year number and the second
            month name connected by a hyphen.

    The function detects which format is used and tries to infer the longest possible
    period that the date or date range might designate.
    """

    date_parts = re.split('[- ]', date_string)
    if len(date_parts) == 2:  # e.g., Apr 07
        # Using the first and the last day of the month
        month, year = date_parts
        year = '20' + year  # Just to be sure
        start = datetime.strptime('{} 1, {}'.format(month, year), '%b %d, %Y').date()
        end = (start + timedelta(days=35)).replace(day=1) - timedelta(days=1)
    elif len(date_parts) == 3:
        # Using the first day of the start month and the last day of the end month
        start_month, end_month, year = date_parts
        year = '20' + year  # Just to be sure
        start = datetime.strptime('{} 1, {}'.format(start_month, year), '%b %d, %Y').date()
        end = datetime.strptime('{} 1, {}'.format(end_month, year), '%b %d, %Y').date()
        end = (end + timedelta(days=35)).replace(day=1) - timedelta(days=1)
    elif len(date_parts) == 4:
        # Using the first day of the start month and the last day of the end month
        start_month, start_year, end_month, end_year = date_parts
        start_year, end_year = '20' + start_year, '20' + end_year  # Just to be sure
        start = datetime.strptime('{} 1, {}'.format(start_month, start_year), '%b %d, %Y').date()
        end = datetime.strptime('{} 1, {}'.format(end_month, end_year), '%b %d, %Y').date()
        end = (end + timedelta(days=35)).replace(day=1) - timedelta(days=1)
    return start, end


def find_country(country_name):
    """
    Given a name of a country, returns the country object.

    Countries can have many nick names. This function maps known nick names
    to their standardized names and looks up the corresponding database object.
    """
    country_name = {
        'Congo (Dem. Rep.)': 'Congo (the Democratic Republic of the)'
    }.get(country_name, country_name)
    for country in countries:
        if country.name == country_name:
            return country
    logger.warn('Country not found %r', country_name)


class CampaignImporter(object):
    """
    Imports distribution campaigns based on the list curated by the
    Against Malaria Foundation.
    """

    url = 'https://www.againstmalaria.com/Distributions.aspx'

    def __init__(self, filename=None):
        if filename:
            with open(filename) as infile:
                self.content = infile.read()
        else:
            # Does not work, because we’d have to post some huge “view state.”
            # Warglbargl. Can someone please reimplement AMF’s website?
            response = requests.post(self.url, timeout=10)
            self.content = response.text

    def parse(self):
        """
        Yields dictionaries representing distribution campaigns extracted
        from the website of the Against Malaria Foundation.
        """
        soup = BeautifulSoup(self.content, 'html.parser')
        for row in soup('tr', class_='TableItemText'):
            net_count, name, _, country, date, _, partner = \
                [value.text.strip() for value in row('td')[0:7]]
            amf_id = int(row('td')[1]('a')[0].attrs['href'].rsplit('=', 1)[1])
            url = 'http://www.againstmalaria.com/Distribution.aspx?ProposalID={}'.format(amf_id)
            yield {'name': name, 'url': url, 'amf_id': amf_id}

    @transaction.atomic()
    def atomic_writer(self, items):
        """
        Writes distribution campaigns to the database in one transaction
        to save any clean-up if the import fails.
        """
        imported = updated = 0
        for item in items:
            campaign, created = DistributionCampaign.objects.update_or_create(
                amf_id=item['amf_id'], defaults=item)
            imported += int(created)
            updated += int(not created)
        return imported, updated


class DistributionImporter(object):
    """
    Imports individual distributions from the KML that the Against Malaria Foundation
    provides on its website.
    """

    url = ('https://www.againstmalaria.com/Map_Distributionlist_GoogleEarth.aspx'
           '?MapID=1&PartnerID=0&StatusID=0&CultureID=1')

    def __init__(self, filename=None):
        if filename:
            with open(filename) as infile:
                self.content = infile.read()
        else:
            response = requests.post(self.url, timeout=10)
            self.content = response.text

    def parse(self):
        """
        Yields dictionaries representing distributions as they are extracted from
        the XML data.
        """
        kml = xmltodict.parse(self.content)
        for folder in kml['kml']['Folder']['Folder']:
            country_name = folder['name']
            country = find_country(country_name)
            if isinstance(folder['Placemark'], dict):
                # If there’s only one, the library doesn’t create a list of course,
                # but this way the code looks cleaner
                folder['Placemark'] = [folder['Placemark']]
            for placemark in folder['Placemark']:
                item = {}
                item['country'] = country
                item['name'] = placemark['name']
                coordinates = placemark['Point']['coordinates']
                longitude, latitude = map(float, coordinates.split(',')[:2])
                item['geom'] = Point(longitude, latitude)
                description = placemark['description']
                matches = {
                    'partner': re.search(r'Distributed by: ([^<]+)', description),
                    'net_count': re.search(r'Number of nets: ([^<]+)', description),
                    'image': re.search(r"([^']*/images/../../small/\d+.jpg)", description),
                    'video': re.search(r'([^"]*youtube.com[^"]*)', description),
                    'status': re.search(r'Status: ([^<]+)', description),
                    'url': re.search(r"([^']*DistributionID=[^']*)", description),
                    'date': re.search(r'When: ([^<]+)', description),
                }
                for key, match in matches.items():
                    if match:
                        item[key] = match.group(1)
                    elif key not in ('image', 'video'):
                        print('No match for {!r} in: {}'.format(key, description))
                amf_ids = dict(urlparse.parse_qsl(urlparse.urlparse(item['url']).query))
                item['amf_id'] = int(amf_ids['DistributionID'])
                item['proposal_id'] = int(amf_ids['ProposalID'])
                item['net_count'] = int(item['net_count'])
                item['start'], item['end'] = parse_date(item['date'])
                del item['date']
                yield item

    @transaction.atomic()
    def atomic_writer(self, items):
        """
        Writes distributions to the database in one transaction
        to save any clean-up if the import fails.
        """
        imported = updated = 0
        for item in items:
            proposal_id = item['proposal_id']
            del item['proposal_id']
            item['campaign'] = DistributionCampaign.objects.get(amf_id=proposal_id)
            campaign, created = Distribution.objects.update_or_create(
                amf_id=item['amf_id'], defaults=item)
            imported += int(created)
            updated += int(not created)
        return imported, updated


class Command(management.base.BaseCommand):
    help = ('Import distributions and distribution campaigns from HTML and '
            'KMZ files that AMF provides')

    def add_arguments(self, parser):
        parser.add_argument('--kmz', help='Read KMZ from file rather than URL')
        parser.add_argument('--html', help='Read HTML from file rather than URL')

    def handle(self, *args, **options):
        if not options['html']:
            raise NotImplementedError(re.sub(' +', ' ',
                """
                Apologies, but this is not currently implemented. Please open

                    https://www.againstmalaria.com/Distributions.aspx

                in your browser of choice, click “Show all distributions,”
                save the page to a file, and specify it using the --html parameter.

                Thank you.
                """))
        # Distribution campaigns
        campaign_importer = CampaignImporter(options['html'])
        campaigns = campaign_importer.parse()
        imported, updated = campaign_importer.atomic_writer(campaigns)
        logger.info('%s campaigns imported, %s updated.', imported, updated)
        # Distributions
        distribution_importer = DistributionImporter(options['kmz'])
        distributions = distribution_importer.parse()
        imported, updated = distribution_importer.atomic_writer(distributions)
        logger.info('%s distributions imported, %s updated.', imported, updated)
