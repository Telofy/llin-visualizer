# -*- encoding: utf-8 -*-
"""
Management command to import survey results.

Some considerations: First, it needs to be flexible. The data AMF has provided up to this point
have come in two different formats, but there is no limit to the number of different raw data
formats LLIN Visualizer needs to be able to handle. Hence, a CSV parser class provides a generic
interface to all the cleaning methods I have written while individual CSV profiles inherit these
and determine to which fields they need to be applied. At the same time they provide the
flexibility of mapping fields of the input data to arbitrary database fields, not only one to
one, but also by merging fields or splitting them up.

Second, the cleaning of the data requires flexibility too. Each resulting field needs to have
custom cleaning routines associated with it but there also need to be higher-level cleaning
routines that can decide whether the data from several fields in combination make sense or
whether the whole row is invalid. Due to some intricacies of the order in which type conversions
are applied, I could not draw on the form validation tools that Django provides, the key
difference being that these escalate field-level errors to the user to correct or re-enter the
data, something that is either impossible or the job of the cleaning routines in our case.

Many of the cleaning routines preform type conversions but there are a few more sophisticated
operations involved as well. The date parsing draws on contextual data from the AMF website to
sanity-check the dates and discard them if them seem erroneous. Many cleaning routines apply
translations that convert fields with values of “oui” and “non” to Boolean types, and normalize
various strings to key values that are standardized within LLIN Visualizer. Finally, names of
locations are normalized using a sequence of heuristics that are optimized to merge many
different spellings of the same name while introducing few errors.

The module allows the administrator fine-grained control over its behavior by specifying the
parser and distribution to be used, specifying some properties of the CSV file, and specifying
the mode in which the database writer should operate. There is also a mode that allows for the
testing of new parser profiles.
"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import logging
import re
import unicodecsv
from datetime import datetime
from django.utils.translation import ugettext as _
from django.db import transaction
from django.db.utils import IntegrityError
from django.core import management
from django.forms import ValidationError
from django.contrib.gis.geos import Point
from ... import models


logger = logging.getLogger(__name__)


class CSVParser(dict):
    """
    The base class for the CSV cleaning profiles.

    Note: We’re not using a Django ModelForm because it calls to_python() on each
    field very early in the process, prior to the cleaning, and at that point
    we would need to have cleaned the field already.
    """

    fields = [field.name.decode('utf-8')
              for field in models.SurveyResponse._meta.fields
              if field.name not in ('id', 'household', 'distribution', 'cluster', 'random')]

    def _fieldmap(self, name):
        return name

    def __getattr__(self, name):
        if name.startswith('clean_'):
            return lambda: self.data[self._fieldmap(name[6:])]
        return self.__getattribute__(name)

    def __init__(self, data):
        self.data = data
        self._process()
        self.error = self._clean()
        self.is_valid = not bool(self.error)

    def _clean(self):
        """
        Cleans the data as a whole and tests its validity.

        The individual clean_*() methods should not raise ValidationError
        because unlike with forms we can’t tell the user to re-enter proper
        data. Rather the clean_*() methods should drop invalid values.

        While individual fields should not raise ValidationError, the clean()
        method should, when it encounters a whole row that is invalid, e.g.,
        because it is essentially empty. This cannot be determined on field
        level.

        clean() is called after the individual clean_*() methods so it has
        access to the cleaned values.
        """
        try:
            self.clean()
        except ValidationError as excp:
            return excp

    def clean(self):
        pass

    def _process(self):
        """
        Performs the actual parsing.

        We leave it to the methods to get their uncleaned data
        so that fields like location can be assembled from several
        sources, none of which is named “location.”
        """
        for field in self.fields:
            self[field] = getattr(self, 'clean_' + field)()


class IMA2014a(CSVParser):
    # pylint: disable=missing-docstring

    unique_together = models.SurveyResponse._meta.unique_together[0]
    unique_together = [field for field in unique_together
                       if field != 'distribution']

    fieldnames = [
        'welcome_screen',
        'latitude',
        'longitude',
        'altitude',
        'accuracy',
        'datetime',
        'phone_id',
        'province_name',
        'district_name',
        'health_zone_name',
        'health_area_name',
        'village_name',
        'day_of_distribution',
        'house_number',
        'name',
        'phone',
        'legal_script',
        'signature',
        'age',
        'sex',
        'people_in_household',
        'pregnant_women',
        'children_under_five',
        'malaria_transmission',
        'malaria_other_transmission',
        'malaria_symptoms',
        'malaria_other_symptoms',
        'malaria_prevention',
        'malaria_treatment',
        'malaria_other_treatment',
        'at_least_one_net',
        'at_least_one_llin',
        'llins',
        'net_source',
        'other_net_source',
        'people_under_net_last_night',
        'pregnant_women_under_net_last_night',
        'fansidar_during_pregnancy',
        'children_under_net_last_night',
        'preventive_measures',
        'other_preventive_measures',
        'child_fever_last_two_weeks',
        'child_benefit_from_drug',
        'child_medicine',
        'child_healthcenter',
        'sleeping_spaces',
        'llins_good',
        'llins_total',
        'summary',
        'llins_installed',
        'llins_brand',
        'llins_returned',
        'llins_packaging_returned',
        'llins_photo',
        'start',
        'end',
        'device_id',
        'instance_id',]

    malaria_symptoms_mapping = {
        1: 'fever',
        2: 'headache',
        3: 'pain',
        4: 'tiredness',
        5: 'vomiting',
        6: 'no_appetite',
        7: 'diarrhea',
        8: 'other',
        9: 'na',
    }

    malaria_transmission_mapping = {
        1: 'mosquito',
        2: 'unsafe_water',
        3: 'sun',
        4: 'dirt',
        5: 'witchcraft',
        6: 'fruit',
        7: 'sick_people',
        8: 'other',
        9: 'na',
    }

    malaria_prevention_mapping = {
        1: 'pipeline',
        2: 'weed',
        3: 'llin',
        4: 'contraceptive',
        5: 'herb',
        6: 'drug',
        7: 'insecticide',
        8: 'other',
        9: 'na',
    }

    malaria_treatment_mapping = {
        'Quinine': 'quinine',
        'Amodiaquine': 'amodiaquine',
        'autre': 'other',
        'Fansidar': 'fansidar',
        'Artemisinin': 'artemisinin',
        'chloroquine': 'chloroquine',
        'Paracetamol': 'paracetamol',
        'Aspirine_Ibuprofen': 'aspirine_ibuprofen',
    }

    preventive_measures_mapping = {
        1: 'water',
        2: 'lotion',
        3: 'spraying',
        4: 'mowing',
        5: 'cleaning',
        6: 'covering',
        7: 'burning',
        8: 'kidney',
        9: 'na',
        10: 'other',
    }

    net_sources_mapping = {
        'marche': 'market',
        'centre_sante': 'health_center',
        'campagne_de_distro': 'distribution',
        'ONG': 'ngo',
        'autres': 'other',
    }

    # Values must correspond to known keys
    assert set(malaria_symptoms_mapping.values()) <= set(models.MALARIA_SYMPTOMS.keys())
    assert set(malaria_transmission_mapping.values()) <= set(models.MALARIA_TRANSMISSION.keys())
    assert set(malaria_prevention_mapping.values()) <= set(models.MALARIA_PREVENTION.keys())
    assert set(malaria_treatment_mapping.values()) <= set(models.MALARIA_TREATMENT.keys())
    assert set(preventive_measures_mapping.values()) <= set(models.PREVENTIVE_MEASURES.keys())
    assert set(net_sources_mapping.values()) <= set(models.NET_SOURCES.keys())

    @staticmethod
    def _to_int(value):
        """Converts a value to integer discarding noise."""
        value = re.search('[0-9]*', value).group(0)
        if value:
            return int(value)

    @staticmethod
    def _map_to_int(value):
        return [int(item) for item in value.split(' ')
                if item not in ('pas_connaitre', '')]

    @staticmethod
    def _to_bool(value):
        return {'oui': True, 'non': False}.get(value.lower())

    @staticmethod
    def _to_datetime(value, default=None):
        try:
            date = datetime.strptime(value, '%d %b %Y %H:%M')
        except ValueError:
            date = None
        if date and date > datetime(2014, 1, 1) and date < datetime.now():
            return date
        return default

    @staticmethod
    def _normalize_string(value):
        value = value.lower()
        value = value.replace('_', '-')
        value = re.sub(r'(?<=[^0-9 -])([0-9]+)', r' \1', value)
        value = re.sub(r'\.', r'. ', value)
        value = re.sub(r'\s+', r' ', value)
        value = value.strip()
        value = re.sub(r'(?<=[ -])([a-z])', lambda match: match.group(1).upper(), value)
        value = re.sub(r'^([a-z])', lambda match: match.group(1).upper(), value)
        return value

    def clean_llins_total(self):
        return self._to_int(self.data['llins_total'])

    def clean_sleeping_spaces(self):
        return self._to_int(self.data['sleeping_spaces'])

    def clean_phone_id(self):
        return self._to_int(self.data['phone_id'])

    def clean_llins_packaging_returned(self):
        return self._to_int(self.data['llins_packaging_returned'])

    def clean_children_under_five(self):
        return self._to_int(self.data['children_under_five'])

    def clean_llins_installed(self):
        return self._to_int(self.data['llins_installed'])

    def clean_people_in_household(self):
        return self._to_int(self.data['people_in_household'])

    def clean_pregnant_women(self):
        return self._to_int(self.data['pregnant_women'])

    def clean_llins_good(self):
        return self._to_int(self.data['llins_good'])

    def clean_llins(self):
        return self._to_int(self.data['llins'])

    def clean_llins_returned(self):
        return self._to_int(self.data['llins_returned'])

    def clean_age(self):
        return self._to_int(self.data['age'])

    def clean_fansidar_during_pregnancy(self):
        return self._to_int(self.data['fansidar_during_pregnancy'])

    def clean_pregnant_women_under_net_last_night(self):
        return self._to_int(self.data['pregnant_women_under_net_last_night'])

    def clean_people_under_net_last_night(self):
        return self._to_int(self.data['people_under_net_last_night'])

    def clean_children_under_net_last_night(self):
        return self._to_int(self.data['children_under_net_last_night'])

    def clean_phone(self):
        value = self.data['phone']
        if re.match('^9+$', value):
            return ''
        return value

    def clean_province_name(self):
        value = self.data['province_name']
        value = self._normalize_string(value)
        return {'kasai': 'Kasaï'}.get(value.lower(), value)

    def clean_health_area_name(self):
        value = self.data['health_area_name']
        value = self._normalize_string(value)
        return value

    def clean_health_zone_name(self):
        value = self.data['health_zone_name']
        value = self._normalize_string(value)
        return value

    def clean_district_name(self):
        value = self.data['district_name']
        value = self._normalize_string(value)
        return value

    def clean_village_name(self):
        value = self.data['village_name']
        value = self._normalize_string(value)
        return value

    def clean_geom(self):
        longitude, latitude, altitude = \
            self.clean_longitude(), self.clean_latitude(), self.clean_altitude()
        if longitude and latitude and altitude:
            return Point(longitude, latitude, altitude)

    def clean_accuracy(self):
        accuracy = self.data['accuracy']
        return float(accuracy) if accuracy else None

    def clean_longitude(self):
        longitude = self.data['longitude']
        return float(longitude) if longitude else None

    def clean_latitude(self):
        latitude = self.data['latitude']
        return float(latitude) if latitude else None

    def clean_altitude(self):
        altitude = self.data['altitude']
        return float(altitude) if altitude else None

    def clean_legal_script(self):
        return self._to_bool(self.data['legal_script'])

    def clean_at_least_one_net(self):
        return self._to_bool(self.data['at_least_one_net'])

    def clean_at_least_one_llin(self):
        return self._to_bool(self.data['at_least_one_llin'])

    def clean_child_benefit_from_drug(self):
        return self._to_bool(self.data['child_benefit_from_drug'])

    def clean_child_healthcenter(self):
        return self._to_bool(self.data['child_healthcenter'])

    def clean_child_fever_last_two_weeks(self):
        return self._to_bool(self.data['child_fever_last_two_weeks'])

    def clean_day_of_distribution(self):
        value = self.data['day_of_distribution']
        value = value.lower().replace('i', '1')
        value = re.search('[0-9]*', value).group(0)
        if value:
            return int(value)

    def clean_datetime(self):
        return self._to_datetime(self.data['datetime'], default=datetime.min)

    def clean_start(self):
        return self._to_datetime(self.data['start'])

    def clean_end(self):
        return self._to_datetime(self.data['end'])

    def clean_sex(self):
        value = self.data['sex']
        return {'homme': 'male', 'femme': 'female'}.get(value.lower(), '')

    def clean_malaria_symptoms(self):
        values = self._map_to_int(self.data['malaria_symptoms'])
        return map(self.malaria_symptoms_mapping.get, values)

    def clean_malaria_transmission(self):
        values = self._map_to_int(self.data['malaria_transmission'])
        return map(self.malaria_transmission_mapping.get, values)

    def clean_malaria_prevention(self):
        values = self._map_to_int(self.data['malaria_prevention'])
        return map(self.malaria_prevention_mapping.get, values)

    def clean_malaria_treatment(self):
        values = self.data['malaria_treatment'].split(' ')
        return map(self.malaria_treatment_mapping.get, values)

    def clean_preventive_measures(self):
        values = self._map_to_int(self.data['preventive_measures'])
        return map(self.preventive_measures_mapping.get, values)

    def clean(self):
        if not all(self[field] for field in self.unique_together):
            raise ValidationError(
                _('Invalid row: %(value)r'),
                code='invalid row',
                params={'value': self})


class IMA2014b(IMA2014a):

    fieldnames = IMA2014a.fieldnames[1:] + ['x']


PARSERS = {
    'ima2014a': IMA2014a,
    'ima2014b': IMA2014b,
}


class Command(management.base.BaseCommand):
    help = 'Imports CSV data on survey results'

    def add_arguments(self, parser):
        parser.add_argument('file', help='Name of the input file')
        parser.add_argument('--parser', help='Name of the input parser')
        parser.add_argument('--distribution', help='Name of the distribution')
        parser.add_argument('--head', type=int,
                            help='Import only the first n lines')
        parser.add_argument('--has-header', action='store_true',
                            dest='has_header', default=False,
                            help='Skips the first row.')
        parser.add_argument('--atomic', action='store_true', default=False,
                            help=('Faster but a single erroneous row will '
                                  'prevent all from being imported.'))

    def process(self, iterable):
        """
        Calls the profile on each raw item to format it for the database write.
        """
        for row in iterable:
            parser = self.Parser(row)
            if parser.is_valid:
                yield parser
            else:
                logger.warn(parser.error)

    @transaction.atomic()
    def atomic_writer(self, items, head=None):
        """
        Writes survey responses to the database in one transaction to save any clean-up if the
        import fails.

        To cut down on the number of failures due to duplicate rows, it keeps an
        in-memory cache of the keys it has seen so it can skip them before they
        hit the database.
        """
        errors = 0
        keycache = set()
        try:
            for index, item in enumerate(items, 1):
                if head and head < index:
                    break
                key = tuple(item[field] for field in self.Parser.unique_together)
                if key in keycache:
                    logger.warn('Skipping duplicate row:\n%s', item)
                    errors += 1
                    continue
                models.SurveyResponse(distribution=self.distribution, **item).save()
                keycache.add(key)
            return index - errors
        except IntegrityError as exc:
            logger.error('Import failed: %r\n%s', exc, item)
            return 0

    def incremental_writer(self, items, head=None):
        """
        Writes survey responses to the database one by one and skips any that cause errors.
        """
        errors = 0
        for index, item in enumerate(items, 1):
            if head and head < index:
                break
            try:
                models.SurveyResponse(distribution=self.distribution, **item).save()
            except IntegrityError as exc:
                logger.warn('Skipping row: %r\n%s', exc, item)
                errors += 1
        return index - errors

    @staticmethod
    def csvparse(csvfile, has_header=False, **kwargs):
        """
        Parses a CSV file. A tiny wrapper for unicodecsv that can skip the header.
        """
        reader = unicodecsv.DictReader(csvfile, encoding='utf-8', **kwargs)
        if has_header:
            reader.next()
        return reader

    def handle(self, *args, **options):
        if not options['parser'] or options['parser'] not in PARSERS:
            parsers = list(enumerate(PARSERS.keys()))  # Arbitrary fixed order
            print('Please select a parser. Valid parsers are:\n',
                  ''.join('({}) {}\n'.format(index, name)
                          for index, name in parsers),
                  sep='')
            parser_index = input('Number of parser: ')
            self.Parser = PARSERS[dict(parsers)[parser_index]]
        else:
            self.Parser = PARSERS[options['parser']]
        if not options['distribution'] or not models.Distribution.objects \
                .filter(name=options['distribution']).exists():
            distributions = list(enumerate(models.Distribution.objects.all()))
            print('Please select a known distribution or use the Admin '
                  'to create a new one. Known distributions are:',
                  *['\n{}: {}'.format(index, distribution.name)
                    for index, distribution in distributions])
            distribution_index = input('Number of distribution: ')
            self.distribution = dict(distributions)[distribution_index]
        else:
            self.distribution = models.Distribution.objects.get(
                name=options['distribution'])
        if options['atomic']:
            writer = self.atomic_writer
        else:
            writer = self.incremental_writer
        with open(options['file']) as csvfile:
            reader = self.csvparse(
                csvfile, has_header=options['has_header'], fieldnames=self.Parser.fieldnames)
            count = writer(self.process(reader), options['head'])
            print('{} row(s) imported'.format(count))
