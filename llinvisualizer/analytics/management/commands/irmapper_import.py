# -*- encoding: utf-8 -*-
"""
Management command to import data on insecticide resistance from IR Mapper.
"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import logging
import json
import re
from django.db import transaction
from django.db.utils import IntegrityError
from django.core import management
from django.contrib.gis.geos import Point
from ...models import IRTest, Country
from .survey_import import CSVParser


logger = logging.getLogger(__name__)
countries = Country.objects.all()


class IRConverter(CSVParser):
    """
    The IR Mapper profile for import of insecticide resistance data from the site.

    IR Mapper provides its data as JSON, but unfortunately it is still not perfectly structured. Many numeric values are saved as strings, sometimes without apparent reason, but sometimes also because some values are in different units than others, significantly sometimes in absolute unites and sometimes in percent. The import job tries hard to resolve these inconsistencies and save the data in the same unit.

    It also maps idiosyncratic names for countries to their official spellings.
    """

    FIELDMAP = {
        'latitude': 'Latitude',
        'longitude': 'Longitude',
        'chemical_class': 'Chemical_class',
        'chemical_type': 'Chemical_type',
        'country': 'Country',
        'country_name': 'Country',
        'irm_id': 'ID',
        'irac_moa': 'IRAC_MoA',
        'irac_moa_code': 'IRAC_MoA_Code',
        'ir_mechanism_name': 'IR_Mechanism_Name',
        'ir_mechanism_status': 'IR_Mechanism_Status',
        'ir_mechanism_status_code': 'IR_Mechanism_Status_Code',
        'ir_test_method': 'IR_Test_Method',
        'ir_test_exposed': 'IR_Test_NumExposed',
        'ir_test_mortality': 'IR_Test__mortality',
        'insecticide_dosage': 'Insecticide_dosage',
        'kdr_frequency': 'Kdr_frequency',
        'locality': 'Locality',
        'molecular_forms': 'Molecular_forms',
        'mosquito_collection_year_start': 'Mosquito_collection_Year_start',
        'mosquito_collection_period': 'Mosquito_collection_period',
        'mosquito_collection_year_end': 'Mosquito_collection_year_end',
        'r_number': 'RNumber',
        'r_code': 'R_Code',
        'reference_name': 'Reference_Name',
        'reference_type': 'Reference_Type',
        'resistance_status': 'Resistance_status',
        'synergist_test_mortality': 'Synergist_Test_mortality',
        'synergist_type': 'Synergist_Type',
        'synergist_dosage': 'Synergist_dosage',
        'url': 'URL',
        'vector_species': 'Vector_species'}

    CHAR_FIELDS = ['chemical_class', 'chemical_type', 'country_name', 'irac_moa',
                   'irac_moa_code', 'ir_mechanism_name', 'ir_mechanism_status',
                   'ir_mechanism_status_code', 'ir_test_method', 'kdr_frequency',
                   'locality', 'molecular_forms', 'mosquito_collection_period',
                   'r_code', 'reference_name', 'reference_type', 'resistance_status',
                   'synergist_type', 'synergist_dosage', 'url', 'vector_species',
                   'insecticide_dosage']

    NULL_VALUES = ('NA', 'NR', None)

    fields = [field.name.decode('utf-8')
              for field in IRTest._meta.fields
              if field.name not in ('id',)]

    def _fieldmap(self, name):
        return self.FIELDMAP.get(name, name)

    def _normalize_string(self, value):
        if value in self.NULL_VALUES:
            return ''
        return value

    def clean_geom(self):
        longitude, latitude = self.clean_longitude(), self.clean_latitude()
        if longitude and latitude:
            return Point(longitude, latitude)

    def clean_ir_test_mortality(self):
        value = self.data[self._fieldmap('ir_test_mortality')]
        if value not in self.NULL_VALUES:
            values = map(float, re.findall('([0-9.]+)', value))
            value = sum(values) / len(values)
            return float(value)

    def clean_synergist_test_mortality(self):
        value = self.data[self._fieldmap('synergist_test_mortality')]
        if value not in self.NULL_VALUES:
            return float(value)

    def clean_ir_test_exposed(self):
        value = self.data[self._fieldmap('ir_test_exposed')]
        if value not in self.NULL_VALUES:
            return float(value)

    def clean_country(self):
        value = self.data[self._fieldmap('country')]
        value = {"Cote d'Ivoire": "Côte d'Ivoire",
                 'Democratic Republic of Congo': 'Congo (the Democratic Republic of the)',
                 'Guinea Bissau': 'Guinea-Bissau',
                 'Guinea Conakry': 'Guinea',
                 "Lao People's Democratic Republic": 'Laos',
                 'Morroco': 'Morocco',
                 'The Gambia': 'Gambia',}.get(value, value)
        for country in countries:
            if country.name == value:
                return country
        logger.warn('Country not found %r', value)

    def clean(self):
        for field in self.CHAR_FIELDS:
            self[field] = self._normalize_string(self[field])


class Command(management.base.BaseCommand):
    help = 'Imports data on insecticide resistance from IR Mapper'

    def add_arguments(self, parser):
        parser.add_argument('file', help='Name of the input file')
        parser.add_argument('--head', type=int,
                            help='Import only the first n lines')

    def process(self, items):
        """
        Calls the profile on each raw item to format it for the database write.
        """
        for item in items:
            parser = IRConverter(item)
            if parser.is_valid:
                yield parser
            else:
                logger.warn(parser.errors)

    @transaction.atomic()
    def atomic_writer(self, items, head=None):
        """
        Writes distributions to the database in one transaction
        to save any clean-up if the import fails.
        """
        try:
            for index, item in enumerate(items, 1):
                if head and head < index:
                    break
                IRTest(**item).save()
            return index
        except IntegrityError as exc:
            logger.error('Import failed: %r\n%s', exc, item)
            return 0

    def handle(self, *args, **options):
        with open(options['file']) as jsonfile:
            items = json.load(jsonfile)
        count = self.atomic_writer(self.process(items), options['head'])
        print('{} row(s) imported'.format(count))
