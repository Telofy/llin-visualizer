# -*- encoding: utf-8 -*-
"""
Management command to run the cluster generation and at the same time generate the statistics associated with the clusters. It runs the same calculations for distributions, which are structurally identical to clusters.
"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import logging
from time import time
from django.db import connection
from django.core import management
from ... import models

logger = logging.getLogger(__name__)


class Command(management.base.BaseCommand):
    help = 'Generates clusters of survey data and their associated statistics'

    def augment_distributions(self):
        """
        Group all responses to the surveys by their respective distributions and then use PostGIS functions to calculate the centroids and convex hulls of these points. These data are then used to augment the distribution relation.
        """
        self.cursor.execute("""
            update analytics_distribution
                set
                    centroid = metrics.centroid,
                    hull = metrics.hull
                from (
                    select
                        analytics_surveyresponse.distribution_id as distribution_id,
                        st_centroid(st_collect(geom)) as centroid,
                        st_convexhull(st_force_2d(st_collect(geom))) as hull
                    from analytics_surveyresponse
                    group by analytics_surveyresponse.distribution_id
                ) metrics
                where analytics_distribution.id = metrics.distribution_id""")

    def generate_clusters(self, fields, kind):
        """
        Generate clusters of type `kind` using `fields` for grouping.

        The job is parameterized with `kind` and `fields`, where `kind` is a name for the type of cluster, such as “village” or “health area,” and `fields` are the grouping fields that describe this type. Here it is important to note that for a grouping by village it is better to group by village name, health area name, health zone name, and even larger categories rather than just by village name, since there are many villages with the same name.

        The query performs the same PostGIS operations as the previous one, but then also concatenates the values of the grouping fields to one comma-joined string, such as “Shakunga, Kabungu,” as a name for the cluster. These names are by necessity unique.
        """
        groupby = ', '.join(
            'analytics_surveyresponse.' + field for field in fields)
        self.cursor.execute("""
            insert into analytics_cluster (kind, name, distribution_id, centroid, hull)
                select
                    '{kind}',
                    concat_ws(', ', {groupby}),
                    analytics_surveyresponse.distribution_id,
                    st_centroid(st_collect(geom)) as centroid,
                    st_convexhull(st_force_2d(st_collect(analytics_surveyresponse.geom)))
                from analytics_surveyresponse
                group by
                    {groupby},
                    analytics_surveyresponse.distribution_id
            """.format(
                kind=kind,
                groupby=groupby))
        self.cursor.execute("""
            update analytics_surveyresponse
            set cluster_id = analytics_cluster.id
            from analytics_cluster
            where
                (concat_ws(', ', {groupby}),
                 analytics_surveyresponse.distribution_id) =
                (analytics_cluster.name, analytics_cluster.distribution_id)
        """.format(groupby=groupby))

    def generate_cluster_statistics(self):
        """
        Generate cluster statistics.
        """
        return self._generate_statistics('cluster_id', 'analytics_cluster')

    def generate_distribution_statistics(self):
        """
        Generate distribution statistics.
        """
        return self._generate_statistics('distribution_id', 'analytics_distribution')

    def _generate_statistics(self, foreign_key, parent_table):
        """
        Internal method for generating statistics.

        This method is used to generate the cluster and distribution statistics. The type of the grouping is again parameterized through the foreign key field and the referenced relation.

        The job creates descriptive statistics such as averages, maximums, standard deviations, counts, and sums of numeric and geographic data in scalar and vector formats. It heeds the definitions of the data structures from the survey importer and interprets them as consisting of the names of keys and the names of values, to which it assigns aggregate results. An example will clarify the operation.

        One simple case is the cluster centroid: The centroid is assigned various attributes to enrich it, namely the average distance of the points it describes, their maximal distance, the standard deviation of their distance, and their number. PostGIS functions are used to determine these values for the centroids of every cluster. In total there are currently 60 such tuples, which are computed for each of 8 distributions and over 10,000 clusters.
        """
        snippets = {}
        for key, mapping in [('malaria_symptoms', models.MALARIA_SYMPTOMS),
                             ('malaria_transmission', models.MALARIA_TRANSMISSION),
                             ('malaria_prevention', models.MALARIA_PREVENTION),
                             ('malaria_treatment', models.MALARIA_TREATMENT),
                             ('preventive_measures', models.PREVENTIVE_MEASURES)]:
            snippets[key] = {}
            snippets[key]['keys'] = ["'{}'".format(key)] * len(mapping)
            snippets[key]['values'] = map("'{}'".format, mapping.keys())
            snippets[key]['sums'] = [
                'sum((array[{}::varchar(32)] <@ analytics_surveyresponse.{})::int)'.format(
                    value, key.replace(' ', '_'))
                for value in snippets[key]['values']]
        nonterminals = {}
        for category in ('keys', 'values', 'sums'):
            nonterminals[category] = ', '.join(
                ', '.join(snippets[key][category])
                for key in snippets.keys())
        query = (
            """
                insert into analytics_statistic ({foreign_key}, key, value, aggregate)
                    select
                        analytics_surveyresponse.{foreign_key},
                        unnest(ARRAY[
                            'centroid',
                            'centroid',
                            'centroid',
                            'survey_response',
                            'total_usage',
                            'total_usage',
                            'women_usage',
                            'women_usage',
                            'children_usage',
                            'children_usage',
                            'net_source',
                            'net_source',
                            'net_source',
                            'net_source',
                            'net_source',
                            {keys}
                        ]),
                        unnest(ARRAY[
                            'avg_distance',
                            'max_distance',
                            'sd_distance',
                            'count',
                            'people_in_household',
                            'people_under_net_last_night',
                            'pregnant_women',
                            'pregnant_women_under_net_last_night',
                            'children_under_five',
                            'children_under_net_last_night',
                            'market',
                            'health_center',
                            'distribution',
                            'ngo',
                            'other',
                            {values}
                        ]),
                        unnest(ARRAY[
                            avg(st_distance({parent_table}.centroid::geography,
                                            analytics_surveyresponse.geom)),
                            max(st_distance({parent_table}.centroid::geography,
                                            analytics_surveyresponse.geom)),
                            stddev_samp(st_distance({parent_table}.centroid::geography,
                                                    analytics_surveyresponse.geom)),
                            count(analytics_surveyresponse.geom),
                            sum(analytics_surveyresponse.people_in_household),
                            sum(analytics_surveyresponse.people_under_net_last_night),
                            sum(analytics_surveyresponse.pregnant_women),
                            sum(analytics_surveyresponse.pregnant_women_under_net_last_night),
                            sum(analytics_surveyresponse.children_under_five),
                            sum(analytics_surveyresponse.children_under_net_last_night),
                            sum((analytics_surveyresponse.net_source = 'market')::int),
                            sum((analytics_surveyresponse.net_source = 'health_center')::int),
                            sum((analytics_surveyresponse.net_source = 'distribution')::int),
                            sum((analytics_surveyresponse.net_source = 'ngo')::int),
                            sum((analytics_surveyresponse.net_source = 'other')::int),
                            {sums}
                        ])
                    from analytics_surveyresponse
                    join {parent_table}
                    on (analytics_surveyresponse.{foreign_key} = {parent_table}.id)
                    group by
                        analytics_surveyresponse.{foreign_key}
            """.format(foreign_key=foreign_key,
                       parent_table=parent_table,
                       **nonterminals))
        self.cursor.execute(query)

    def add_arguments(self, parser):
        parser.add_argument('kind',
                            help='Type of cluster (e.g., village or district)')
        parser.add_argument('groupby', nargs='+',
                            choices=['village_name', 'district_name', 'health_zone_name',
                                     'health_area_name'],
                            help='Survey response fields to group by, smallest first')
        parser.add_argument('-d', '--delete', action='store_true', default=False,
                            help='Delete and regenerate existing tables')

    def handle(self, *args, **options):
        self.cursor = connection.cursor()
        if options['delete']:
            print('Emptying...')
            start = time()
            self.cursor.execute('update analytics_distribution set centroid = null, hull = null')
            self.cursor.execute('update analytics_surveyresponse set cluster_id = null')
            self.cursor.execute('delete from analytics_statistic')
            self.cursor.execute('delete from analytics_cluster')
            print('Took {:.1f} s'.format(time() - start))
        if not models.Distribution.objects.exclude(centroid=None).exists():
            print('Augmenting distributions...')
            start = time()
            self.augment_distributions()
            print('Took {:.1f} s'.format(time() - start))
        if not models.Cluster.objects.all().exists():
            print('Generating clusters...')
            start = time()
            self.generate_clusters(options['groupby'], options['kind'])
            print('Took {:.1f} s'.format(time() - start))
        if not models.Statistic.objects.exclude(cluster=None).exists():
            print('Generating cluster statistics...')
            start = time()
            self.generate_cluster_statistics()
            print('Took {:.1f} s'.format(time() - start))
        if not models.Statistic.objects.exclude(distribution=None).exists():
            print('Generating distribution statistics...')
            start = time()
            self.generate_distribution_statistics()
            print('Took {:.1f} s'.format(time() - start))
