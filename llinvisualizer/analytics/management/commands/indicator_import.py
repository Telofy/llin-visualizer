# -*- encoding: utf-8 -*-
"""
Management command to import CSV data on net gaps.

The net gap data have come in tables that can be converted to CSV files. A dedicated job imports data on nets needed, financed, and net gaps severally. The user needs to specify the type of data (out of those three types) and the year of the estimate, so that LLIN Visualizer can choose the newer estimate if there are several for a given year, country, and type.
"""
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import logging
import re
import unicodecsv
from copy import copy
from datetime import datetime, date
from django.utils.translation import ugettext as _
from django.db import transaction
from django.db.utils import IntegrityError
from django.core import management
from django.forms import ValidationError
from django.contrib.gis.geos import Point
from ... import models


logger = logging.getLogger(__name__)
countries = {country.name: country for country in models.Country.objects.all()}
synonyms = {'Democratic Republic of Congo': countries['Congo (the Democratic Republic of the)'],
            'Micronesia (urban)': countries['Micronesia (Federated States of)'],
            'Gambia,The': countries['Gambia'],
            'St Lucia': countries['Saint Lucia'],
            'India-Rural': countries['India'],
            'India-Urban': None,
            "Cote d'Ivoire": countries["Côte d'Ivoire"],
            'Ivory Coast': countries["Côte d'Ivoire"],
            'Congo, Rep.': countries['Congo'],
            'Indonesia-Rural': countries['Indonesia'],
            'Indonesia-Urban': None,
            'China - Rural': countries['China'],
            'China - Urban': None,
            'Kyrgyz Republic': countries['Kyrgyzstan'],
            'Mexico (i)': None,
            'Mexico (c)': countries['Mexico'],
            'Romania (i)': None,
            'Romania (c)': countries['Romania'],
            'Poland (i)': None,
            'Poland (c)': countries['Poland'],
            'Macedonia, FYR': countries['Macedonia'],
            'West Bank and Gaza': None,
            'Slovak Republic': countries['Slovakia'],
            'Taiwan, China': countries['Taiwan'],
            'United States': countries['United States of America'],
            'North Cyprus': countries['Cyprus'],
            'Northern Cyprus': countries['Cyprus'],
            'Palestinian Territories': countries['Palestine, State of'],
            'Congo (Kinshasa)': countries['Congo (the Democratic Republic of the)'],
            'Congo (Brazzaville)': countries['Congo'],
            'Congo Brazzaville': countries['Congo'],
            'Trinidad & Tobago': countries['Trinidad and Tobago'],
            'Kosovo': None,
            'Somaliland region': None,}
countries.update(synonyms)


class Command(management.base.BaseCommand):
    help = """Imports CSV indicator data. These data can be obtained from the following sources:
            (1) https://www.givingwhatwecan.org/post/2016/05/giving-and-global-inequality/#fn3
            (2) http://worldhappiness.report/"""

    def add_arguments(self, parser):
        parser.add_argument('file', help='Name of the input file')
        parser.add_argument('--whrsource', action='store_true', default=False,
                            help='Source data from World Happiness Report.')
        parser.add_argument('--whrscore', action='store_true', default=False,
                            help='Scores from World Happiness Report.')
        parser.add_argument('--gdppc', action='store_true', default=False,
                            help='GDP per capita data.')

    def process_whr_source(self, iterable):
        """
        Cleans and transforms the raw data to give it the right shape for the database.
        """
        for row in iterable:
            country_name, _, year, life_ladder, _, social_support, life_expectancy, freedom, \
            generosity, corruption, positive_affect, negative_affect, confidence_government, \
            democratic_quality, delivery_quality, gini_index, trust = row[:17]
            country_name = country_name.strip()
            if country_name == '':
                break
            if country_name == 'WP5 Country' or countries[country_name] is None:
                continue
            yield {'country': countries[country_name],
                   'year': year,
                   'life_ladder': life_ladder.replace(',', '.') or None,
                   'social_support': social_support.replace(',', '.') or None,
                   'life_expectancy': life_expectancy.replace(',', '.') or None,
                   'freedom': freedom.replace(',', '.') or None,
                   'generosity': generosity.replace(',', '.') or None,
                   'corruption': corruption.replace(',', '.') or None,
                   'positive_affect': positive_affect.replace(',', '.') or None,
                   'negative_affect': negative_affect.replace(',', '.') or None,
                   'confidence_government': confidence_government.replace(',', '.') or None,
                   'democratic_quality': democratic_quality.replace(',', '.') or None,
                   'delivery_quality': delivery_quality.replace(',', '.') or None,
                   'gini_index': gini_index.replace(',', '.') or None,
                   'trust': trust.replace(',', '.') or None,}

    def process_whr_score(self, iterable):
        """
        Cleans and transforms the raw data to give it the right shape for the database.
        """
        for row in iterable:
            country_name, whr_score = row[:2]
            country_name = country_name.strip()
            if country_name == '':
                break
            if country_name == 'Country' or countries[country_name] is None:
                continue
            yield {'country': countries[country_name],
                   'year': 2016,
                   'whr_score': whr_score.replace(',', '.')}

    def process_gdp(self, iterable):
        """
        Cleans and transforms the raw data to give it the right shape for the database.
        """
        for row in iterable:
            country_name, median_gdp, mean_gdp, _ = row
            country_name = country_name.strip()
            if country_name == '':
                break
            if country_name == 'Country' or countries[country_name] is None:
                continue
            yield {'country': countries[country_name],
                   'year': 2014,
                   'gdp_pc_median': median_gdp.replace('$', '').replace(',', ''),
                   'gdp_pc_mean': mean_gdp.replace('$', '').replace(',', '')}

    @transaction.atomic()
    def atomic_writer(self, items):
        """
        Writes distributions to the database in one transaction
        to save any clean-up if the import fails.
        """
        unique_together = models.Indicator._meta.unique_together[0]
        for index, item in enumerate(items, 1):
            keys = {key: value for key, value in item.items()
                    if key in unique_together}
            indicator, created = models.Indicator.objects.update_or_create(
                defaults=item, **keys)
        return index

    def handle(self, *args, **options):
        processors = {'whrsource': self.process_whr_source,
                      'whrscore': self.process_whr_score,
                      'gdppc': self.process_gdp}
        input_type = [key for key, value in options.items()
                      if value and key in processors][0]
        process = processors[input_type]
        with open(options['file']) as csvfile:
            reader = unicodecsv.reader(csvfile, encoding='utf-8', delimiter=b'\t')
            count = self.atomic_writer(process(reader))
            print('{} row(s) imported'.format(count))
