# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from django import template
from jinja2 import Environment

register = template.Library()
environment = Environment()


@register.filter()
def add_class(field, css):
   return field.as_widget(attrs={'class': css})
