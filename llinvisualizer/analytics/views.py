# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import logging
from itertools import chain
from urllib import urlencode
from djgeojson.views import GeoJSONLayerView
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.gis.db.models.fields import GeometryField
from django.db.models import Q
from . import models


logger = logging.getLogger(__name__)


class LoginRequiredMixin(object):
    """
    A mixin for class-based views that corresponds to the
    `@login_required` decorator for function views.
    """

    @classmethod
    def as_view(cls, *args, **kwargs):
        """
        Wrapper for the `as_view` method of the super class that
        wraps it using the `login_required` decorator.
        """
        return login_required(
            super(LoginRequiredMixin, cls).as_view(*args, **kwargs))


def map_(request):
    """
    The main map view.

    It parameterizes the template render call with URLs to prefetch
    in the browser as well as the models for access to the chart or
    statistics constants.
    """
    distribution_ids = models.Statistic.objects.filter(
        distribution_id__isnull=False,
        key='survey_response',
        value='count',
        aggregate__gt=0).values_list('distribution_id')
    distribution_ids = list(chain.from_iterable(distribution_ids))
    prefetch = [reverse(key, args=[distribution_id])
                for key in ['cluster-centroid-geojson', 'cluster-hull-geojson']
                for distribution_id in distribution_ids]
    prefetch += [reverse('distribution-hull-geojson'),
                 reverse('distribution-centroid-geojson'),
                 reverse('distribution-point-geojson'),
                 reverse('countries-netgaps-geojson'),
                 reverse('countries-indicators-geojson')]
    params = {'models': models, 'prefetch': prefetch}
    if request.GET.get('base'):
        return render(request, 'analytics/map_base.jinja', params)
    return render(request, 'analytics/map.jinja', params)


def tests(request):
    """
    The tests view, which is very similar to the map view, but uses
    a template that hides the actual map.
    """
    params = {'models': models}
    return render(request, 'analytics/tests.jinja', params)


def translate(request):
    """
    A view that returns redirects to translate between the IDs AMF uses
    and the IDs LLIN Visualizer uses for distirbutions and distribution
    campaigns.

    If a distribution ID is given, LLIN Visualizer with center the distribution
    and zoom in to it. If a proposal ID (a.k.a. distribution campaign ID) is
    given, it will focus the bounding box of the distributions covered.
    """
    distribution_id = int(request.GET.get('distribution_id', 0))
    proposal_id = int(request.GET.get('proposal_id', 0))
    query = [('version', 1)]
    if distribution_id:
        distribution = get_object_or_404(models.Distribution, amf_id=distribution_id)
        query.append(('distribution[target]', distribution.pk))
    elif proposal_id:
        campaign = get_object_or_404(models.DistributionCampaign, amf_id=proposal_id)
        bbox = campaign.bbox
        query.extend([
            ('distribution[bbox][0][]', bbox[1]),
            ('distribution[bbox][0][]', bbox[0]),
            ('distribution[bbox][1][]', bbox[3]),
            ('distribution[bbox][1][]', bbox[2]),])
    return redirect(reverse('analytics-map-view') + '#' + urlencode(query))


class AnalyticsLayer(GeoJSONLayerView):
    """
    An abstract GeoJSON class the overrides the defaults of the library
    so that all model attributes and properties are serialized by default
    and don’t have to be explicitly whitelisted. The `id` field and geometry
    fields are excluded because GeoJSON already has special places for them.

    All the subclasses are called from `urls.py`.
    """

    def _get_properties(self):
        return [name for name, var in vars(self.model).items()
                if type(var) is property]

    def render_to_response(self, *args, **kwargs):
        self.properties = [
            field.name.decode('utf-8')
            for field in self.model._meta.fields
            if field.name != 'id' and not isinstance(field, GeometryField)]
        self.properties += self._get_properties()
        return super(AnalyticsLayer, self).render_to_response(*args, **kwargs)


class DistributionLayer(AnalyticsLayer):
    """
    The (unparameterized) list of all distributions including metadata.
    """

    def get_queryset(self):
        return super(DistributionLayer, self).get_queryset() \
            .select_related('country', 'campaign').prefetch_related('statistic_set').all()


class ClusterLayer(AnalyticsLayer):
    """
    The list of all clusters, including metadata, that belong to one distribution.
    """

    def get_queryset(self):
        distribution = models.Distribution.objects.get(pk=self.kwargs['distribution_id'])
        return distribution.clusters.prefetch_related('statistic_set').all()


class SurveyResponseLayer(LoginRequiredMixin, AnalyticsLayer):
    """
    The list of all survey responses, including metadata,
    that belong to one cluster. Only accessible for users
    with sufficient privileges.
    """

    def get_queryset(self):
        cluster = models.Cluster.objects.get(pk=self.kwargs['cluster_id'])
        return cluster.responses.all()


class IRTestLayer(AnalyticsLayer):
    """
    The list of all clusters, including metadata, that belong to one distribution.
    """

    def get_queryset(self):
        country = models.Country.objects.get(pk=self.kwargs['country_id'])
        return country.ir_tests.all()


class NetGapLayer(AnalyticsLayer):
    """
    The list of all countries that have net gaps associated with them, and
    of course the net gaps themselves.

    I’m avoiding Q expressions here since that led to a mysterious duplication
    of the countries as often as there are net gaps for it.
    """

    def _get_properties(self):
        return ['netgap']

    def get_queryset(self):
        return models.Country.objects.prefetch_related('netgaps') \
            .filter(geom__isnull=False) \
            .exclude(netgaps__gap__isnull=True,
                     netgaps__needed__isnull=True,
                     netgaps__financed__isnull=True).all()


class IndicatorLayer(AnalyticsLayer):
    """
    The list of all countries that have indicators associated with them, and
    of course the indicators themselves.

    I’m avoiding Q expressions here since that led to a mysterious duplication
    of the countries as often as there are net gaps for it.
    """

    def _get_properties(self):
        return ['indicator']

    def get_queryset(self):
        return models.Country.objects.prefetch_related('indicators') \
            .filter(geom__isnull=False).all()
