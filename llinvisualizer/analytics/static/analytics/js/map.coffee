root = exports ? this


String.prototype.capitalize = ->
    @[0].toUpperCase() + @[1..]


$('[data-toggle="tooltip"]').tooltip(html: true, container: 'body')


# Map initialization

root.maplayers =
  osm: L.tileLayer(
    'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    id: 'osm'),
  humanitarian: L.tileLayer(
    'https://{s}.openstreetmap.fr/hot/{z}/{x}/{y}.png',
    subdomains: ['tile-a', 'tile-b', 'tile-c'])

root.pfalciparum = L.tileLayer(
  'https://{s}.arcgis.com/tiles/UPrwBo0c2XSbU4QX/arcgis/rest/services/ST_PR_Mean/MapServer/tile/{z}/{y}/{x}',
  id: 'pfalciparum',
  opacity: 0.5,
  subdomains: ['tiles1', 'tiles2', 'tiles3', 'tiles4'])

root.pvivax = L.tileLayer(
  'https://{s}.arcgis.com/tiles/UPrwBo0c2XSbU4QX/arcgis/rest/services/PVPR_Mean/MapServer/tile/{z}/{y}/{x}',
  id: 'pvivax',
  opacity: 0.5,
  subdomains: ['tiles1', 'tiles2', 'tiles3', 'tiles4'])

root.eir = L.tileLayer(
  'https://{s}.arcgis.com/tiles/UPrwBo0c2XSbU4QX/arcgis/rest/services/EIR_Layers_TIFF/MapServer/tile/{z}/{y}/{x}',
  id: 'eir',
  opacity: 0.5,
  subdomains: ['tiles1', 'tiles2', 'tiles3', 'tiles4'])

root.map = L.map('map',
  center: new L.LatLng(-6.4, 21),
  zoom: 4,
  zoomControl: false
)

attribution =
  'Sources: <a href="http://againstmalaria.com/">AMF</a>, ' +
  '<a href="http://www.irmapper.com/">IRM</a>, ' +
  '<a href="http://www.map.ox.ac.uk/">MAP</a>, ' +
  '<a href="http://openstreetmap.org/">OSM</a>' +
  ' | <a href="https://bitbucket.org/Telofy/llin-visualizer/issues">Bugs?</a>'
if auth.isAuthenticated
  attribution +=
    " | Hi, #{auth.user}! " +
    "(<a href='#{auth.logoutUrl}?next=#{window.location.pathname}'>Log out</a>"
  if auth.isStaff
    attribution += ", <a href='#{auth.adminUrl}'>admin</a>)"
  else
    attribution += ")"
else
  attribution +=
    " | <a data-toggle='lightbox' data-parent='' data-gallery='remoteload'" +
    " data-title='LLIN Visualizer Internal' data-disable-external-check='true'" +
    " href='#{auth.loginUrl}?next=#{window.location.pathname}'" +
    ">Log in</a>"
map.attributionControl.setPrefix('').addAttribution(attribution)

$(document).delegate('*[data-toggle="lightbox"]', 'click', (event) ->
    event.preventDefault()
    $(this).ekkoLightbox())

root.zoom = L.control.zoom(position: 'topright').addTo(map)

loadingControl = L.Control.loading
  separate: true,
  zoomControl: zoom,
  position: 'topright'
map.addControl(loadingControl)

menuContainer = $('.menu-container')
for [prop, cls] in [[L.Browser.touch, 'leaflet-touch'],
                  [L.Browser.retina, 'leaflet-retina'],
                  [L.Browser.ielt9, 'leaflet-oldie'],
                  [L.Browser.safari, 'leaflet-safari'],
                  [this._fadeAnimated, 'leaflet-fade-anim']]
  if prop
    menuContainer.addClass(cls)

# Info

root.info = L.control(position: 'bottomright')
root.info.onAdd = (map) ->
  @_div = L.DomUtil.create('div', 'info legend')
  @_div.style.display = 'none'
  @update()
  return this._div
root.info.update = (content) ->
  if content
    @_div.innerHTML = content
    $(@_div).find('[data-toggle="tooltip"]').tooltip(html: true)
    if $(@_div).find('img').length
      @_div.style['min-width'] = '300px'
    else
      @_div.style['min-width'] = '0'
  else
    this._div.innerHTML = 'Hover over a feature'
    @_div.style['min-width'] = '0'
info.addTo(map)


# Slider initialization

$('#ir-slider').slider(
  range: true,
  min: 1950,
  max: (new Date()).getFullYear(),
  step: 1,
  values: [2000, (new Date()).getFullYear()],
  slide: (event, ui) ->
    $('#ir-range').text(ui.values[0] + '–' + ui.values[1]))
$('#ir-range').text(
  $('#ir-slider').slider('values', 0) + '–' +
  $('#ir-slider').slider('values', 1))

$('#netgap-year-slider').slider(
  min: 2013,
  max: 2017,
  step: 1,
  value: 2016,
  slide: (event, ui) ->
    $('#netgap-year').text(ui.value))
$('#netgap-year').text($('#netgap-year-slider').slider('value'))

$('#indicator-year-slider').slider(
  min: 2013,
  max: 2016,
  step: 1,
  value: 2016,
  slide: (event, ui) ->
    $('#indicator-year').text(ui.value))
$('#indicator-year').text($('#indicator-year-slider').slider('value'))

$('#distribution-responses-slider').slider(
  min: 0,
  max: 10**5,
  step: 10**3,
  value: 0,
  slide: (event, ui) ->
    $('#distribution-responses-range').text(ui.value + '–∞'))
$('#distribution-responses-range').text(
  $('#distribution-responses-slider').slider('values', 0) + '–∞')

$('#distribution-nets-slider').slider(
  min: 0,
  max: 10**5,
  step: 10**3,
  value: 0,
  slide: (event, ui) ->
    $('#distribution-nets-range').text(ui.value + '–∞'))
$('#distribution-nets-range').text(
  $('#distribution-nets-slider').slider('values', 0) + '–∞')

$('#distribution-years-slider').slider(
  range: true,
  min: 2006,
  max: (new Date()).getFullYear() + 5,
  step: 1,
  values: [(new Date()).getFullYear() - 4,
           (new Date()).getFullYear() + 5],
  slide: (event, ui) ->
    $('#distribution-years-range').text(ui.values[0] + '–' + ui.values[1]))
$('#distribution-years-range').text(
  $('#distribution-years-slider').slider('values', 0) + '–' +
  $('#distribution-years-slider').slider('values', 1))

$('#cluster-count-slider').slider(
  range: true,
  min: 0,
  max: 2000,
  step: 50,
  values: [100, 2000],
  slide: (event, ui) ->
    $('#cluster-count-range').text(ui.values[0] + '–' + ui.values[1]))
$('#cluster-count-range').text(
  $('#cluster-count-slider').slider('values', 0) + '–' +
  $('#cluster-count-slider').slider('values', 1))

$('#cluster-variance-slider').slider(
  range: true,
  min: 0,
  max: 2 * 10**5,
  step: 10**4,
  values: [0, 4 * 10**4],
  slide: (event, ui) ->
    $('#cluster-variance-range').text(ui.values[0] / 1000 + '–' + ui.values[1] / 1000 + ' km'))
$('#cluster-variance-range').text(
  $('#cluster-variance-slider').slider('values', 0) / 1000 + '–' +
  $('#cluster-variance-slider').slider('values', 1) / 1000 + ' km')


class Semaphore
  counter: 0

  acquire: (fun) ->
    if @counter == 0
      fun()
    @counter++

  release: (fun) ->
    @counter--
    if @counter == 0
      fun()


root.loadingSemaphore = new Semaphore()


class URLState

  constructor: ->
    @state = version: 1
    params = $.deparam location.hash.replace('#', '')
    if params.version == '1'
      @state = params
    else if params.version
      console.warn('Unknown version: ' + params.version)

  update: ->
    location.hash = '#' + $.param @state

  set: (namespace, key, value) ->
    if namespace not of @state
      @state[namespace] = {}
    if @state[namespace][key] != value
      @state[namespace][key] = value
      @update()

  get: (namespace, key) ->
    if namespace of @state
      @state[namespace][key]


root.urlState = new URLState()


class NetLayer
  lowerLayers: []
  urlState: urlState
  highlitFeature: null

  # Entry and exit point

  load: (parent) =>
    loadingSemaphore.acquire(->
      map.fire('dataloading'))
    @parent = parent
    @allFeatures.clearLayers()
    $.getJSON(@getUrl(), (data) =>
      @loadData(data)
      @reviewData()
      loadingSemaphore.release(->
        map.fire('dataload')))

  drillDown: =>
    for layer in @lowerLayers
      layer.load(@)

  # Relevant for the current layer as a whole

  init: =>

  loadData: (data) =>
    @allFeatures = L.geoJson(data, @geoJsonOptions())
    # Has a target of this layer already been selected?
    target_id = @urlState.get(@name, 'target')
    if target_id
      @selectedLayer = @layerMapping[target_id]
      @focusFeatureLight()
      @focusFeatureHeavy()
    bbox = @urlState.get(@name, 'bbox')
    if bbox
      @focusBBox(bbox)

  clearData: =>
    for layer in @lowerLayers
      layer.clearData()
    @allFeatures.clearLayers()

  reviewData: =>
    @hideData()
    @viewData()

  viewData: =>
    @init()
    @allFeatures.eachLayer(@select)
    @visibleFeatures.eachLayer((layer) =>
      if layer.setStyle
        layer.setStyle(@style(layer.feature)))
    @fitBounds()

  focusBBox: (bbox) =>
    @selectedBounds = L.latLngBounds(bbox...)
    map.fitBounds(@selectedBounds, maxZoom: 8, animate: true)

  ### Set the view such that all visible features of the current layer are visible. ###
  fitBounds: =>
    bounds = @visibleFeatures.getBounds()
    if Object.keys(bounds).length > 0
      map.fitBounds(@visibleFeatures.getBounds(), animate: true)

  hideData: =>
    @visibleFeatures.clearLayers()

  legend: =>
    title = "<p><strong>#{@gradeDimension}</strong></p>"
    labels = []
    symbols = {'%': 0.01, '': 1, 'K': 10**3, 'M': 10**6, 'G': 10**9}
    for grade, color of @grades
      gradePPs = (grade / divisor + symbol for symbol, divisor of symbols)
      gradePP = _.sortBy(gradePPs, (gradePP) -> gradePP.length)[0]
      html = "<i style=\"background: #{color}\"></i> #{gradePP}&thinsp;#{@gradeUnit}"
      labels.push(html)
    title + labels.join('<br />')

  # Relevant for each feature of the current layer

  geoJsonOptions: =>
    if @view == 'hull'
      style: @style,
      onEachFeature: @onEach,
    else  # point-shaped markers
      pointToLayer: @pointToLayer,
      onEachFeature: @onEach

  onEach: (feature, layer) =>
    @layerMapping[feature.id] = layer
    layer.on(
      mouseover: @highlight,
      mouseout: @resetHighlight,
      click: @click)

  click: (event) =>
    @urlState.set(@name, 'target', event.target.feature.id)
    @selectedLayer = event.target
    if @lowerLayers.length
      @focusFeatureLight(true)
      @drillDown()
      setTimeout(@focusFeatureHeavy, 1000)
    else
      @focusFeatureLight()
      @focusFeatureHeavy()

  style: (feature) =>
    color = @getColor(@getColorMetric(feature))
    weight: 0,
    opacity: 1,
    color: 'white',
    fillOpacity: 0.3,
    fillColor: color

  pointToLayer: (feature, latlng) =>
    # TODO: Add title, alt
    L.marker(latlng, icon: @getIcon(feature))

  getIcon: (feature) =>
    color = @getColor(@getColorMetric(feature))
    size = @getSize(null, feature)
    size = if size > 0.3 then size else .3
    size = if size < 1.0 then size else 1.0
    dimensions = [100, 100]
    options =
      iconSize: (dim * size for dim in dimensions),
      html: @markerTemplate(
        fill: color, stroke: '#FFFFFF', strokeWidth: 0)
    L.divIcon(options)

  select: (layer) =>
    @visibleFeatures.addLayer(layer)

  # Relevant for specific features of the current layer

  viewInfo: (feature) =>
    context = @info(feature)
    content = @infoTemplate(context)
    info.update(content)
    map.on('click', => @resetInfo())

  resetInfo: =>
    info.update(@legend())

  highlight: (event) =>
    if @view == 'hull'
      event.target.setStyle(weight: 3)
    else
      $(event.target._icon).find('.icon-path').css('stroke-width', 7)
    if @highlitFeature != event.target.feature.id
      @highlitFeature = event.target.feature.id
      @viewInfo(event.target.feature)

  resetHighlight: (event) =>
    if @view == 'hull'
      event.target.setStyle(weight: 0)
    else
      $(event.target._icon).find('.icon-path').css('stroke-width', 0)

  # A map feature is selected through a click or the URL, not just hovering.
  # Light version for instantaneous execution.
  focusFeatureLight: (drillDown=false) =>
    if not drillDown
      @setView()
    @control.collapse('show')

  # Heavy version for delayed execution.
  focusFeatureHeavy: =>
    info.update(@legend())
    @viewStats(@selectedLayer.feature)

  ### Set the view so that the selected layer is centered. ###
  setView: =>
    zoom = Math.max(map.getZoom(), 6)
    if @selectedLayer.getLatLng
      map.setView(@selectedLayer.getLatLng(), zoom, animate: true)
    else
      map.fitBounds(@selectedLayer.getBounds(), animate: true)

  viewStats: (feature) =>
    for key, chart of charts
      values = $.extend({}, feature.properties.statistics[key])
      for subkey, value of values
        if not chartMeta[key].detail[subkey]
          console.log(key, subkey)
        # Changing sign
        if chartMeta[key].detail[subkey].correct is false
          values[subkey] *= -1
        else if chartMeta[key].detail[subkey].correct is null
          delete values[subkey]
      for subkey, value of chartMeta[key].detail
        # Renaming
        values[value.description.capitalize()] = values[subkey]
        delete values[subkey]
      legendWidth = 0
      for subkey, value of values
        # Determining width of legend
        if subkey.length > legendWidth
          legendWidth = subkey.length
      subkeys = _(Object.keys(values)).sortBy((element) -> -values[element])
      values.name = key.replace('_', ' ').capitalize()
      if chartMeta[key].tab == 'knowledge'
        charts[key].resize(width: 200 + 6.5 * legendWidth)
      else
        charts[key].resize(width: 150 + 40 * subkeys.length + 6.5 * legendWidth)
      charts[key].load
        json: [values],
        keys:
          x: 'name',
          value: subkeys,
      if chartMeta[key].tab == 'knowledge'
        charts[key].groups([subkeys])


class SurveyResponseLayer extends NetLayer
  name: 'surveyResponse'
  infoTemplate: Handlebars.compile($("#survey-response-template").html())
  markerTemplate: Handlebars.compile($("#group-marker-template").html())
  allFeatures: L.featureGroup()
  layerMapping: {}
  visibleFeatures: L.featureGroup().addTo(map)
  colorScale: chroma.scale(['#9999ff', '#000044'])
  control: $('#cluster-control')

  ### grades needs to be here because it uses getColor,
      which will only be defined later. The rest are here
      for semantic contiguity. ###
  init: =>
    @gradeUnit = 'nets'
    @gradeDimension = 'Total nets<br />per household'
    @grades = _.object([grade, @getColor(grade)] for grade in [0, 2, 5, 10])

  getUrl: =>
    url = geoJsonUrls.surveyResponse
    url.replace('0', @parent.selectedLayer.feature.id)

  getSize: (metric, feature=null) =>
    if metric is null
      metric = feature.properties.people_in_household
    Math.log(metric) / 3

  getColorMetric: (feature) =>
    feature.properties.llins_total

  getColor: (metric) =>
    scaledMetric = metric / 10
    @colorScale(scaledMetric).hex()

  info: (feature) ->
    name: feature.properties.name,
    people_in_household: feature.properties.people_in_household,
    person_term: if feature.properties.people_in_household == 1 then 'person' else 'people',
    datetime: feature.properties.datetime,
    pregnant_women: feature.properties.pregnant_women,
    children_under_five: feature.properties.children_under_five,
    llins: feature.properties.llins,
    net_source: feature.properties.net_source,
    other_net_source: feature.properties.other_net_source,
    sleeping_spaces: feature.properties.sleeping_spaces,
    llins_good: feature.properties.llins_good,
    llins_total: feature.properties.llins_total,
    llins_installed: feature.properties.llins_installed,


root.surveyResponseLayer = new SurveyResponseLayer()


class ClusterLayer extends NetLayer
  name: 'cluster'
  infoTemplate: Handlebars.compile($("#cluster-template").html())
  markerTemplate: Handlebars.compile($("#real-estate-marker-template").html())
  lowerLayers: if auth.isAuthenticated then [surveyResponseLayer] else []
  colorScale: chroma.scale(['green', '#AA0000'])
  allFeatures: L.featureGroup()
  layerMapping: {}
  visibleFeatures: L.featureGroup().addTo(map)
  control: $('#cluster-control')

  init: =>
    @grades = _.object([grade, @getColor(grade)] for grade in [0, 500, 1000, 5000, 10000])
    @gradeUnit = 'meters'
    @gradeDimension = 'Cluster standard deviation'

  load: (parent) =>
    @parent = parent
    if @parent.selectedLayer.feature.properties.statistics.survey_response.count > 0
      super(parent)

  getUrl: =>
    url = geoJsonUrls.cluster[@view]
    url.replace('0', @parent.selectedLayer.feature.id)

  select: (layer) =>
    response_count = layer.feature.properties.statistics.survey_response.count
    max_distance = layer.feature.properties.statistics.centroid.max_distance
    if response_count < @minResponses
    else if response_count > @maxResponses
    else if max_distance > @maxVariance
    else if max_distance < @minVariance
    else
      @visibleFeatures.addLayer(layer)

  getColorMetric: (feature) =>
    feature.properties.statistics.centroid.sd_distance

  getColor: (metric) =>
    scaledMetric = metric / 10000
    @colorScale(scaledMetric).hex()

  getSize: (metric, feature=null) =>
    if metric is null
      metric = feature.properties.statistics.survey_response.count
    Math.log(metric) / 7

  info: (feature) ->
    name: feature.properties.name,
    kind: feature.properties.kind.capitalize(),
    household_count: feature.properties.statistics.survey_response.count,
    household_term: if feature.properties.statistics.survey_response.count == 1 then 'household' else 'households',
    avg_distance: (feature.properties.statistics.centroid.avg_distance / 1000).toFixed(2),
    sd_distance: (feature.properties.statistics.centroid.sd_distance / 1000).toFixed(2),
    max_distance: (feature.properties.statistics.centroid.max_distance / 1000).toFixed(2)


root.clusterLayer = new ClusterLayer()


class IRTestLayer extends NetLayer
  name: 'irTest'
  infoTemplate: Handlebars.compile($("#ir-test-template").html())
  markerTemplate: Handlebars.compile($("#flag-marker-template").html())
  lowerLayers: []
  colorScale: chroma.scale(['#00FF00', 'red'])
  allFeatures: L.featureGroup()
  layerMapping: {}
  visibleFeatures: L.featureGroup().addTo(map)
  control: $('#ir-control')
  classes: {}

  init: =>
    @grades = _.object(
      [grade, @getColor(grade)] \
      for grade in [0, 2, 5, 10, 15, 20, 30, 40, 60, 80, 100])
    @gradeUnit = '% resistant'
    @gradeDimension = 'Insecticide resistance'

  getUrl: =>
    url = geoJsonUrls.irTest
    url.replace('0', @parent.selectedLayer.feature.properties.country)

  select: (layer) =>
    properties = layer.feature.properties
    if properties.mosquito_collection_year_start < @minYear
    else if properties.mosquito_collection_year_end > @maxYear
    else if not @classes.pyrethroids and properties.chemical_class == 'Pyrethroids'
    else if not @classes.organochlorines and properties.chemical_class == 'Organochlorines'
    else if not @classes.organophosphates and properties.chemical_class == 'Organophosphates'
    else if not @classes.carbamates and properties.chemical_class == 'Carbamates'
    else
      @visibleFeatures.addLayer(layer)

  click: (event) =>
    @control.collapse('show')

  getSize: (metric, feature=null) =>
    if metric is null
      metric = feature.properties.ir_test_exposed
    metric * 0.001

  getColorMetric: (feature) =>
    feature.properties.insecticide_resistance

  getColor: (metric) =>
    scaledMetric = Math.log(metric + 1) / Math.log(101)
    @colorScale(scaledMetric).hex()

  fitBounds: =>

  info: (feature) ->
    insecticide_resistance: feature.properties.insecticide_resistance,
    chemical_class: feature.properties.chemical_class,
    chemical_type: feature.properties.chemical_type,
    country_name: feature.properties.country_name,
    irm_id: feature.properties.irm_id,
    irac_moa: feature.properties.irac_moa,
    irac_moa_code: feature.properties.irac_moa_code,
    ir_mechanism_name: feature.properties.ir_mechanism_name,
    ir_mechanism_status: feature.properties.ir_mechanism_status,
    ir_mechanism_status_code: feature.properties.ir_mechanism_status_code,
    ir_test_method: feature.properties.ir_test_method,
    ir_test_exposed: feature.properties.ir_test_exposed,
    ir_test_mortality: feature.properties.ir_test_mortality,
    insecticide_dosage: feature.properties.insecticide_dosage,
    kdr_frequency: feature.properties.kdr_frequency,
    locality: feature.properties.locality,
    molecular_forms: feature.properties.molecular_forms,
    mosquito_collection_year_start: feature.properties.mosquito_collection_year_start,
    mosquito_collection_period: feature.properties.mosquito_collection_period,
    mosquito_collection_year_end: if feature.properties.mosquito_collection_year_end != feature.properties.mosquito_collection_year_start then feature.properties.mosquito_collection_year_end else null,
    r_number: feature.properties.r_number,
    r_code: feature.properties.r_code,
    reference_name: feature.properties.reference_name,
    reference_type: feature.properties.reference_type,
    resistance_status: feature.properties.resistance_status,
    synergist_test_mortality: feature.properties.synergist_test_mortality,
    synergist_type: feature.properties.synergist_type,
    synergist_dosage: feature.properties.synergist_dosage,
    url: feature.properties.url,
    vector_species: feature.properties.vector_species,


root.irTestLayer = new IRTestLayer()


class NetGapLayer extends NetLayer
  name: 'netGap'
  infoTemplate: Handlebars.compile($("#netgap-template").html())
  lowerLayers: []
  gradeDimension: 'Net Gap'
  colorScale: chroma.scale(['orange', 'red'])
  allFeatures: L.featureGroup()
  layerMapping: {}
  visibleFeatures: L.featureGroup().addTo(map)
  control: $('#netgap-control')
  classes: {}
  view: 'hull'

  init: =>
    if @mode == 'absolute'
      @gradeUnit = ' nets'
      @getColorMetric = (feature) =>
        netgap = feature.properties.netgap[@year]
        netgap.gap
      @getColor = (metric) =>
        if metric == 0
          return '#009900'
        metric = metric / (3*10**7)
        @colorScale(metric).hex()
      @grades = _.object(
        [grade, @getColor(grade)] \
        for grade in [0, 1*10**6, 5*10**6, 1*10**7, 5*10**7])
    else if @mode == 'relative'
      @gradeUnit = ' of nets'
      @getColorMetric = (feature) =>
        netgap = feature.properties.netgap[@year]
        netgap.gap / netgap.needed
      @getColor = (metric) =>
        if metric == 0
          return '#009900'
        @colorScale(metric).hex()
      @grades = _.object(
        [grade, @getColor(grade)] \
        for grade in [0, 0.2, 0.4, 0.6, 0.8])

  getUrl: =>
    geoJsonUrls.netGap

  select: (layer) =>
    props = layer.feature.properties
    if @mode == 'hidden'
    else if @year not of props.netgap
    else if 'gap' not of props.netgap[@year]
    else if @mode == 'relative' and 'needed' not of props.netgap[@year]
    else
      @visibleFeatures.addLayer(layer)

  click: (event) =>
    @control.collapse('show')

  fitBounds: =>

  info: (feature) ->
    props = feature.properties
    country: props.name,
    netgap: props.netgap


root.netGapLayer = new NetGapLayer()


class IndicatorLayer extends NetLayer
  name: 'indicator'
  infoTemplate: Handlebars.compile($("#indicator-template").html())
  lowerLayers: []
  gradeDimension: 'Indicator'
  allFeatures: L.featureGroup()
  layerMapping: {}
  visibleFeatures: L.featureGroup().addTo(map)
  control: $('#indicator-control')
  classes: {}
  view: 'hull'

  init: =>
    if @mode == 'gdp_pc_mean' or @mode == 'gdp_pc_median'
      @gradeUnit = ' $ PPP'
    else
      @gradeUnit = ''

  valueRange: =>
    minimum = 1000
    maximum = -1000
    @allFeatures.eachLayer (layer) =>
      indicator = layer.feature.properties.indicator
      for year, value of indicator[@mode]
        if value < minimum
          minimum = value
        if value > maximum
          maximum = value
    [minimum, maximum]

  updateSlider: =>
    minimum = 3000
    maximum = 1000
    @allFeatures.eachLayer (layer) =>
      indicator = layer.feature.properties.indicator
      for year, value of indicator[@mode]
        year = parseInt(year, 10)
        if year < minimum
          minimum = year
        if year > maximum
          maximum = year
    if minimum == maximum
      $('#indicator-year-slider').slider(disabled: true)
    else
      $('#indicator-year-slider').slider(disabled: false)
    $('#indicator-year-slider').slider(min: minimum, max: maximum)
    $('#indicator-year').text($('#indicator-year-slider').slider('value'))
    @year = $('#indicator-year-slider').slider('value')

  updateMethods: =>
    [minimum, maximum] = @valueRange()
    difference = maximum - minimum
    @colorScale = chroma.scale(['#ccccff', '#0000ff'])
    @getColor = (metric) =>
      metric = (metric - minimum) / maximum
      @colorScale(metric).hex()
    @grades = _.object(
      [grade, @getColor(grade)] \
      for grade in [minimum, minimum + (difference / 3), minimum + 2 * (difference / 3), maximum])

  viewData: =>
    @updateSlider()
    @updateMethods()
    super()

  getColorMetric: (feature) =>
    if @mode of feature.properties.indicator and \
        @year of feature.properties.indicator[@mode]
      feature.properties.indicator[@mode][@year]

  getUrl: =>
    geoJsonUrls.indicator

  select: (layer) =>
    indicator = layer.feature.properties.indicator
    if @mode == 'hidden'
    else if @mode not of indicator
    else if @year not of indicator[@mode]
    else
      @visibleFeatures.addLayer(layer)

  click: (event) =>
    @control.collapse('show')

  fitBounds: =>

  info: (feature) ->
    years: feature.properties.indicator[@mode],
    country: feature.properties.name

  viewInfo: (feature) =>
    super(feature)
    title = $('h5.indicator-mode')
    title.text($("input[value='#{@mode}']").parent().text())

root.indicatorLayer = new IndicatorLayer()


class DistributionLayer extends NetLayer
  name: 'distribution'
  infoTemplate: Handlebars.compile($("#distribution-template").html())
  markerTemplate: Handlebars.compile($("#shield-marker-template").html())
  colorScale: chroma.scale(['#9999ff', '#000044'])
  lowerLayers: [clusterLayer, irTestLayer]
  allFeatures: L.featureGroup()
  layerMapping: {}
  visibleFeatures: L.featureGroup().addTo(map)
  control: $('#distribution-control')

  init: =>
    @grades = _.object(
      [grade, @getColor(grade)] \
      for grade in [0, 1*10**4, 2*10**4, 3*10**4, 4*10**4, 5*10**4])
    @gradeUnit = 'responses'
    @gradeDimension = 'Survey responses<br />per distribution'

  getUrl: =>
    geoJsonUrls.distribution[@view]

  getColorMetric: (feature) =>
    if feature.properties.statistics.survey_response
      metric = feature.properties.statistics.survey_response.count
    else
      metric = 0

  getColor: (metric) =>
    scaledMetric = metric / (5*10**4)
    @colorScale(scaledMetric).hex()

  getSize: (metric, feature=null) =>
    if metric is null
      metric = feature.properties.net_count
    metric / 200000

  select: (layer) =>
    if @selectedLayer and @selectedLayer == layer
      @visibleFeatures.addLayer(layer)
      return
    if @selectedBounds and @selectedBounds.pad(10).contains(
        layer.feature.geometry.coordinates.reverse())
      # Note: only works for curated coordinates.
      @visibleFeatures.addLayer(layer)
      return
    props = layer.feature.properties
    responses = 0
    if props.statistics.survey_response
      responses = props.statistics.survey_response.count
    if props.start < "#{@minYear}-01-01"
    else if props.end > "#{@maxYear}-12-31"
    else if responses < @minResponses
    else if props.net_count < @minNets
    else
      @visibleFeatures.addLayer(layer)

  fitBounds: =>

  info: (feature) ->
    statistics = feature.properties.statistics
    if not statistics.survey_response
      statistics.survey_response = count: 0
      statistics.centroid =
        'average distance': 0,
        'sd distance': 0,
        'maximum distance': 0
    name: feature.properties.name,
    start: feature.properties.start,
    end: feature.properties.end,
    url: feature.properties.url,
    net_count: feature.properties.net_count,
    partner: feature.properties.partner,
    image: feature.properties.image,
    response_count: feature.properties.statistics.survey_response.count,
    response_term: if feature.properties.statistics.survey_response.count == 1 then 'response' else 'responses',
    avg_distance: (feature.properties.statistics.centroid.avg_distance / 1000).toFixed(2),
    sd_distance: (feature.properties.statistics.centroid.sd_distance / 1000).toFixed(2),
    max_distance: (feature.properties.statistics.centroid.max_distance / 1000).toFixed(2)


root.distributionLayer = new DistributionLayer()


# Overlay controls

## Initialization

root.maplayer = maplayers[$('#map-control .layers :checked').val()]

netGapLayer.year = $('#netgap-year-slider').slider('value')
indicatorLayer.year = $('#indicator-year-slider').slider('value')

distributionLayer.view = $('#distribution-control .views :checked').val()
for elem in $('#distribution-control .filters :checked')
  distributionLayer[elem.value] = true
distributionLayer.minResponses = $('#distribution-responses-slider').slider('value')
distributionLayer.minNets = $('#distribution-nets-slider').slider('value')
distributionLayer.minYear = $('#distribution-years-slider').slider('values', 0)
distributionLayer.maxYear = $('#distribution-years-slider').slider('values', 1)

clusterLayer.view = $('#cluster-control .views :checked').val()
for elem in $('#cluster-control .filters :checked')
  clusterLayer[elem.value] = true
clusterLayer.minResponses = $('#cluster-count-slider').slider('values', 0)
clusterLayer.maxResponses = $('#cluster-count-slider').slider('values', 1)
clusterLayer.minVariance = $('#cluster-variance-slider').slider('values', 0)
clusterLayer.maxVariance = $('#cluster-variance-slider').slider('values', 1)

for elem in $('#ir-control .filters :checked')
  irTestLayer.classes[elem.value] = true
irTestLayer.minYear = $('#ir-slider').slider('values', 0)
irTestLayer.maxYear = $('#ir-slider').slider('values', 1)

## Event handling

widgets =
  statsbox: 'div#charts',
  infobox: 'div.info'
$('#interface-control').on('change', 'input', ->
  if @checked
    $(widgets[@value]).fadeIn()
  else
    $(widgets[@value]).fadeOut()
)

$('#netgap-control .modes').on('change', ':checked', ->
  netGapLayer.mode = @value
  if not Object.keys(netGapLayer.allFeatures._layers).length
    netGapLayer.load(null)
  netGapLayer.reviewData()
)
$('#netgap-year-slider').on('slidechange', ->
  netGapLayer.year = $(@).slider('value')
  netGapLayer.reviewData()
)

$('#indicator-control .modes').on('change', ':checked', ->
  indicatorLayer.mode = @value
  if not Object.keys(indicatorLayer.allFeatures._layers).length
    indicatorLayer.load(null)
  indicatorLayer.reviewData()
)
$('#indicator-year-slider').on('slidechange', ->
  indicatorLayer.year = $(@).slider('value')
  indicatorLayer.reviewData()
)

$('#distribution-control .views').on('change', ':checked', ->
  distributionLayer.view = @value
  if Object.keys(distributionLayer.allFeatures._layers).length
    distributionLayer.load(null)
)
$('#distribution-control .filters').on('change', 'input', ->
  if @checked
    distributionLayer[@value] = true
  else
    distributionLayer[@value] = false
  distributionLayer.reviewData()
)
$('#distribution-responses-slider').on('slidechange', ->
  distributionLayer.minResponses = $(@).slider('value')
  distributionLayer.reviewData()
)
$('#distribution-nets-slider').on('slidechange', ->
  distributionLayer.minNets = $(@).slider('value')
  distributionLayer.reviewData()
)
$('#distribution-years-slider').on('slidechange', ->
  distributionLayer.minYear = $(@).slider('values', 0)
  distributionLayer.maxYear = $(@).slider('values', 1)
  distributionLayer.reviewData()
)

$('#cluster-control .views').on('change', ':checked', ->
  clusterLayer.view = @value
  if Object.keys(clusterLayer.allFeatures._layers).length
    clusterLayer.load(clusterLayer.parent)
)
$('#cluster-control .filters').on('change', 'input', ->
  if @checked
    clusterLayer[@value] = true
  else
    clusterLayer[@value] = false
  clusterLayer.reviewData()
)
$('#cluster-count-slider').on('slidechange', ->
  clusterLayer.minResponses = $(@).slider('values', 0)
  clusterLayer.maxResponses = $(@).slider('values', 1)
  clusterLayer.reviewData()
)
$('#cluster-variance-slider').on('slidechange', ->
  clusterLayer.minVariance = $(@).slider('values', 0)
  clusterLayer.maxVariance = $(@).slider('values', 1)
  clusterLayer.reviewData()
)

$('#map-control .layers').on('change', ':checked', ->
  for key, layer of maplayers
    map.removeLayer(maplayers[key])
    if key == @value
      map.addLayer(maplayers[key])
      maplayers[key].bringToBack()
)

$('#incidence-control .overlays').on('change', 'input', ->
  layers =
    pfalciparum: pfalciparum,
    pvivax: pvivax,
    eir: eir
  if @checked
    map.addLayer(layers[@value])
  else
    map.removeLayer(layers[@value])
)

$('#ir-control .filters').on('change', 'input', ->
  if @checked
    irTestLayer.classes[@value] = true
  else
    irTestLayer.classes[@value] = false
  irTestLayer.reviewData()
)
$('#ir-slider').on('slidechange', ->
  irTestLayer.minYear = $(@).slider('values', 0)
  irTestLayer.maxYear = $(@).slider('values', 1)
  irTestLayer.reviewData()
)

# Charts

chartKeys = ['malaria_symptoms',
             'malaria_transmission',
             'malaria_prevention',
             'malaria_treatment',
             'preventive_measures',
             'total_usage',
             'children_usage',
             'women_usage',
             'net_source']
root.charts = {}
for key in chartKeys
  root.charts[key] = c3.generate(
    bindto: "##{key.replace('_', '-')}-chart",
    size:
        width: 300,
        height: 250
    bar:
      width: 40,
    axis:
      x:
        type: 'category'
    legend:
        position: 'right'
    data:
      json: [],
      type: 'bar',
      groups: [],
      keys:
        x: 'name',
        value: [],
      labels: true,
      grid:
        y:
          lines: [value: 0]
  )
