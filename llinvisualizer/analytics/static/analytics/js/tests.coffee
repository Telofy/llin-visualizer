describe 'The map', ->

  it 'is defined', ->
    expect(map).toBeDefined()

  it 'has a zoom control', ->
    expect(zoom._initHooksCalled).toBeTruthy()


describe 'The String', ->

  it 'can be capitalized', ->
    expect('sara'.capitalize()).toEqual('Sara')


describe 'The info box', ->

  it 'is defined', ->
    expect(info).toBeDefined()

  it 'has update method', ->
    expect(info.update).toBeDefined()

  it 'can be updated', ->
    string = 'Sara is awesome!'
    info.update(string)
    expect(info._div.innerHTML).toEqual(string)

  it 'has the right classes', ->
    expect(info._div.getAttribute('class')).toEqual('info legend leaflet-control')


describe 'The IR slider', ->

  it 'is defined', ->
    expect($('#ir-slider').length).toBeGreaterThan(0)


describe 'The distribution responses slider', ->

  it 'is defined', ->
    expect($('#distribution-responses-slider').length).toBeGreaterThan(0)


describe 'The distribution nets slider', ->

  it 'is defined', ->
    expect($('#distribution-nets-slider').length).toBeGreaterThan(0)


describe 'The controls', ->

  it 'are defined', ->
    expect($('#distribution-control').length).toBeGreaterThan(0)
    expect($('#cluster-control').length).toBeGreaterThan(0)
    expect($('#ir-control').length).toBeGreaterThan(0)
    expect($('#incidence-control').length).toBeGreaterThan(0)

  it 'are expanded and collapsed respectively', ->
    expect($('#distribution-control')[0].classList.contains('in')).not.toBeTruthy()
    expect($('#cluster-control')[0].classList.contains('in')).not.toBeTruthy()
    expect($('#ir-control')[0].classList.contains('in')).not.toBeTruthy()
    expect($('#incidence-control')[0].classList.contains('in')).not.toBeTruthy()

  it 'react to clicks', ->
    $('#cluster-heading').click()
    expect($('#cluster-control')[0].classList.contains('collapsing')).toBeTruthy()


describe 'The DistributionLayer', ->

  points = {
    "crs": {
      "type": "link",
      "properties": {
        "href": "http://spatialreference.org/ref/epsg/4326/",
        "type": "proj4"
      }
    },
    "type": "FeatureCollection",
    "features": [
      {
        "geometry": {
          "coordinates": [
            20.7575,
            -6.42367
          ],
          "type": "Point"
        },
        "id": 89,
        "properties": {
          "status": "Distribution complete",
          "statistics": {
            "preventive measures": {
              "burning": 988.0,
              "covering": 1770.0,
              "na": 2078.0,
              "mowing": 6256.0,
              "lotion": 15814.0,
              "water": 15690.0,
              "spraying": 5667.0,
              "other": 470.0,
              "cleaning": 3045.0,
              "kidney": 193.0
            },
            "malaria_symptoms": {
              "fever": 27191.0,
              "pain": 14609.0,
              "na": 1144.0,
              "diarrhea": 674.0,
              "vomiting": 6107.0,
              "other": 384.0,
              "tiredness": 10798.0,
              "no appetite": 1095.0,
              "headache": 17107.0
            },
            "survey_response": {
                "count": 36102.0
            },
            "malaria_transmission": {
              "sick people": 523.0,
              "dirt": 2663.0,
              "sun": 2714.0,
              "witchcraft": 1204.0,
              "fruit": 1525.0,
              "other": 492.0,
              "mosquito": 20309.0,
              "na": 1634.0,
              "unsafe water": 4498.0
            },
            "malaria_prevention": {
              "contraceptive": 1294.0,
              "herb": 1257.0,
              "pipeline": 15408.0,
              "drug": 1209.0,
              "weed": 10146.0,
              "na": 1408.0,
              "other": 223.0,
              "insecticide": 327.0,
              "llin": 18158.0
            },
            "centroid": {
              "max_distance": 45229.507666397,
              "sd_distance": 8796.46180349886,
              "avg_distance": 10220.863861163
            },
            "usage": {
              "children_under_five": 51767.0,
              "pregnant_women under net last night": 1508.0,
              "children_under_net_last_night": 4244.0,
              "people_under_net_last_night": 12127.0,
              "people_in_household": 217912.0,
              "pregnant_women": 22059.0
            },
            "malaria_treatment": {
              "aspirine ibuprofen": 647.0,
              "chloroquine": 6324.0,
              "artemisinin": 5589.0,
              "fansidar": 4231.0,
              "other": 384.0,
              "quinine": 16044.0,
              "paracetamol": 6645.0,
              "amodiaquine": 10540.0
            },
            "net source": {
              "health center": 3641.0,
              "ngo": 375.0,
              "distribution": 2228.0,
              "other": 68.0,
              "market": 537.0
            }
          },
          "end": "2014-11-30",
          "name": "Health Zone 1: Tshikapa, Congo (Dem. Rep.)",
          "campaign": 1,
          "net_count": 103880,
          "country": 52,
          "region": "",
          "start": "2014-08-01",
          "video": "//www.youtube.com/embed/AecXL8Ym-VM",
          "url": "http://www.AgainstMalaria.com/Distribution.aspx?DistributionID=671&ProposalID=194",
          "partner": "IMA World Health/DFID",
          "model": "analytics.distribution",
          "image": "http://www.againstmalaria.com/images/00/20/small/20122.jpg"
        },
        "type": "Feature"
      }
    ]
  }

  hulls = {
    "crs": {
      "type": "link",
      "properties": {
        "href": "http://spatialreference.org/ref/epsg/4326/",
        "type": "proj4"
      }
    },
    "type": "FeatureCollection",
    "features": [
      {
        "geometry": {
          "coordinates": [
            [
              [
                20.8746736089,
                -7.0529447875
              ],
              [
                20.62721805,
                -6.959286647
              ],
              [
                20.16413303,
                -6.626164921
              ],
              [
                20.52134366,
                -5.450045912
              ],
              [
                21.3004963778,
                -6.8905740359
              ],
              [
                21.13236698,
                -7.003986111
              ],
              [
                20.8746736089,
                -7.0529447875
              ]
            ]
          ],
          "type": "Polygon"
        },
        "id": 90,
        "properties": {
          "campaign": 1,
          "country": 52,
          "end": "2014-11-30",
          "image": "http://www.againstmalaria.com/images/00/20/small/20076.jpg",
          "model": "analytics.distribution",
          "name": "Health Zone 3: Kanzala, Congo (Dem. Rep.)",
          "net_count": 69233,
          "partner": "IMA World Health/DFID",
          "region": "",
          "start": "2014-08-01",
          "statistics": {
            "centroid": {
              "avg_distance": 7005.93586882138,
              "max_distance": 99474.435219017,
              "sd_distance": 8898.50330442952
            },
            "malaria_prevention": {
              "contraceptive": 873.0,
              "drug": 896.0,
              "herb": 661.0,
              "insecticide": 347.0,
              "llin": 14418.0,
              "na": 958.0,
              "other": 181.0,
              "pipeline": 10417.0,
              "weed": 7876.0
            },
            "malaria_symptoms": {
              "diarrhea": 492.0,
              "fever": 20311.0,
              "headache": 12905.0,
              "na": 632.0,
              "no appetite": 586.0,
              "other": 223.0,
              "pain": 11200.0,
              "tiredness": 8396.0,
              "vomiting": 5418.0
            },
            "malaria_transmission": {
              "dirt": 1367.0,
              "fruit": 531.0,
              "mosquito": 16197.0,
              "na": 951.0,
              "other": 29.0,
              "sick people": 85.0,
              "sun": 1629.0,
              "unsafe water": 3443.0,
              "witchcraft": 549.0
            },
            "malaria_treatment": {
              "amodiaquine": 7042.0,
              "artemisinin": 3855.0,
              "aspirine ibuprofen": 481.0,
              "chloroquine": 5210.0,
              "fansidar": 3355.0,
              "other": 199.0,
              "paracetamol": 5348.0,
              "quinine": 13034.0
            },
            "net source": {
              "distribution": 1330.0,
              "health center": 1766.0,
              "market": 779.0,
              "ngo": 259.0,
              "other": 108.0
            },
            "preventive measures": {
              "burning": 656.0,
              "cleaning": 2306.0,
              "covering": 1193.0,
              "kidney": 194.0,
              "lotion": 12227.0,
              "mowing": 4597.0,
              "na": 1305.0,
              "other": 199.0,
              "spraying": 4347.0,
              "water": 11181.0
            },
            "survey_response": {
              "count": 24851.0
            },
            "usage": {
              "children_under_five": 25031.0,
              "children_under_net_last_night": 2678.0,
              "people_in_household": 145162.0,
              "people_under_net_last_night": 6948.0,
              "pregnant_women": 3305.0,
              "pregnant_women under net last night": 844.0
            }
          }
        },
        "type": "Feature"
      }
    ]
  }

  beforeEach ->
    L.geoJson(points, distributionLayer.geoJsonOptions('point')).eachLayer(
      (layer) -> distributionLayer.allFeatures.addLayer(layer))
    L.geoJson(hulls, distributionLayer.geoJsonOptions('hull')).eachLayer(
      (layer) -> distributionLayer.allFeatures.addLayer(layer))

  afterEach ->
    distributionLayer.clearData()
    distributionLayer.hideData()

  it 'returns GeoJson options', ->
    expect(distributionLayer.geoJsonOptions()).toEqual(
      jasmine.objectContaining({onEachFeature: jasmine.anything()}))

  it 'returns style preferences', ->
    feature = properties: statistics: 'survey_response': count: 1000
    expect(distributionLayer.style(feature)).toEqual(
      jasmine.objectContaining(
        weight: 0,
        opacity: jasmine.anything(),
        color: jasmine.anything(),
        fillOpacity: jasmine.anything(),
        fillColor: jasmine.anything()))

  it 'can load data', ->
    expect(Object.keys(distributionLayer.allFeatures._layers).length).toEqual(2)

  it 'can clear data', ->
    distributionLayer.clearData()
    expect(Object.keys(distributionLayer.allFeatures._layers)).toEqual([])

  it 'can view data', ->
    distributionLayer.viewData()
    expect(Object.keys(distributionLayer.visibleFeatures._layers).length).toEqual(2)

  it 'can hide data', ->
    distributionLayer.viewData()
    expect(Object.keys(distributionLayer.visibleFeatures._layers).length).toEqual(2)
    distributionLayer.hideData()
    expect(Object.keys(distributionLayer.visibleFeatures._layers)).toEqual([])

  it 'can review data', ->
    distributionLayer.reviewData()
    expect(Object.keys(distributionLayer.visibleFeatures._layers).length).toEqual(2)

  it 'can select data', ->
    expect(Object.keys(distributionLayer.visibleFeatures._layers)).toEqual([])
    key = Object.keys(distributionLayer.allFeatures._layers)[0]
    layer = distributionLayer.allFeatures._layers[key]
    distributionLayer.select(layer)
    expect(Object.keys(distributionLayer.visibleFeatures._layers).length).toEqual(1)

  it 'can produce an icon', ->
    feature = properties:
      statistics: 'survey_response': count: 1000
      net_count: 100000
    icon = distributionLayer.getIcon(feature)
    expect(icon.options.iconSize[0]).toBeGreaterThan(19)
    expect(icon.options.iconSize[1]).toBeGreaterThan(19)
    expect(icon.options.html).toContain('svg')

  it 'can produce a marker', ->
    feature = properties:
      statistics: 'survey_response': count: 1000
      net_count: 100000
    latlng = L.latLng(50.5, 30.5)
    marker = distributionLayer.pointToLayer(feature, latlng)
    expect(marker.options.icon.options.html).toContain('svg')

  it 'can update the info panel', ->
    feature = properties:
      statistics:
        'survey_response': count: 1000
        'centroid':
          'avg_distance': 7005.93586882138,
          'max_distance': 99474.435219017,
          'sd_distance': 8898.50330442952
      net_count: 970404
    marker = distributionLayer.viewInfo(feature)
    expect(info._div.innerHTML).toContain('970404')

  it 'can reset the info panel', ->
    info.update('Narf')
    distributionLayer.resetInfo()
    expect(info._div.innerHTML).not.toContain('Narf')
    expect(info._div.innerHTML).toContain('responses')

  it 'can highlight hulls', ->
    distributionLayer.view = 'hull'
    distributionLayer.viewData()
    hull = distributionLayer.layerMapping[90]
    distributionLayer.highlight(target: hull)
    expect(hull.options.weight).toEqual(3)

  it 'can reset hull highlight', ->
    distributionLayer.view = 'hull'
    distributionLayer.viewData()
    hull = distributionLayer.layerMapping[90]
    distributionLayer.highlight(target: hull)
    distributionLayer.resetHighlight(target: hull)
    expect(hull.options.weight).toEqual(0)

  it 'can highlight points', ->
    distributionLayer.view = 'point'
    distributionLayer.viewData()
    point = distributionLayer.layerMapping[89]
    distributionLayer.highlight(target: point)
    expect($(point._icon).find('.icon-path').css('stroke-width')).toEqual('7px')

  it 'can reset point highlight', ->
    distributionLayer.view = 'point'
    distributionLayer.viewData()
    point = distributionLayer.layerMapping[89]
    distributionLayer.highlight(target: point)
    distributionLayer.resetHighlight(target: point)
    expect($(point._icon).find('.icon-path').css('stroke-width')).toEqual('0px')

  it 'can draw up a map key', ->
    legend = distributionLayer.legend()
    expect(legend).toContain('0&thinsp;responses')
    expect(legend).toContain('Survey responses')

  it 'produces URLs', ->
    expect(distributionLayer.getUrl()).toEqual('/analytics/distributions.points.geojson')

  it 'determines colors', ->
    hull = distributionLayer.layerMapping[90].feature
    colorMetric = distributionLayer.getColor(
      hull.properties.statistics.survey_response.count)
    expect(colorMetric).toEqual('#4c4ca2')

  it 'determines sizes', ->
    point = distributionLayer.layerMapping[89].feature
    sizeMetric = distributionLayer.getSize(point.properties.net_count)
    sizeFeature = distributionLayer.getSize(null, point)
    expect(sizeFeature).toBeGreaterThan(0.1)
    expect(sizeFeature).toEqual(sizeMetric)

  it 'returns feature info', ->
    point = distributionLayer.layerMapping[89].feature
    featureInfo = distributionLayer.info(point)
    html = distributionLayer.infoTemplate(featureInfo)
    expect(html).toContain('Distance to Centroid')
    expect(html).toContain('103880 nets')
    expect(html).toContain('36102 survey responses')
    expect(html).toContain('<th>Standard deviation</th><td>8.80 km</td>')


describe 'The IRTestLayer', ->

  points = {
    "crs": {
      "properties": {
        "href": "http://spatialreference.org/ref/epsg/4326/",
        "type": "proj4"
      },
      "type": "link"
    },
    "features": [
      {
        "geometry": {
          "coordinates": [
            26.95,
            -11.03333
          ],
          "type": "Point"
        },
        "id": 35020,
        "properties": {
          "chemical_class": "Pyrethroids",
          "chemical_type": "Bendiocarb",
          "country": 52,
          "country_name": "Democratic Republic of Congo",
          "insecticide_dosage": "0.1",
          "insecticide_resistance": 0.0,
          "ir_mechanism_name": "",
          "ir_mechanism_status": "",
          "ir_mechanism_status_code": "",
          "ir_test_exposed": 80,
          "ir_test_method": "WHO tube assay",
          "ir_test_mortality": 100.0,
          "irac_moa": "Acetylcholinesterase inhibitors",
          "irac_moa_code": "1A",
          "irm_id": 11383,
          "kdr_frequency": "",
          "latitude": -11.03333,
          "locality": "Kapolowe",
          "longitude": 26.95,
          "model": "analytics.irtest",
          "molecular_forms": "",
          "mosquito_collection_period": "2012",
          "mosquito_collection_year_end": 2012,
          "mosquito_collection_year_start": 2012,
          "r_code": "S",
          "r_number": 3,
          "reference_name": "PMI, 2014",
          "reference_type": "Unpublished data",
          "resistance_status": "Susceptibility",
          "synergist_dosage": "",
          "synergist_test_mortality": null,
          "synergist_type": "",
          "url": "",
          "vector_species": "Anopheles gambiae sl"
        },
        "type": "Feature"
      }
    ]
  }

  beforeEach ->
    L.geoJson(points, irTestLayer.geoJsonOptions('point')).eachLayer(
      (layer) -> irTestLayer.allFeatures.addLayer(layer))

  afterEach ->
    irTestLayer.clearData()
    irTestLayer.hideData()

  it 'can select data', ->
    expect(Object.keys(irTestLayer.visibleFeatures._layers)).toEqual([])
    key = Object.keys(irTestLayer.allFeatures._layers)[0]
    layer = irTestLayer.allFeatures._layers[key]
    irTestLayer.select(layer)
    expect(Object.keys(irTestLayer.visibleFeatures._layers).length).toEqual(1)

  it 'produces URLs', ->
    irTestLayer.parent = selectedLayer: feature: properties: country: 52
    expect(irTestLayer.getUrl()).toEqual('/analytics/countries/52/ir-tests.geojson')

  it 'determines colors', ->
    point = irTestLayer.layerMapping[35020].feature
    colorMetric = irTestLayer.getColor(100 - point.properties.ir_test_mortality)
    colorFeature = irTestLayer.getColor(null, point)
    expect(colorFeature[0]).toEqual('#')
    expect(colorFeature).toEqual(colorMetric)

  it 'determines sizes', ->
    point = irTestLayer.layerMapping[35020].feature
    sizeMetric = irTestLayer.getSize(point.properties.ir_test_exposed)
    sizeFeature = irTestLayer.getSize(null, point)
    expect(sizeFeature).toBeGreaterThan(0)
    expect(sizeFeature).toEqual(sizeMetric)

  it 'returns feature info', ->
    point = irTestLayer.layerMapping[35020].feature
    featureInfo = irTestLayer.info(point)
    html = irTestLayer.infoTemplate(featureInfo)
    expect(html).toContain('Insecticide Resistance')
    expect(html).toContain('(Susceptibility, 0&thinsp;% insecticide resistant)')


describe 'The ClusterLayer', ->

  points = {
    "crs": {
      "properties": {
        "href": "http://spatialreference.org/ref/epsg/4326/",
        "type": "proj4"
      },
      "type": "link"
  },
    "features": [
      {
        "geometry": {
          "coordinates": [
            21.233019246025002,
            -7.153917442085
          ],
          "type": "Point"
        },
        "id": 30551,
        "properties": {
          "distribution": 83,
          "kind": "village",
          "model": "analytics.cluster",
          "name": "1, Tshitepa",
          "statistics": {
            "centroid": {
              "avg_distance": 0.0,
              "max_distance": 0.0,
              "sd_distance": 90
            },
            "malaria_prevention": {
              "contraceptive": 0.0,
              "drug": 0.0,
              "herb": 0.0,
              "insecticide": 0.0,
              "llin": 0.0,
              "na": 0.0,
              "other": 0.0,
              "pipeline": 0.0,
              "weed": 1.0
            },
            "malaria_symptoms": {
              "diarrhea": 0.0,
              "fever": 1.0,
              "headache": 0.0,
              "na": 0.0,
              "no appetite": 0.0,
              "other": 0.0,
              "pain": 0.0,
              "tiredness": 0.0,
              "vomiting": 0.0
            },
            "malaria_transmission": {
              "dirt": 0.0,
              "fruit": 0.0,
              "mosquito": 1.0,
              "na": 0.0,
              "other": 0.0,
              "sick_people": 0.0,
              "sun": 0.0,
              "unsafe_water": 0.0,
              "witchcraft": 0.0
            },
            "malaria_treatment": {
              "amodiaquine": 0.0,
              "artemisinin": 0.0,
              "aspirine_ibuprofen": 0.0,
              "chloroquine": 0.0,
              "fansidar": 0.0,
              "other": 0.0,
              "paracetamol": 0.0,
              "quinine": 1.0
            },
            "net_source": {
              "distribution": 0.0,
              "health_center": 1.0,
              "market": 0.0,
              "ngo": 0.0,
              "other": 0.0
            },
            "preventive_measures": {
              "burning": 0.0,
              "cleaning": 0.0,
              "covering": 0.0,
              "kidney": 0.0,
              "lotion": 1.0,
              "mowing": 0.0,
              "na": 0.0,
              "other": 0.0,
              "spraying": 0.0,
              "water": 0.0
            },
            "survey_response": {
              "count": 120.0
            },
            "usage": {
              "children_under_five": 1.0,
              "children_under_net_last_night": 0.0,
              "people_in_household": 4.0,
              "people_under_net_last_night": 0.0,
              "pregnant_women": 0.0,
              "pregnant_women_under_net_last_night": null
            }
          }
        },
        "type": "Feature"
      }
    ]
  }

  beforeEach ->
    L.geoJson(points, clusterLayer.geoJsonOptions('point')).eachLayer(
      (layer) -> clusterLayer.allFeatures.addLayer(layer))

  afterEach ->
    clusterLayer.clearData()
    clusterLayer.hideData()

  it 'can select data', ->
    expect(Object.keys(clusterLayer.visibleFeatures._layers)).toEqual([])
    key = Object.keys(clusterLayer.allFeatures._layers)[0]
    layer = clusterLayer.allFeatures._layers[key]
    clusterLayer.select(layer)
    expect(Object.keys(clusterLayer.visibleFeatures._layers).length).toEqual(1)

  it 'produces URLs', ->
    clusterLayer.parent = selectedLayer: feature: id: 30551
    expect(clusterLayer.getUrl()).toEqual('/analytics/distributions/30551/clusters.centroids.geojson')

  it 'determines colors', ->
    point = clusterLayer.layerMapping[30551].feature
    colorMetric = clusterLayer.getColor(point.properties.statistics.centroid.sd_distance)
    expect(colorMetric).toEqual('#017e00')

  it 'determines sizes', ->
    point = clusterLayer.layerMapping[30551].feature
    sizeMetric = clusterLayer.getSize(point.properties.statistics.survey_response.count)
    sizeFeature = clusterLayer.getSize(null, point)
    expect(sizeFeature).toBeGreaterThan(0.1)
    expect(sizeFeature).toEqual(sizeMetric)

  it 'returns feature info', ->
    point = clusterLayer.layerMapping[30551].feature
    featureInfo = clusterLayer.info(point)
    html = clusterLayer.infoTemplate(featureInfo)
    expect(html).toContain('Village')
    expect(html).toContain('120 households')
    expect(html).toContain('<th>Standard deviation</th><td>0.09 km</td>')


describe 'The SurveyResponseLayer', ->

  points = {
    "crs": {
      "properties": {
        "href": "http://spatialreference.org/ref/epsg/4326/",
        "type": "proj4"
      },
      "type": "link"
    },
    "features": [
      {
        "geometry": {
          "coordinates": [
            21.2976151549,
            -6.7406232146,
            637.4771015443
          ],
          "type": "Point"
        },
        "id": 477809,
        "properties": {
          "accuracy": 4.0,
          "age": 42,
          "all_children_under_net_last_night": true,
          "altitude": 637.4771015443,
          "at_least_one_llin": null,
          "at_least_one_net": false,
          "child_benefit_from_drug": true,
          "child_fever_last_two_weeks": true,
          "child_healthcenter": true,
          "child_medicine": "Artemisinin",
          "children_under_five": 2,
          "cluster": 32661,
          "datetime": null,
          "day_of_distribution": null,
          "device_id": "359909050851202",
          "distribution": 84,
          "district_name": "Tshikapa",
          "end": null,
          "fansidar_during_pregnancy": null,
          "health_area_name": "Kalumbu",
          "health_zone_name": "Kamuesha",
          "house_number": "64",
          "household": null,
          "instance_id": "uuid:5227ca84-a1f6-4f35-850c-a835a0b869b1",
          "latitude": -6.7406232146,
          "legal_script": true,
          "llins": null,
          "llins_brand": "Olyset",
          "llins_good": 0,
          "llins_installed": 2,
          "llins_packaging_returned": 2,
          "llins_photo": "http://aggregate.defaultdomain/view/binaryData?blobKey=AMF_Enregistration_V3_Final%5B%40version%3Dnull+and+%40uiVersion%3Dnull%5D%2FAMF_Enregistration_V3_Final%5B%40key%3Duuid%3A5227ca84-a1f6-4f35-850c-a835a0b869b1%5D%2Flegales_non%3Adistribution%3Aphoto_installee",
          "llins_returned": 0,
          "llins_total": 2,
          "longitude": 21.2976151549,
          "malaria_other_symptoms": "",
          "malaria_other_transmission": "",
          "malaria_other_treatment": "",
          "malaria_prevention": [
            "sleep under mosquito net impregnated with insecticide"
          ],
          "malaria_symptoms": [
            "fever / high temperature"
          ],
          "malaria_transmission": [
            "being bitten by an infected female mosquito"
          ],
          "malaria_treatment": [
            "Paracetamol"
          ],
          "model": "analytics.surveyresponse",
          "name": "",
          "net_source": "",
          "other_net_source": "",
          "other_preventive_measures": "",
          "people_in_household": 7,
          "people_under_net_last_night": null,
          "phone": "",
          "phone_id": 147,
          "pregnant_women": 0,
          "pregnant_women_under_net_last_night": null,
          "preventive_measures": [
            "Use mosquito repellent lotion"
          ],
          "province_name": "Kasa\u00ef",
          "random": -200000000,
          "sex": "male",
          "signature": "http://aggregate.defaultdomain/view/binaryData?blobKey=AMF_Enregistration_V3_Final%5B%40version%3Dnull+and+%40uiVersion%3Dnull%5D%2FAMF_Enregistration_V3_Final%5B%40key%3Duuid%3A5227ca84-a1f6-4f35-850c-a835a0b869b1%5D%2Fidentification%3Asignature_of_chef",
          "sleeping_spaces": 2,
          "start": null,
          "summary": "",
          "village_name": "Kalumbu"
        },
        "type": "Feature"
      }
    ]
  }

  beforeEach ->
    L.geoJson(points, surveyResponseLayer.geoJsonOptions('point')).eachLayer(
      (layer) -> surveyResponseLayer.allFeatures.addLayer(layer))

  afterEach ->
    surveyResponseLayer.clearData()
    surveyResponseLayer.hideData()

  it 'can select data', ->
    expect(Object.keys(surveyResponseLayer.visibleFeatures._layers)).toEqual([])
    key = Object.keys(surveyResponseLayer.allFeatures._layers)[0]
    layer = surveyResponseLayer.allFeatures._layers[key]
    surveyResponseLayer.select(layer)
    expect(Object.keys(surveyResponseLayer.visibleFeatures._layers).length).toEqual(1)

  it 'produces URLs', ->
    surveyResponseLayer.parent = selectedLayer: feature: id: 477809
    expect(surveyResponseLayer.getUrl())
      .toEqual('/analytics/clusters/477809/survey-responses.geojson')

  it 'determines colors', ->
    point = surveyResponseLayer.layerMapping[477809].feature
    colorMetric = surveyResponseLayer.getColor(point.properties.llins_total)
    expect(colorMetric).toEqual('#7a7ad9')

  it 'determines sizes', ->
    point = surveyResponseLayer.layerMapping[477809].feature
    sizeMetric = surveyResponseLayer.getSize(point.properties.people_in_household)
    sizeFeature = surveyResponseLayer.getSize(null, point)
    expect(sizeFeature).toBeGreaterThan(0.1)
    expect(sizeFeature).toEqual(sizeMetric)

  it 'returns feature info', ->
    point = surveyResponseLayer.layerMapping[477809].feature
    featureInfo = surveyResponseLayer.info(point)
    html = surveyResponseLayer.infoTemplate(featureInfo)
    expect(html).toContain('Survey Response')
    expect(html).toContain('7 people')
    expect(html).toContain('<th>Children under five</th><td>2</td>')
