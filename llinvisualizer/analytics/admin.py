# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from django.contrib import admin
from . import models


admin.site.site_header = 'LLIN Visualizer administration'


class SurveyResponseAdmin(admin.ModelAdmin):
    """Settings for the admin interface for survey responses."""

    list_display = ['name', 'village_name', 'health_zone_name', 'health_area_name',
                    'district_name', 'province_name', 'datetime']
    search_fields = ['name', 'village_name', 'health_zone_name', 'health_area_name',
                    'district_name', 'province_name']
    list_filter = ['health_zone_name', 'district_name', 'province_name', 'health_area_name']


class HouseholdAdmin(admin.ModelAdmin):
    """Settings for the admin interface for households."""

    pass


class CountryAdmin(admin.ModelAdmin):
    """Settings for the admin interface for countries."""

    list_display = ['name', 'alpha2', 'alpha3', 'numeric']
    search_fields = ['name', 'alpha2', 'alpha3', 'numeric']
    ordering = ['name']


class DistributionCampaignAdmin(admin.ModelAdmin):
    """Settings for the admin interface for distribution campaigns."""

    list_display = ['name']
    search_fields = ['name', 'description', 'url']
    ordering = ['name']


class DistributionAdmin(admin.ModelAdmin):
    """Settings for the admin interface for distributions."""

    list_display = ['name', 'campaign', 'net_count', 'partner', 'start', 'end']
    search_fields = ['name', 'region', 'partner', 'country__name', 'url']
    readonly_fields = ['centroid', 'hull']
    list_filter = ['partner', 'country', 'campaign']
    ordering = ['-start']


class ClusterAdmin(admin.ModelAdmin):
    """Settings for the admin interface for clusters."""

    list_display = ['name', 'kind']
    search_fields = ['name', 'kind']
    readonly_fields = ['centroid', 'hull']
    list_filter = ['kind']
    ordering = ['name']


class IRTestAdmin(admin.ModelAdmin):
    """Settings for the admin interface for insecticide resistance tests."""

    list_display = ['locality', 'country', 'ir_test_mortality', 'mosquito_collection_year_end']
    search_fields = ['locality', 'country__name', 'reference_name', 'url']
    list_filter = ['chemical_class', 'chemical_type', 'irac_moa', 'irac_moa_code',
                   'ir_mechanism_name', 'ir_mechanism_status', 'ir_mechanism_status_code',
                   'ir_test_method', 'synergist_type', 'vector_species',
                   'mosquito_collection_year_start', 'mosquito_collection_year_end']
    ordering = ['-mosquito_collection_year_start']


class NetGapAdmin(admin.ModelAdmin):
    """Settings for the admin interface for net gaps."""

    list_display = ['country', 'year', 'estimate_date', 'gap']
    search_fields = ['country', 'year']
    list_filter = ['country', 'year']
    ordering = ['-year', 'country', '-estimate_date']


class StatisticAdmin(admin.ModelAdmin):
    """Settings for the admin interface for statistics."""

    list_display = ['key', 'value', 'aggregate', 'cluster', 'distribution']
    search_fields = ['key', 'value', 'cluster', 'distribution']
    list_filter = ['key', 'value']
    ordering = ['key', 'value']


admin.site.register(models.SurveyResponse, SurveyResponseAdmin)
admin.site.register(models.Household, HouseholdAdmin)
admin.site.register(models.Distribution, DistributionAdmin)
admin.site.register(models.DistributionCampaign, DistributionCampaignAdmin)
admin.site.register(models.Cluster, ClusterAdmin)
admin.site.register(models.Country, CountryAdmin)
admin.site.register(models.IRTest, IRTestAdmin)
admin.site.register(models.NetGap, NetGapAdmin)
admin.site.register(models.Statistic, StatisticAdmin)
