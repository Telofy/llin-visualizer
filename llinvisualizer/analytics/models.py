# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import logging
from collections import defaultdict
from itertools import groupby
from random import randint
from django.contrib.postgres.fields import ArrayField
from django.contrib.gis.db import models

logger = logging.getLogger(__name__)


MALARIA_SYMPTOMS = {
    'fever': {
        'description': 'fever or high temperature',
        'correct': True},
    'headache': {
        'description': 'headache',
        'correct': True},
    'pain': {
        'description': 'pain in the joints / general body pain',
        'correct': True},
    'tiredness': {
        'description': 'tiredness',
        'correct': True},
    'vomiting': {
        'description': 'vomiting',
        'correct': True},
    'no_appetite': {
        'description': 'lacking appetite / bitter taste in mouth',
        'correct': None},
    'diarrhea': {
        'description': 'diarrhea / general pain',
        'correct': None},
    'other': {
        'description': 'other',
        'correct': None},
    'na': {
        'description': 'I do not know / ignored',
        'correct': False},
}

MALARIA_TRANSMISSION = {
    'mosquito': {
        'description': 'being bitten by an infected female mosquito',
        'correct': True},
    'unsafe_water': {
        'description': 'taking a self-catering or dirty water',
        'correct': False},
    'sun': {
        'description': 'wet out being hit by excessive sun',
        'correct': False},
    'dirt': {
        'description': 'dirty environment',
        'correct': False},
    'witchcraft': {
        'description': 'witchcraft',
        'correct': False},
    'fruit': {
        'description': 'eating mangoes or green fruit',
        'correct': False},
    'sick_people': {
        'description': 'living with sick people',
        'correct': False},
    'other': {
        'description': 'other',
        'correct': None},
    'na': {
        'description': 'I do not know / ignored',
        'correct': False},
}

MALARIA_PREVENTION = {
    'pipeline': {
        'description': 'digging stagnant water pipeline',
        'correct': True},
    'weed': {
        'description': 'remove weeds',
        'correct': True},
    'llin': {
        'description': 'sleep under mosquito net impregnated with insecticide',
        'correct': True},
    'contraceptive': {
        'description': 'contraceptive use against mosquitoes',
        'correct': False},
    'herb': {
        'description': 'burning herbs',
        'correct': False},
    'drug': {
        'description': 'take antimalaria drugs',
        'correct': True},
    'insecticide': {
        'description': 'use insecticides',
        'correct': True},
    'other': {
        'description': 'other',
        'correct': None},
    'na': {
        'description': 'I do not know / ignored',
        'correct': False},
}

MALARIA_TREATMENT = {
    'quinine': {
        'description': 'Quinine',
        'correct': True},
    'amodiaquine': {
        'description': 'Amodiaquine',
        'correct': True},
    'fansidar': {
        'description': 'Fansidar',
        'correct': True},
    'artemisinin': {
        'description': 'Artemisinin',
        'correct': True},
    'chloroquine': {
        'description': 'Chloroquine',
        'correct': True},
    'paracetamol': {
        'description': 'Paracetamol',
        'correct': False},
    'aspirine_ibuprofen': {
        'description': 'Aspirine Ibuprofen',
        'correct': False},
    'other': {
        'description': 'other',
        'correct': None},
}

PREVENTIVE_MEASURES = {
    'water': {
        'description': 'removing stagnant water'},
    'lotion': {
        'description': 'use mosquito-repellent lotion'},
    'spraying': {
        'description': 'spraying houses'},
    'mowing': {
        'description': 'mowing grass'},
    'cleaning': {
        'description': 'cleaning environment'},
    'covering': {
        'description': 'covering water containers'},
    'burning': {
        'description': 'burning leaves, manure, etc.'},
    'kidney': {
        'description': 'kidney'},
    'other': {
        'description': 'other'},
    'na': {
        'description': 'does not know / ignored'},
}

NET_SOURCES = {
    'market': {
        'description': 'market place'},
    'health_center': {
        'description': 'health center'},
    'distribution': {
        'description': 'distribution campaign'},
    'ngo': {
        'description': 'NGO'},
    'other': {
        'description': 'other'},
}


def maxrandint():
    """
    Silly wrapper because Django can’t serialize functools.partial.
    """
    return randint(-2 * 10**8, -2 * 10**8)


class StatisticsMixin(object):
    """
    The `statistics` property is needed on all models that have
    statistics associated with them.
    """

    @property
    def statistics(self):
        """
        Statistics associated with the model.

        The foreign key relationship between models that have statistics
        associated with them and the statistics themselves corresponds to
        a long form table. This property transforms them into a more useful
        dictionary format.
        """
        keyfunc = lambda obj: obj.key
        stats = groupby(sorted(self.statistic_set.all(), key=keyfunc), keyfunc)
        return {key: {obj.value: obj.aggregate for obj in objs}
                for key, objs in stats}


class Household(models.Model):
    """
    Class to represent a household.

    Since all attributes of a household can change but some are unlikely to chance, it should be possible to match them fuzzily to survey resonses. This is not yet implemented as it depends
    on PDCU data that is not yet available.
    """
    pass


class Country(models.Model):
    """
    Class to represent a country.

    Some of these data are copied from a library and then joined with geographical
    data and associated with net gap data.
    """
    name = models.CharField(max_length=256, unique=True)
    numeric = models.IntegerField()
    alpha2 = models.CharField(max_length=2)
    alpha3 = models.CharField(max_length=3)
    geom = models.GeometryField(null=True, blank=True)

    @property
    def netgap(self):
        """
        Net gap data associated with the country.

        The net gap data is keyed on country, year, and estimate date
        and may be sparse, so that for easier access it is converted into
        a dictionary format where newer estimates of the same value override
        older estimates.
        """
        years = sorted(self.netgaps.all(), key=lambda obj: (obj.year, obj.estimate_date))
        years = groupby(years, key=lambda obj: obj.year)
        netgaps = {}
        for year, objs in years:
            netgaps[year] = defaultdict(lambda: None)
            for obj in objs:
                # High years come last and thus override lower years
                if obj.needed is not None:
                    netgaps[year]['needed'] = obj.needed
                if obj.financed is not None:
                    netgaps[year]['financed'] = obj.financed
                if obj.netgap is not None:
                    # Using netgap rather than gap to return redundant
                    # rather than sparse data
                    netgaps[year]['gap'] = obj.netgap
        return netgaps

    @property
    def indicator(self):
        """
        Indicator data associated with the country.

        The indicator data is keyed on country and year, and may be sparse,
        so that for easier access it is converted into a dictionary format.
        """
        filtered_vars = lambda obj: {key: value for key, value in vars(obj).items()
                                     if not key.startswith('_') and
                                        not key in ('year', 'id', 'country_id') and
                                        value is not None}
        indicators = defaultdict(dict)
        for indicator in self.indicators.all():
            for key, value in filtered_vars(indicator).items():
                indicators[key][indicator.year] = value
        return indicators

    objects = models.GeoManager()

    class Meta:
        verbose_name_plural = 'countries'

    def __unicode__(self):
        return self.name


class NetGap(models.Model):
    """
    The number of nets lacking per country per year.

    These data are keyed on country, year, and estimate date and may be sparse.
    Ideally they specify the nets `needed` (but not financed), the nets `financed`,
    and the resulting net `gap`. The relationship is `needed - financed = gap`.
    """
    needed = models.IntegerField(null=True, blank=True)
    financed = models.IntegerField(null=True, blank=True)
    gap = models.IntegerField(null=True, blank=True)
    year = models.IntegerField()
    country = models.ForeignKey('Country', related_name='netgaps',
                                on_delete=models.PROTECT)
    estimate_date = models.DateField()

    @property
    def netgap(self):
        """
        Returns or computes the net gap.

        The net gap is known in two cases, when it is specified, and when both
        needed and financed nets are specified. This function returns the value
        in both cases.
        """
        if self.gap is not None:
            return self.gap
        if self.needed is not None and self.financed is not None:
            return self.needed - self.financed

    class Meta:
        unique_together = (('year', 'country', 'estimate_date'),)

    def __unicode__(self):
        return 'Gap for {} in {} as of {}: {}'.format(
            self.country, self.year, self.estimate_date, self.netgap)


class Indicator(models.Model):
    """
    A collection of indicators, e.g., of standard of living.

    These data are keyed on country and year, and may be sparse.
    """
    gdp_pc_mean = models.FloatField(null=True, blank=True)
    gdp_pc_median = models.FloatField(null=True, blank=True)
    whr_score = models.FloatField(null=True, blank=True)
    life_ladder = models.FloatField(null=True, blank=True)
    social_support = models.FloatField(null=True, blank=True)
    life_expectancy = models.FloatField(null=True, blank=True)
    freedom = models.FloatField(null=True, blank=True)
    generosity = models.FloatField(null=True, blank=True)
    corruption = models.FloatField(null=True, blank=True)
    positive_affect = models.FloatField(null=True, blank=True)
    negative_affect = models.FloatField(null=True, blank=True)
    confidence_government = models.FloatField(null=True, blank=True)
    democratic_quality = models.FloatField(null=True, blank=True)
    delivery_quality = models.FloatField(null=True, blank=True)
    gini_index = models.FloatField(null=True, blank=True)
    trust = models.FloatField(null=True, blank=True)
    year = models.IntegerField()
    country = models.ForeignKey('Country', related_name='indicators',
                                on_delete=models.PROTECT)

    class Meta:
        unique_together = (('year', 'country'),)

    def __unicode__(self):
        return 'Indicator for {} in {}'.format(self.country, self.year)


class DistributionCampaign(models.Model):
    """
    AMF lists proposals, each of which can specify several distributions.
    To keep the names more intuitive, proposals are called distribution
    campaigns in LLIN Visualizer.
    """
    name = models.CharField(max_length=256)
    url = models.CharField(max_length=2048, blank=True)
    description = models.TextField(blank=True)
    amf_id = models.IntegerField(db_index=True, unique=True, null=True, blank=True)

    @property
    def bbox(self):
        """
        The bounding box of all countained distributions.
        """
        return self.distributions.all().collect().extent

    def __unicode__(self):
        return self.name


class Distribution(models.Model, StatisticsMixin):
    """
    Class to represent a distribution.

    These data are typically imported from AMF’s website.
    """
    name = models.CharField(max_length=256)
    geom = models.PointField(null=True, blank=True)
    status = models.CharField(max_length=256, blank=True)
    net_count = models.IntegerField(null=True, blank=True)
    partner = models.CharField(max_length=256, blank=True)
    region = models.CharField(max_length=256, blank=True)
    start = models.DateField(null=True, blank=True)
    end = models.DateField(null=True, blank=True)
    url = models.CharField(max_length=2048, blank=True)
    image = models.CharField(max_length=2048, blank=True)
    video = models.CharField(max_length=2048, blank=True)
    amf_id = models.IntegerField(db_index=True, unique=True, null=True, blank=True)

    # Calculated data
    centroid = models.PointField(null=True, blank=True)
    hull = models.GeometryField(null=True, blank=True)

    country = models.ForeignKey('Country', related_name='distributions',
                                null=True, blank=True, on_delete=models.SET_NULL)
    campaign = models.ForeignKey('DistributionCampaign', related_name='distributions',
                                 null=True, blank=True, on_delete=models.SET_NULL)

    objects = models.GeoManager()

    @property
    def statistics(self):
        """Wrapper to make it show up in vars()"""
        return super(Distribution, self).statistics

    class Meta:
        unique_together = (('name', 'start'),)

    def __unicode__(self):
        return self.name


class SurveyResponse(models.Model):
    """
    Response to a survey.

    Surveys are predistribution registration surveys and postdistribution
    check-ups. All survey responses are associated with a distribution and
    serve as a basis for the generation of clusters, which they are associated
    with following the clusters’ generation.

    The set of values is strongly influenced by IMA’s 2014 PDRS. When more data
    become available, the set of attributes may be extended.

    TODO: This might need to be linked to a survey.
    """
    geom = models.PointField(dim=3)
    latitude = models.FloatField()
    longitude = models.FloatField()
    altitude = models.FloatField()
    accuracy = models.FloatField()
    datetime = models.DateTimeField()
    province_name = models.CharField(max_length=256, blank=True)
    district_name = models.CharField(max_length=256, blank=True)
    health_zone_name = models.CharField(max_length=256, blank=True)
    health_area_name = models.CharField(max_length=256, blank=True)
    village_name = models.CharField(max_length=256, blank=True)
    house_number = models.CharField(max_length=32, blank=True)
    name = models.CharField(max_length=256, blank=True)
    phone = models.CharField(max_length=32, blank=True)
    phone_id = models.IntegerField(null=True, blank=True)
    day_of_distribution = models.IntegerField(null=True, blank=True)
    legal_script = models.NullBooleanField()
    signature = models.URLField(max_length=2048)

    # Demography
    age = models.IntegerField(null=True, blank=True)
    sex = models.CharField(max_length=32, blank=True)
    people_in_household = models.IntegerField(null=True, blank=True)
    pregnant_women = models.IntegerField(null=True, blank=True)
    children_under_five = models.IntegerField(null=True, blank=True)

    # Knowledge
    malaria_transmission = ArrayField(models.CharField(max_length=256))
    malaria_other_transmission = models.CharField(max_length=256, blank=True)
    malaria_symptoms = ArrayField(models.CharField(max_length=256))
    malaria_other_symptoms = models.CharField(max_length=256, blank=True)
    malaria_prevention = ArrayField(models.CharField(max_length=256))
    malaria_treatment = ArrayField(models.CharField(max_length=256))
    malaria_other_treatment = models.CharField(max_length=256, blank=True)

    # Prevention
    at_least_one_net = models.NullBooleanField()
    at_least_one_llin = models.NullBooleanField()
    llins = models.IntegerField(null=True, blank=True)
    net_source = models.CharField(max_length=256, blank=True)
    other_net_source = models.CharField(max_length=256, blank=True)
    people_under_net_last_night = models.IntegerField(null=True, blank=True)
    pregnant_women_under_net_last_night = models.IntegerField(null=True, blank=True)
    fansidar_during_pregnancy = models.IntegerField(null=True, blank=True)
    children_under_net_last_night = models.IntegerField(null=True, blank=True)
    preventive_measures = ArrayField(models.CharField(max_length=256))
    other_preventive_measures = models.CharField(max_length=256, blank=True)

    # Treatment
    child_fever_last_two_weeks = models.NullBooleanField()
    child_benefit_from_drug = models.NullBooleanField()
    child_medicine = models.CharField(max_length=256, blank=True)
    child_healthcenter = models.NullBooleanField()

    # Distribution
    sleeping_spaces = models.IntegerField(null=True, blank=True)
    llins_good = models.IntegerField(null=True, blank=True)
    llins_total = models.IntegerField(null=True, blank=True)
    summary = models.CharField(max_length=256, blank=True)
    llins_installed = models.IntegerField(null=True, blank=True)
    llins_brand = models.CharField(max_length=256, blank=True)
    llins_returned = models.IntegerField(null=True, blank=True)
    llins_packaging_returned = models.IntegerField(null=True, blank=True)
    llins_photo = models.URLField(max_length=2048)

    # Meta
    start = models.DateTimeField(null=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    device_id = models.CharField(max_length=256, blank=True)
    instance_id = models.CharField(max_length=256, blank=True)

    random = models.IntegerField(default=maxrandint, db_index=True)

    household = models.ForeignKey('Household', related_name='responses',
                                  null=True, blank=True, on_delete=models.SET_NULL)
    cluster = models.ForeignKey('Cluster', related_name='responses',
                                null=True, blank=True, on_delete=models.SET_NULL)
    distribution = models.ForeignKey('Distribution', related_name='responses',
                                     null=True, blank=True, on_delete=models.SET_NULL)

    objects = models.GeoManager()

    class Meta:
        unique_together = (
            ('distribution', 'latitude', 'longitude', 'altitude', 'accuracy', 'datetime'),)

    def __unicode__(self):
        return self.name


class Cluster(models.Model, StatisticsMixin):
    """
    An intermediary level in the tree to reduce the number of features
    rendered on the map at the same time.

    The cluster generation job takes a composite key to distinguish clusters
    as well as a `kind` to describe it. An intuitive choice for the `kind` may
    be “village.” (It’s called `kind` and not `type` to avoid a collision with
    Python’s funny `type()` function.)
    """
    name = models.CharField(max_length=256)
    kind = models.CharField(max_length=256, blank=True)

    # Calculated data
    centroid = models.PointField()
    hull = models.GeometryField()

    distribution = models.ForeignKey('Distribution', related_name='clusters')

    @property
    def statistics(self):
        """Wrapper to make it show up in vars()"""
        return super(Cluster, self).statistics

    class Meta:
        unique_together = (
            ('name', 'distribution'),)

    objects = models.GeoManager()

    def __unicode__(self):
        return self.name


class IRTest(models.Model):
    """
    Results of insecticide resistance test from IR Mapper.
    """
    geom = models.PointField()
    latitude = models.FloatField()
    longitude = models.FloatField()
    chemical_class = models.CharField(max_length=256, blank=True)
    chemical_type = models.CharField(max_length=256, blank=True)
    country_name = models.CharField(max_length=256, blank=True)
    irm_id = models.IntegerField(null=True, blank=True, unique=True)
    irac_moa = models.CharField(max_length=256, blank=True)
    irac_moa_code = models.CharField(max_length=256, blank=True)
    ir_mechanism_name = models.CharField(max_length=256, blank=True)
    ir_mechanism_status = models.CharField(max_length=256, blank=True)
    ir_mechanism_status_code = models.CharField(max_length=256, blank=True)
    ir_test_method = models.CharField(max_length=256, blank=True)
    ir_test_exposed = models.IntegerField(null=True, blank=True)
    ir_test_mortality = models.FloatField(null=True, blank=True)
    insecticide_dosage = models.CharField(max_length=256, blank=True)  # Mostly % but sometimes µg
    kdr_frequency = models.CharField(max_length=256, blank=True)
    locality = models.CharField(max_length=256, blank=True)
    molecular_forms = models.CharField(max_length=256, blank=True)
    mosquito_collection_year_start = models.IntegerField(null=True, blank=True)
    mosquito_collection_period = models.CharField(max_length=256, blank=True)
    mosquito_collection_year_end = models.IntegerField(null=True, blank=True)
    r_number = models.IntegerField(null=True, blank=True)
    r_code = models.CharField(max_length=256, blank=True)
    reference_name = models.CharField(max_length=256, blank=True)
    reference_type = models.CharField(max_length=256, blank=True)
    resistance_status = models.CharField(max_length=256, blank=True)
    synergist_test_mortality = models.FloatField(null=True, blank=True)
    synergist_type = models.CharField(max_length=256, blank=True)
    synergist_dosage = models.CharField(max_length=256, blank=True)
    url = models.CharField(max_length=2048, blank=True)
    vector_species = models.CharField(max_length=256, blank=True)

    country = models.ForeignKey('Country', related_name='ir_tests',
                                null=True, blank=True, on_delete=models.SET_NULL)

    @property
    def insecticide_resistance(self):
        """
        The percentage of mosquitoes that we found resistant.

        The insecticide resistance tests specify the percentage of mosquitoes that
        succumed to the insecticide, while we are usually interested in the
        percentage of mosquitoes that were resistant.
        """
        return 100 - self.ir_test_mortality

    objects = models.GeoManager()

    class Meta:
        verbose_name = 'IR test'

    def __unicode__(self):
        return '{} ({}%)'.format(self.locality, self.ir_test_mortality)


class Statistic(models.Model):
    """
    Model to store generated aggregate data.

    `key` and `value` allow for two levels of keying of the aggregates.
    A `key` may be something like “malaria_symptoms” and `value` something
    like `fever` as well as various other symptoms. The `aggregate` is in this
    case the sum of symptoms of a certain kind. Such `aggregate`s can of course
    take many shapes.
    """
    key = models.CharField(max_length=2048)
    value = models.CharField(max_length=2048)
    aggregate = models.FloatField(null=True, blank=True)

    cluster = models.ForeignKey('Cluster', null=True, blank=True,
                                on_delete=models.SET_NULL)
    distribution = models.ForeignKey('Distribution', null=True, blank=True,
                                     on_delete=models.SET_NULL)

    class Meta:
        unique_together = (
            ('key', 'value', 'cluster'),
            ('key', 'value', 'distribution'))

    def __unicode__(self):
        return '{}({}) = {}'.format(self.key, self.value, self.aggregate)
