# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from django.test import TestCase
from django.db import connection
from django.contrib.gis.geos import Point, Polygon, LineString
from ..management.commands.generate_clusters import Command
from ..management.commands import survey_import
from ..models import Distribution, SurveyResponse, Cluster, Statistic


CSV_PATH = 'llinvisualizer/analytics/tests/fixtures/donnees-kamonia.csv.head'
CSV_PARSER = 'ima2014a'


class BaseTestCase(TestCase):

    maxDiff = None

    def setUp(self):
        self.command = Command()
        self.survey_import_command = survey_import.Command()
        self.survey_import_command.distribution = Distribution(name='Mock Distribution 1')
        self.survey_import_command.distribution.save()
        with open(CSV_PATH) as csvfile:
            self.survey_import_command.Parser = survey_import.PARSERS[CSV_PARSER]
            reader = survey_import.Command.csvparse(
                csvfile, has_header=True, fieldnames=self.survey_import_command.Parser.fieldnames)
            self.outputs = map(self.survey_import_command.Parser, reader)
            self.survey_import_command.atomic_writer(self.outputs)
        self.command.cursor = connection.cursor()

    def test_import(self):
        self.assertEqual(len(self.outputs), 9)
        self.assertEqual(SurveyResponse.objects.count(), len(self.outputs))
        self.assertEqual(Distribution.objects.count(), 1)


class DistributionAugmentationTestCase(BaseTestCase):

    def setUp(self):
        super(DistributionAugmentationTestCase, self).setUp()
        self.command.augment_distributions()

    def test_centroid(self):
        distribution = Distribution.objects.get(name='Mock Distribution 1')
        self.assertEqual(str(distribution.centroid),
                         str(Point((20.9325095864777744, -6.8800338672222221), srid=4326)))

    def test_hull(self):
        distribution = Distribution.objects.get(name='Mock Distribution 1')
        self.assertEqual(str(distribution.hull),
                         str(Polygon((
                            (20.9329516178000006, -6.8814863061000002),
                            (20.9328862817999983, -6.8813517519999996),
                            (20.9317975314999991, -6.8786779565999998),
                            (20.9321743241000000, -6.8783831112999998),
                            (20.9328937844999992, -6.8810483414999997),
                            (20.9329516178000006, -6.8814863061000002),),
                            srid=4326)))


class ClusterGenerationTestCase(BaseTestCase):

    def setUp(self):
        super(ClusterGenerationTestCase, self).setUp()
        self.command.generate_clusters(('village_name', 'health_area_name'), 'village')

    def test_distribution(self):
        for cluster in Cluster.objects.all():
            self.assertEqual(cluster.distribution, self.survey_import_command.distribution)

    def test_kind(self):
        for cluster in Cluster.objects.all():
            self.assertEqual(cluster.kind, 'village')

    def test_cluster_count(self):
        self.assertEqual(Cluster.objects.count(), 3)

    def test_cluster_centroid(self):
        cluster = Cluster.objects.get(name='Kamonia, Luanga-Tshimu')
        self.assertEqual(str(cluster.centroid),
                         str(Point(20.9328900331499987, -6.8812000467499992, srid=4326)))

    def test_cluster_hull_point(self):
        cluster = Cluster.objects.get(name='Commercial, Luanga-Tshimu')
        self.assertEqual(str(cluster.hull),
                         str(Point((20.9329215921000014, -6.8813506607999999), srid=4326)))

    def test_cluster_hull_linestring(self):
        cluster = Cluster.objects.get(name='Kamonia, Luanga-Tshimu')
        self.assertEqual(str(cluster.hull),
                         str(LineString((
                            (20.9328862817999983, -6.8813517519999996,),
                            (20.9328937844999992, -6.8810483414999997),),
                            srid=4326)))

    def test_cluster_hull_polygon(self):
        cluster = Cluster.objects.get(name='Kabangu, Luanga-Tshimu')
        self.assertEqual(str(cluster.hull),
                         str(Polygon((
                            (20.9329516178000006, -6.8814863061000002),
                            (20.9317975314999991, -6.8786779565999998),
                            (20.9321743241000000, -6.8783831112999998),
                            (20.9329516178000006, -6.8814863061000002),),
                            srid=4326)))

    def test_augmentation(self):
        clusters = set(Cluster.objects.all())
        for survey_response in SurveyResponse.objects.all():
            self.assertTrue(survey_response.cluster in clusters)


class ClusterStatisticsTestCase(BaseTestCase):

    @staticmethod
    def _compress_stats(queryset):
        return {stat.value: stat.aggregate for stat in queryset}

    def setUp(self):
        super(ClusterStatisticsTestCase, self).setUp()
        self.command.generate_clusters(('village_name', 'health_area_name'), 'village')
        self.command.generate_cluster_statistics()
        self.sample_cluster = Cluster.objects.get(name='Kabangu, Luanga-Tshimu')

    def test_count(self):
        self.assertEqual(Statistic.objects.count(), 180)
        self.assertEqual(Statistic.objects.filter(cluster=self.sample_cluster).count(), 60)

    def test_centroid(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='centroid'))
        self.assertEqual(stats, {
            'sd_distance': 56.4314256737453,
            'max_distance': 238.529827163,
            'avg_distance': 139.488746492})

    def test_survey_response(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='survey_response'))
        self.assertEqual(stats, {'count': 6.0})

    def test_total_usage(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='total_usage'))
        self.assertEqual(stats, {
            'people_in_household': 31.0,
            'people_under_net_last_night': 3.0})

    def test_women_usage(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='women_usage'))
        self.assertEqual(stats, {
            'pregnant_women': 2.0,
            'pregnant_women_under_net_last_night': 2.0})

    def test_children_usage(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='children_usage'))
        self.assertEqual(stats, {
            'children_under_five': 7.0,
            'children_under_net_last_night': 3.0})

    def test_net_source(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='net_source'))
        self.assertEqual(stats, {
            'distribution': 0.0,
            'health_center': 0.0,
            'market': 0.0,
            'ngo': 0.0,
            'other': 0.0})

    def test_malaria_symptoms(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='malaria_symptoms'))
        self.assertEqual(stats, {
            'diarrhea': 0.0,
            'fever': 6.0,
            'headache': 4.0,
            'na': 0.0,
            'no_appetite': 0.0,
            'other': 0.0,
            'pain': 4.0,
            'tiredness': 2.0,
            'vomiting': 0.0})

    def test_malaria_transmission(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='malaria_transmission'))
        self.assertEqual(stats, {
            'dirt': 0.0,
            'fruit': 0.0,
            'mosquito': 4.0,
            'na': 0.0,
            'other': 0.0,
            'sick_people': 0.0,
            'sun': 2.0,
            'unsafe_water': 0.0,
            'witchcraft': 0.0})

    def test_malaria_prevention(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='malaria_prevention'))
        self.assertEqual(stats, {
            'contraceptive': 0.0,
            'drug': 0.0,
            'herb': 0.0,
            'insecticide': 0.0,
            'llin': 4.0,
            'na': 0.0,
            'other': 0.0,
            'pipeline': 2.0,
            'weed': 4.0})

    def test_malaria_treatment(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='malaria_treatment'))
        self.assertEqual(stats, {
            'amodiaquine': 3.0,
            'artemisinin': 3.0,
            'aspirine_ibuprofen': 0.0,
            'chloroquine': 1.0,
            'fansidar': 0.0,
            'other': 0.0,
            'paracetamol': 3.0,
            'quinine': 4.0})

    def test_preventive_measures(self):
        stats = self._compress_stats(
            Statistic.objects.filter(cluster=self.sample_cluster, key='preventive_measures'))
        self.assertEqual(stats, {
            'burning': 0.0,
            'cleaning': 1.0,
            'covering': 0.0,
            'kidney': 0.0,
            'lotion': 3.0,
            'mowing': 0.0,
            'na': 0.0,
            'other': 0.0,
            'spraying': 1.0,
            'water': 4.0})


class DistributionStatisticsTestCase(ClusterStatisticsTestCase):

    def setUp(self):
        super(ClusterStatisticsTestCase, self).setUp()
        self.command.augment_distributions()
        self.command.generate_distribution_statistics()
        self.sample_distribution = self.survey_import_command.distribution

    def test_count(self):
        self.assertEqual(Statistic.objects.count(), 60)

    def test_centroid(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='centroid'))
        stats = {key: round(value) for key, value in stats.items()}
        self.assertEqual(stats, {
            'sd_distance': 27,
            'max_distance': 186,
            'avg_distance': 155,})

    def test_survey_response(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='survey_response'))
        self.assertEqual(stats, {'count': 9.0})

    def test_total_usage(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='total_usage'))
        self.assertEqual(stats, {
            'people_in_household': 49.0,
            'people_under_net_last_night': 7.0})

    def test_women_usage(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='women_usage'))
        self.assertEqual(stats, {
            'pregnant_women': 5.0,
            'pregnant_women_under_net_last_night': 3.0})

    def test_children_usage(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='children_usage'))
        self.assertEqual(stats, {
            'children_under_five': 13.0,
            'children_under_net_last_night': 5.0})

    def test_net_source(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='net_source'))
        self.assertEqual(stats, {
            'distribution': 0.0,
            'health_center': 0.0,
            'market': 0.0,
            'ngo': 0.0,
            'other': 0.0})

    def test_malaria_symptoms(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='malaria_symptoms'))
        self.assertEqual(stats, {
            'diarrhea': 0.0,
            'fever': 9.0,
            'headache': 6.0,
            'na': 0.0,
            'no_appetite': 0.0,
            'other': 1.0,
            'pain': 5.0,
            'tiredness': 2.0,
            'vomiting': 0.0})

    def test_malaria_transmission(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='malaria_transmission'))
        self.assertEqual(stats, {
            'dirt': 0.0,
            'fruit': 1.0,
            'mosquito': 5.0,
            'na': 0.0,
            'other': 0.0,
            'sick_people': 0.0,
            'sun': 2.0,
            'unsafe_water': 1.0,
            'witchcraft': 0.0})

    def test_malaria_prevention(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='malaria_prevention'))
        self.assertEqual(stats, {
            'contraceptive': 0.0,
            'drug': 0.0,
            'herb': 1.0,
            'insecticide': 0.0,
            'llin': 6.0,
            'na': 0.0,
            'other': 0.0,
            'pipeline': 4.0,
            'weed': 5.0})

    def test_malaria_treatment(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='malaria_treatment'))
        self.assertEqual(stats, {
            'amodiaquine': 3.0,
            'artemisinin': 3.0,
            'aspirine_ibuprofen': 0.0,
            'chloroquine': 1.0,
            'fansidar': 0.0,
            'other': 1.0,
            'paracetamol': 5.0,
            'quinine': 7.0})

    def test_preventive_measures(self):
        stats = self._compress_stats(
            Statistic.objects.filter(distribution=self.sample_distribution, key='preventive_measures'))
        self.assertEqual(stats, {
            'burning': 0.0,
            'cleaning': 2.0,
            'covering': 0.0,
            'kidney': 0.0,
            'lotion': 4.0,
            'mowing': 0.0,
            'na': 0.0,
            'other': 1.0,
            'spraying': 2.0,
            'water': 6.0})
