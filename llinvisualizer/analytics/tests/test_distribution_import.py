# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import datetime
from django.test import TestCase
from ..management.commands.distribution_import import DistributionImporter, CampaignImporter
from ..models import Distribution, DistributionCampaign


class CampaignImportTestCase(TestCase):

    maxDiff = None

    def setUp(self):
        self.campaign_importer = CampaignImporter(
            'llinvisualizer/analytics/tests/fixtures/Distributions.2016-02-28.html')
        self.campaigns = list(self.campaign_importer.parse())

    def test_name(self):
        self.assertEqual(self.campaigns[0]['name'], 'Balaka District 2018')

    def test_url(self):
        self.assertEqual(self.campaigns[0]['url'],
                         'http://www.againstmalaria.com/Distribution.aspx?ProposalID=210')

    def test_amf_id(self):
        self.assertEqual(self.campaigns[0]['amf_id'], 210)

    def test_atomic_writer(self):
        imported, updated = self.campaign_importer.atomic_writer(self.campaigns)
        self.assertEqual(imported, len(self.campaigns))
        self.assertEqual(updated, 0)
        self.assertEqual(DistributionCampaign.objects.count(), len(self.campaigns))


class DistributionImportTestCase(TestCase):

    maxDiff = None

    def setUp(self):
        self.distribution_importer = DistributionImporter(
            'llinvisualizer/analytics/tests/fixtures/Distributions.2016-02-01.kmz')
        self.distributions = list(self.distribution_importer.parse())

    def test_partner(self):
        self.assertEqual(self.distributions[0]['partner'], 'UNICEF')

    def test_net_count(self):
        self.assertEqual(self.distributions[0]['net_count'], 2600)

    def test_image(self):
        self.assertEqual(self.distributions[0]['image'],
                         'http://www.againstmalaria.com/images/00/02/small/2539.jpg')

    def test_video(self):
        self.assertEqual(self.distributions[6]['video'], '//www.youtube.com/embed/wwUDDx6rMcA')

    def test_status(self):
        self.assertEqual(self.distributions[0]['status'], 'Distribution complete')

    def test_url(self):
        self.assertEqual(
            self.distributions[0]['url'],
            'http://www.AgainstMalaria.com/Distribution.aspx?DistributionID=50&ProposalID=12')

    def test_start(self):
        self.assertEqual(self.distributions[0]['start'], datetime.date(2007, 4, 1))

    def test_end(self):
        self.assertEqual(self.distributions[0]['end'], datetime.date(2007, 4, 30))

    def test_amf_id(self):
        self.assertEqual(self.distributions[0]['amf_id'], 50)

    def test_proposal_id(self):
        self.assertEqual(self.distributions[0]['proposal_id'], 12)

    def test_atomic_writer(self):
        DistributionCampaign.objects.bulk_create([
            DistributionCampaign(name='Mock 1', amf_id=1),
            DistributionCampaign(name='Mock 2', amf_id=12),
            DistributionCampaign(name='Mock 3', amf_id=92),
            DistributionCampaign(name='Mock 4', amf_id=106),
        ])
        imported, updated = self.distribution_importer.atomic_writer(self.distributions)
        self.assertEqual(imported, len(self.distributions))
        self.assertEqual(updated, 0)
        self.assertEqual(Distribution.objects.count(), len(self.distributions))
