# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import json
from django.test import TestCase
from ..models import Country, IRTest
from ..management.commands import irmapper_import


def import_testdata():
    command = irmapper_import.Command()
    with open('llinvisualizer/analytics/tests/fixtures/irmapper.json') as jsonfile:
        items = json.load(jsonfile)
    outputs = []
    for item in items:
        outputs.append(irmapper_import.IRConverter(item))
    return command, outputs


class IRImportTestCase(TestCase):

    maxDiff = None

    def setUp(self):
        self.command, self.outputs = import_testdata()

    def test_is_valid(self):
        for output in self.outputs:
            self.assertTrue(output.is_valid)

    def test_country(self):
        self.assertEqual(self.outputs[0]['country'], Country.objects.get(name='Nigeria'))

    def test_atomic_writer(self):
        self.command.atomic_writer(self.outputs)
        self.assertEqual(IRTest.objects.count(), len(self.outputs))
