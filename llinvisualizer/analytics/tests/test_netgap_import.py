# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import unicodecsv
from django.test import TestCase
from ..management.commands import netgap_import
from ..models import NetGap, Country


def import_testdata():
    command = netgap_import.Command()
    with open('llinvisualizer/analytics/tests/fixtures/gap.2015.csv') as csvfile:
        reader = unicodecsv.DictReader(csvfile, encoding='utf-8', delimiter=b'\t')
        outputs = list(command.process(
            reader, 'gap', {'estimate_date': '2015-01-01'}))
    return command, outputs


class NetgapImportTestCase(TestCase):

    maxDiff = None

    def setUp(self):
        self.command, self.outputs = import_testdata()

    def test_benin(self):
        self.assertEqual(self.outputs[0], {
            'country': Country.objects.get(name='Benin'),
            'estimate_date': '2015-01-01',
            'gap': '767197',
            'year': '2015'})

    def test_atomic_writer(self):
        self.command.atomic_writer(self.outputs)
        self.assertEqual(NetGap.objects.count(), len(self.outputs))
