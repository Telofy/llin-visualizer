# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import datetime
from django.test import TestCase
from django.contrib.gis.geos import Point
from ..management.commands import survey_import
from ..models import SurveyResponse, Distribution


def import_testdata(distribution):
    command = survey_import.Command()
    command.distribution = distribution
    command.Parser = survey_import.PARSERS['ima2014a']
    with open('llinvisualizer/analytics/tests/fixtures/donnees-kamonia.csv.head') as csvfile:
        reader = command.csvparse(
            csvfile, has_header=True, fieldnames=command.Parser.fieldnames)
        outputs = map(command.Parser, reader)
    return command, outputs


class IMA2014aCSVImportTestCase(TestCase):

    maxDiff = None

    def setUp(self):
        distribution = Distribution(name='Test Distribution')
        distribution.save()
        self.command, self.outputs = import_testdata(distribution)

    def test_is_valid(self):
        for output in self.outputs:
            self.assertTrue(output.is_valid)

    def test_geom(self):
        self.assertEqual(self.outputs[0]['latitude'], -6.881351752)
        self.assertEqual(str(self.outputs[1]['geom']),
                         str(Point(20.9328937845, -6.8810483415, 637.405523358)))

    def test_string_list(self):
        self.assertEqual(self.outputs[2]['preventive_measures'], ['water'])

    def test_date(self):
        self.assertEqual(self.outputs[3]['datetime'], datetime.datetime(1, 1, 1, 0, 0))
        self.assertEqual(self.outputs[2]['datetime'], datetime.datetime(2014, 10, 18, 15, 12))

    def test_none(self):
        self.assertEqual(self.outputs[4]['child_healthcenter'], None)

    def test_bool(self):
        self.assertEqual(self.outputs[5]['at_least_one_net'], True)

    def test_atomic_writer(self):
        self.command.atomic_writer(self.outputs)
        self.assertEqual(SurveyResponse.objects.count(), len(self.outputs))

    def test_incremental_writer(self):
        self.command.incremental_writer(self.outputs)
        self.assertEqual(SurveyResponse.objects.count(), len(self.outputs))


class IMA2014bCSVImportTestCase(IMA2014aCSVImportTestCase):

    maxDiff = None

    def setUp(self):
        self.command = survey_import.Command()
        self.command.distribution = Distribution(name='Test Distribution')
        self.command.distribution.save()
        self.command.Parser = survey_import.PARSERS['ima2014b']
        with open('llinvisualizer/analytics/tests/fixtures/donnees-kitangwa.csv.head') as csvfile:
            reader = self.command.csvparse(
                csvfile, has_header=True, fieldnames=self.command.Parser.fieldnames)
            self.outputs = map(self.command.Parser, reader)

    def test_geom(self):
        self.assertEqual(self.outputs[0]['latitude'], -6.0705290411)
        self.assertEqual(str(self.outputs[1]['geom']),
                         str(Point(19.9225345109, -6.0702319209, 626.4016512245)))

    def test_string_list(self):
        self.assertEqual(self.outputs[2]['preventive_measures'], ['lotion'])

    def test_date(self):
        self.assertEqual(self.outputs[2]['datetime'], datetime.datetime(2014, 10, 12, 9, 41))
        self.assertEqual(self.outputs[3]['datetime'], datetime.datetime(2014, 10, 12, 13, 12))

    def test_none(self):
        self.assertEqual(self.outputs[4]['child_healthcenter'], None)

    def test_bool(self):
        self.assertEqual(self.outputs[5]['at_least_one_net'], True)
