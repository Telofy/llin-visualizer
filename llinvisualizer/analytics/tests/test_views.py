# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import datetime
import json
import unicodecsv
from itertools import chain
from StringIO import StringIO
from django.test import TestCase, Client
from django.db import connection
from django.contrib.auth.models import User
from django.contrib.gis.geos import Point, Polygon, LineString
from ..management.commands import survey_import, generate_clusters
from ..models import (Distribution, DistributionCampaign, SurveyResponse,
                      Cluster, Statistic, Country)
from . import test_netgap_import, test_survey_import, test_irmapper_import, test_country_import


CSV_PATH = 'llinvisualizer/analytics/tests/fixtures/donnees-kamonia.csv.head'
CSV_PARSER = 'ima2014a'


class ViewsTestCase(TestCase):

    maxDiff = None

    def setUp(self):
        # Mocking
        DistributionCampaign(id=0, name='Mock Campaign 1', amf_id=5).save()
        self.distribution = Distribution(
            id=0, name='Mock Distribution 1', amf_id=23, geom=Point(20, -6))
        self.distribution.save()
        # Country import
        test_country_import.import_testdata()
        # Survey import
        command, outputs = test_survey_import.import_testdata(self.distribution)
        command.distribution = self.distribution
        command.atomic_writer(outputs)
        # Cluster generation
        command = generate_clusters.Command()
        command.cursor = connection.cursor()
        command.augment_distributions()
        command.generate_clusters(('village_name', 'health_area_name'), 'village')
        # Stats generation
        command.generate_cluster_statistics()
        command.generate_distribution_statistics()
        # Netgap import
        command, outputs = test_netgap_import.import_testdata()
        command.atomic_writer(outputs)
        # IR Mapper import
        command, outputs = test_irmapper_import.import_testdata()
        command.atomic_writer(outputs)

    def test_import(self):
        #import IPython; IPython.embed()
        self.assertEqual(SurveyResponse.objects.count(), 9)
        self.assertTrue(Distribution.objects.all()[0].hull)
        self.assertTrue(Distribution.objects.all()[0].centroid)
        self.assertEqual(Cluster.objects.count(), 3)
        self.assertEqual(Statistic.objects.count(), 240)

    def test_map(self):
        client = Client()
        response = client.get('/', follow=True)
        self.assertEqual(response.redirect_chain, [('/analytics/map/', 302)])
        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response, '<link rel="prefetch" href="/analytics/distributions.hulls.geojson" />')

    def test_tests(self):
        client = Client()
        response = client.get('/analytics/tests/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'jasmine.min.js"></script>')

    def test_translate_distribution(self):
        client = Client()
        response = client.get('/analytics/translate/?distribution_id=23', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.redirect_chain,
            [('/analytics/map/#version=1&distribution%5Btarget%5D=0', 302)])

    def test_translate_campaign(self):
        client = Client()
        response = client.get('/analytics/translate/?campaign_id=23', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.redirect_chain, [('/analytics/map/#version=1', 302)])

    def test_translate_both(self):
        client = Client()
        response = client.get(
            '/analytics/translate/?campaign_id=23&distribution_id=23', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.redirect_chain,
            [('/analytics/map/#version=1&distribution%5Btarget%5D=0', 302)])

    def test_distribution_points(self):
        client = Client()
        response = client.get('/analytics/distributions.points.geojson')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['crs'], {
            'properties': {'href': 'http://spatialreference.org/ref/epsg/4326/', 'type': 'proj4'},
            'type': 'link'})
        self.assertTrue(data['features'][0]['geometry'])
        self.assertTrue(data['features'][0]['properties']['name'])
        self.assertTrue(data['features'][0]['properties']['statistics'])

    def test_distribution_centroids(self):
        client = Client()
        response = client.get('/analytics/distributions.centroids.geojson')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['crs'], {
            'properties': {'href': 'http://spatialreference.org/ref/epsg/4326/', 'type': 'proj4'},
            'type': 'link'})
        self.assertTrue(data['features'][0]['geometry'])
        self.assertTrue(data['features'][0]['properties']['name'])
        self.assertTrue(data['features'][0]['properties']['statistics'])

    def test_distribution_hulls(self):
        client = Client()
        response = client.get('/analytics/distributions.hulls.geojson')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['crs'], {
            'properties': {'href': 'http://spatialreference.org/ref/epsg/4326/', 'type': 'proj4'},
            'type': 'link'})
        self.assertTrue(data['features'][0]['geometry'])
        self.assertTrue(data['features'][0]['properties']['name'])
        self.assertTrue(data['features'][0]['properties']['statistics'])

    def test_cluster_centroids(self):
        client = Client()
        response = client.get('/analytics/distributions/0/clusters.centroids.geojson')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['crs'], {
            'properties': {'href': 'http://spatialreference.org/ref/epsg/4326/', 'type': 'proj4'},
            'type': 'link'})
        self.assertTrue(data['features'][0]['geometry'])
        self.assertTrue(data['features'][0]['properties']['name'])
        self.assertTrue(data['features'][0]['properties']['statistics'])

    def test_cluster_hulls(self):
        client = Client()
        response = client.get('/analytics/distributions/0/clusters.hulls.geojson')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['crs'], {
            'properties': {'href': 'http://spatialreference.org/ref/epsg/4326/', 'type': 'proj4'},
            'type': 'link'})
        self.assertTrue(data['features'][0]['geometry'])
        self.assertTrue(data['features'][0]['properties']['name'])
        self.assertTrue(data['features'][0]['properties']['statistics'])

    def test_survey_response(self):
        cluster = Cluster.objects.get(name='Kabangu, Luanga-Tshimu')
        client = Client()
        user = User(username='Test', is_staff=True)
        user.save()
        client.force_login(user)
        response = client.get('/analytics/clusters/{}/survey-responses.geojson'.format(cluster.pk))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['crs'], {
            'properties': {'href': 'http://spatialreference.org/ref/epsg/4326/', 'type': 'proj4'},
            'type': 'link'})
        self.assertTrue(data['features'][0]['geometry'])
        self.assertTrue(data['features'][0]['properties']['name'])

    def test_ir_test(self):
        country = Country.objects.get(name='Nigeria')
        client = Client()
        response = client.get('/analytics/countries/{}/ir-tests.geojson'.format(country.pk))
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['crs'], {
            'properties': {'href': 'http://spatialreference.org/ref/epsg/4326/', 'type': 'proj4'},
            'type': 'link'})
        self.assertTrue(data['features'][0]['geometry'])
        self.assertTrue(data['features'][0]['properties']['chemical_class'])
        self.assertTrue(data['features'][0]['properties']['ir_test_mortality'])

    def test_netgap(self):
        client = Client()
        response = client.get('/analytics/countries.netgaps.geojson')
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['crs'], {
            'properties': {'href': 'http://spatialreference.org/ref/epsg/4326/', 'type': 'proj4'},
            'type': 'link'})
        self.assertTrue(data['features'][0]['geometry'])
        self.assertTrue(data['features'][0]['properties']['name'])
