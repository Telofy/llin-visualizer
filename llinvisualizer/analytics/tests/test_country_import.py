# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from django.test import TestCase
from ..models import Country
from ..management.commands import country_import


def import_testdata():
    command = country_import.Command()
    command.handle(file='llinvisualizer/analytics/tests/fixtures/countries.geo.json')
    return command


class IRImportTestCase(TestCase):

    maxDiff = None

    def setUp(self):
        self.command = import_testdata()

    def test_metadata(self):
        country = Country.objects.get(name='Nigeria')
        self.assertEqual(country.numeric, 566)
        self.assertEqual(country.alpha2, 'NG')
        self.assertEqual(country.alpha3, 'NGA')

    def test_geom(self):
        country = Country.objects.get(name='Nigeria')
        self.assertTrue(country.geom)
