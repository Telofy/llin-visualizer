# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from django.conf.urls import patterns, url
from django.views.decorators.cache import cache_page
from . import views, models


urlpatterns = patterns('',
    url(r'^map/$', views.map_, name='analytics-map-view'),
    url(r'^tests/$', views.tests, name='analytics-tests-view'),
    url(r'^translate/$', views.translate, name='analytics-translate-view'),
    url(r'^clusters/(?P<cluster_id>[0-9]+)/survey-responses.geojson$',
        cache_page(None)(
            views.SurveyResponseLayer.as_view(
                model=models.SurveyResponse)),
        name='survey-response-geojson'),
    url(r'^distributions/(?P<distribution_id>[0-9]+)/clusters.centroids.geojson$',
        cache_page(None)(
            views.ClusterLayer.as_view(
                model=models.Cluster,
                geometry_field='centroid')),
        name='cluster-centroid-geojson'),
    url(r'^distributions/(?P<distribution_id>[0-9]+)/clusters.hulls.geojson$',
        cache_page(None)(
            views.ClusterLayer.as_view(
                model=models.Cluster,
                geometry_field='hull')),
        name='cluster-hull-geojson'),
    url(r'^distributions.points.geojson$',
        cache_page(None)(
            views.DistributionLayer.as_view(
                model=models.Distribution,
                geometry_field='geom')),
        name='distribution-point-geojson'),
    url(r'^distributions.centroids.geojson$',
        cache_page(None)(
            views.DistributionLayer.as_view(
                model=models.Distribution,
                geometry_field='centroid')),
        name='distribution-centroid-geojson'),
    url(r'^distributions.hulls.geojson$',
        cache_page(None)(
            views.DistributionLayer.as_view(
                model=models.Distribution,
                geometry_field='hull')),
        name='distribution-hull-geojson'),
    url(r'^countries/(?P<country_id>[0-9]+)/ir-tests.geojson$',
        cache_page(None)(
            views.IRTestLayer.as_view(
                model=models.IRTest)),
        name='ir-test-geojson'),
    url(r'^countries.netgaps.geojson$',
        cache_page(None)(
            views.NetGapLayer.as_view(
                model=models.Country,
                geometry_field='geom')),
        name='countries-netgaps-geojson'),
    url(r'^countries.indicators.geojson$',
        cache_page(None)(
            views.IndicatorLayer.as_view(
                model=models.Country,
                geometry_field='geom')),
        name='countries-indicators-geojson'),
)
