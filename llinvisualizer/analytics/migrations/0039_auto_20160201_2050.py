# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0038_auto_20160201_1912'),
    ]

    operations = [
        migrations.AlterField(
            model_name='distributioncampaign',
            name='name',
            field=models.CharField(max_length=256),
        ),
    ]
