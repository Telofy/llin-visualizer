# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0015_auto_20151005_0801'),
    ]

    operations = [
        migrations.AlterField(
            model_name='healtharea',
            name='hull',
            field=django.contrib.gis.db.models.fields.GeometryField(srid=4326),
        ),
        migrations.AlterField(
            model_name='village',
            name='hull',
            field=django.contrib.gis.db.models.fields.GeometryField(srid=4326),
        ),
    ]
