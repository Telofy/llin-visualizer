# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0006_auto_20150930_1314'),
    ]

    operations = [
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='health_area_district',
            new_name='district',
        ),
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='health_area_type',
            new_name='health_area',
        ),
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='health_area_health_zone',
            new_name='health_zone',
        ),
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='health_area_province',
            new_name='province',
        ),
    ]
