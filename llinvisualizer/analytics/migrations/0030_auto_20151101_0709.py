# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0029_auto_20151031_1812'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cluster',
            name='name',
            field=models.CharField(max_length=256),
        ),
        migrations.AlterUniqueTogether(
            name='cluster',
            unique_together=set([('name', 'distribution')]),
        ),
    ]
