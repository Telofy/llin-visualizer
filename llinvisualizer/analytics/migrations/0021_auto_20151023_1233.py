# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0020_auto_20151023_1204'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='country',
            options={'verbose_name_plural': 'countries'},
        ),
        migrations.AlterModelOptions(
            name='irtest',
            options={'verbose_name': 'IR test'},
        ),
        migrations.AlterField(
            model_name='distribution',
            name='end',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='distribution',
            name='start',
            field=models.DateField(null=True, blank=True),
        ),
    ]
