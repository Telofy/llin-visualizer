# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0017_irtest'),
    ]

    operations = [
        migrations.AddField(
            model_name='irtest',
            name='latitude',
            field=models.FloatField(default=0.0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='irtest',
            name='longitude',
            field=models.FloatField(default=0.0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='irtest',
            name='irm_id',
            field=models.IntegerField(unique=True, null=True, blank=True),
        ),
    ]
