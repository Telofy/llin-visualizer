# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0026_auto_20151030_1232'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cluster',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=256)),
                ('kind', models.CharField(max_length=256, blank=True)),
                ('centroid', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('hull', django.contrib.gis.db.models.fields.GeometryField(srid=4326)),
                ('avg_distance', models.FloatField()),
                ('max_distance', models.FloatField()),
                ('sd_distance', models.FloatField(null=True, blank=True)),
                ('response_count', models.IntegerField()),
                ('distribution', models.ForeignKey(related_name='clusters', to='analytics.Distribution')),
            ],
        ),
        migrations.RemoveField(
            model_name='surveyresponse',
            name='health_area',
        ),
        migrations.RemoveField(
            model_name='surveyresponse',
            name='village',
        ),
        migrations.AddField(
            model_name='surveyresponse',
            name='cluster',
            field=models.ForeignKey(related_name='responses', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Cluster', null=True),
        ),
    ]
