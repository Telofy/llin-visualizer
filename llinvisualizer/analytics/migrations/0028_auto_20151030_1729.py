# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0027_auto_20151030_1721'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='healtharea',
            name='country',
        ),
        migrations.AlterUniqueTogether(
            name='village',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='village',
            name='country',
        ),
        migrations.RemoveField(
            model_name='village',
            name='health_area',
        ),
        migrations.DeleteModel(
            name='HealthArea',
        ),
        migrations.DeleteModel(
            name='Village',
        ),
    ]
