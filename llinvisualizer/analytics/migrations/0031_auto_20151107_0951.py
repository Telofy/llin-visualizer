# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0030_auto_20151101_0709'),
    ]

    operations = [
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='all_children_under_net_last_night',
            new_name='children_under_net_last_night',
        ),
        # Can’t cast to int
        migrations.RemoveField(
            model_name='surveyresponse',
            name='children_under_net_last_night',
        ),
        migrations.AddField(
            model_name='surveyresponse',
            name='children_under_net_last_night',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.RemoveField(
            model_name='surveyresponse',
            name='fansidar_during_pregnancy',
        ),
        migrations.AddField(
            model_name='surveyresponse',
            name='fansidar_during_pregnancy',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
