# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0004_auto_20150930_1312'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='surveyresponse',
            unique_together=set([('distribution', 'latitude', 'longitude', 'altitude', 'accuracy'), ('distribution', 'latitude', 'longitude', 'altitude', 'accuracy', 'datetime')]),
        ),
    ]
