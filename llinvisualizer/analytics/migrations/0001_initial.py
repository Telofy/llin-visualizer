# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Household',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='SurveyResponse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('location', django.contrib.gis.db.models.fields.PointField(srid=4326, dim=4)),
                ('health_area_province', models.CharField(max_length=256)),
                ('health_area_district', models.CharField(max_length=256)),
                ('health_area_health_zone', models.CharField(max_length=256)),
                ('health_area_type', models.CharField(max_length=256)),
                ('village', models.CharField(max_length=256)),
                ('house_number', models.CharField(max_length=32)),
                ('name', models.CharField(max_length=256)),
                ('phone', models.CharField(max_length=32)),
                ('datetime', models.DateTimeField()),
                ('phone_id', models.IntegerField()),
                ('day_of_distribution', models.IntegerField()),
                ('legal_script', models.NullBooleanField()),
                ('signature', models.URLField(max_length=2048)),
                ('age', models.IntegerField()),
                ('sex', models.CharField(max_length=32)),
                ('people_in_household', models.IntegerField()),
                ('pregnant_women', models.IntegerField()),
                ('children_under_five', models.IntegerField()),
                ('malaria_transmission', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=256), size=None)),
                ('malaria_other_transmission', models.CharField(max_length=256, blank=True)),
                ('malaria_symptoms', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=256), size=None)),
                ('malaria_other_symptoms', models.CharField(max_length=256, blank=True)),
                ('malaria_prevention', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=256), size=None)),
                ('malaria_treatment', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=256), size=None)),
                ('malaria_other_treatment', models.CharField(max_length=256, blank=True)),
                ('at_least_one_net', models.NullBooleanField()),
                ('at_least_one_llin', models.NullBooleanField()),
                ('llins', models.IntegerField()),
                ('net_source', models.CharField(max_length=256)),
                ('other_net_source', models.CharField(max_length=256)),
                ('people_under_net_last_night', models.IntegerField()),
                ('pregnant_women_under_net_last_night', models.IntegerField()),
                ('fansidar_during_pregnancy', models.NullBooleanField()),
                ('all_children_under_net_last_night', models.NullBooleanField()),
                ('preventive_measures', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=256), size=None)),
                ('other_preventive_measures', models.CharField(max_length=256, blank=True)),
                ('child_fever_last_two_weeks', models.NullBooleanField()),
                ('child_benefit_from_drug', models.NullBooleanField()),
                ('child_medicine', models.CharField(max_length=256, blank=True)),
                ('child_healthcenter', models.NullBooleanField()),
                ('sleeping_spaces', models.IntegerField()),
                ('llins_good', models.IntegerField()),
                ('llins_total', models.IntegerField()),
                ('summary', models.CharField(max_length=256)),
                ('llins_installed', models.IntegerField()),
                ('llins_brand', models.CharField(max_length=256)),
                ('llins_returned', models.IntegerField()),
                ('llins_packaging_returned', models.IntegerField()),
                ('llins_photo', models.URLField(max_length=256)),
                ('start', models.DateTimeField()),
                ('end', models.DateTimeField()),
                ('device_id', models.CharField(max_length=256)),
                ('instance_id', models.CharField(max_length=256)),
                ('household', models.ForeignKey(related_name='responses', to='analytics.Household')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='surveyresponse',
            unique_together=set([('location', 'name', 'datetime')]),
        ),
    ]
