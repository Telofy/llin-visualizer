# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0012_auto_20151004_1746'),
    ]

    operations = [
        migrations.AddField(
            model_name='surveyresponse',
            name='health_area',
            field=models.ForeignKey(related_name='responses', blank=True, to='analytics.HealthArea', null=True),
        ),
        migrations.AddField(
            model_name='surveyresponse',
            name='village',
            field=models.ForeignKey(related_name='responses', blank=True, to='analytics.Village', null=True),
        ),
    ]
