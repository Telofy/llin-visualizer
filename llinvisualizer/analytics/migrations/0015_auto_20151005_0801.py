# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0014_auto_20151005_0734'),
    ]

    operations = [
        migrations.AlterField(
            model_name='surveyresponse',
            name='health_area',
            field=models.ForeignKey(related_name='responses', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.HealthArea', null=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='household',
            field=models.ForeignKey(related_name='responses', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Household', null=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='village',
            field=models.ForeignKey(related_name='responses', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Village', null=True),
        ),
    ]
