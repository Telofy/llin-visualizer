# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0033_auto_20151107_1731'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statistic',
            name='cluster',
            field=models.ForeignKey(related_name='statistics', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Cluster', null=True),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='distribution',
            field=models.ForeignKey(related_name='statistics', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Distribution', null=True),
        ),
    ]
