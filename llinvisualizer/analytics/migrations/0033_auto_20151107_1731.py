# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0032_auto_20151107_1522'),
    ]

    operations = [
        migrations.CreateModel(
            name='Statistic',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(max_length=2048)),
                ('value', models.CharField(max_length=2048)),
                ('aggregate', models.IntegerField()),
                ('cluster', models.ForeignKey(related_name='statistics', to='analytics.Cluster')),
                ('distribution', models.ForeignKey(related_name='statistics', to='analytics.Distribution')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='statistic',
            unique_together=set([('key', 'value', 'cluster'), ('key', 'value', 'distribution')]),
        ),
    ]
