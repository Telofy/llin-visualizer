# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0021_auto_20151023_1233'),
    ]

    operations = [
        migrations.AddField(
            model_name='distribution',
            name='avg_distance',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='centroid',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='count',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='hull',
            field=django.contrib.gis.db.models.fields.GeometryField(srid=4326, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='max_distance',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='sd_distance',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
