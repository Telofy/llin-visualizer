# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0002_auto_20150923_1512'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='surveyresponse',
            unique_together=set([('location', 'distribution')]),
        ),
    ]
