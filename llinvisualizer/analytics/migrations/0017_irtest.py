# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0016_auto_20151005_1012'),
    ]

    operations = [
        migrations.CreateModel(
            name='IRTest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('chemical_class', models.CharField(max_length=256, blank=True)),
                ('chemical_type', models.CharField(max_length=256, blank=True)),
                ('country', models.CharField(max_length=256, blank=True)),
                ('irm_id', models.IntegerField(null=True, blank=True)),
                ('irac_moa', models.CharField(max_length=256, blank=True)),
                ('irac_moa_code', models.CharField(max_length=256, blank=True)),
                ('ir_mechanism_name', models.CharField(max_length=256, blank=True)),
                ('ir_mechanism_status', models.CharField(max_length=256, blank=True)),
                ('ir_mechanism_status_code', models.CharField(max_length=256, blank=True)),
                ('ir_test_method', models.CharField(max_length=256, blank=True)),
                ('ir_test_exposed', models.IntegerField(null=True, blank=True)),
                ('ir_test_mortality', models.FloatField(null=True, blank=True)),
                ('insecticide_dosage', models.FloatField(null=True, blank=True)),
                ('kdr_frequency', models.CharField(max_length=256, blank=True)),
                ('locality', models.CharField(max_length=256, blank=True)),
                ('molecular_forms', models.CharField(max_length=256, blank=True)),
                ('mosquito_collection_year_start', models.IntegerField(null=True, blank=True)),
                ('mosquito_collection_period', models.CharField(max_length=256, blank=True)),
                ('mosquito_collection_year_end', models.IntegerField(null=True, blank=True)),
                ('r_number', models.IntegerField(null=True, blank=True)),
                ('r_code', models.CharField(max_length=256, blank=True)),
                ('reference_name', models.CharField(max_length=256, blank=True)),
                ('reference_type', models.CharField(max_length=256, blank=True)),
                ('resistance_status', models.CharField(max_length=256, blank=True)),
                ('synergist_test_mortality', models.FloatField(null=True, blank=True)),
                ('synergist_type', models.CharField(max_length=256, blank=True)),
                ('synergist_dosage', models.CharField(max_length=256, blank=True)),
                ('url', models.CharField(max_length=2048, blank=True)),
                ('vector_species', models.CharField(max_length=256, blank=True)),
            ],
        ),
    ]
