# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import models, migrations
from django.contrib.gis.geos import Polygon

# Arbitrary placeholder copied from documentation
ext_coords = ((0, 0), (0, 1), (1, 1), (1, 0), (0, 0))

class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0013_auto_20151004_1746'),
    ]

    operations = [
        migrations.RenameField(
            model_name='healtharea',
            old_name='geom',
            new_name='centroid',
        ),
        migrations.RenameField(
            model_name='village',
            old_name='geom',
            new_name='centroid',
        ),
        migrations.AddField(
            model_name='healtharea',
            name='hull',
            field=django.contrib.gis.db.models.fields.PolygonField(default=Polygon(ext_coords), srid=4326),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='village',
            name='hull',
            field=django.contrib.gis.db.models.fields.PolygonField(default=Polygon(ext_coords), srid=4326),
            preserve_default=False,
        ),
    ]
