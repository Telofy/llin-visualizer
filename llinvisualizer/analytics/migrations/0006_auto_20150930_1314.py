# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0005_auto_20150930_1313'),
    ]

    operations = [
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='location',
            new_name='geom',
        ),
    ]
