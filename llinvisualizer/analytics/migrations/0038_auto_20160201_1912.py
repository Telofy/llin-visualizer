# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0037_auto_20160201_1807'),
    ]

    operations = [
        migrations.AlterField(
            model_name='distribution',
            name='amf_id',
            field=models.IntegerField(db_index=True, unique=True, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='distributioncampaign',
            name='amf_id',
            field=models.IntegerField(db_index=True, unique=True, null=True, blank=True),
        ),
    ]
