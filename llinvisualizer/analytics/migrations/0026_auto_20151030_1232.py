# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0025_distribution_geom'),
    ]

    operations = [
        migrations.AlterField(
            model_name='country',
            name='name',
            field=models.CharField(unique=True, max_length=256),
        ),
        migrations.AlterField(
            model_name='distributioncampaign',
            name='name',
            field=models.CharField(unique=True, max_length=256),
        ),
        migrations.AlterField(
            model_name='healtharea',
            name='name',
            field=models.CharField(unique=True, max_length=256, blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='distribution',
            unique_together=set([('name', 'start')]),
        ),
        migrations.AlterUniqueTogether(
            name='village',
            unique_together=set([('name', 'health_area')]),
        ),
    ]
