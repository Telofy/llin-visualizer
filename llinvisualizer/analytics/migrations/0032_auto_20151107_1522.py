# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0031_auto_20151107_0951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='surveyresponse',
            name='datetime',
            field=models.DateTimeField(default=datetime.datetime(1, 1, 1, 0, 0)),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='surveyresponse',
            unique_together=set([('distribution', 'latitude', 'longitude', 'altitude', 'accuracy', 'datetime')]),
        ),
    ]
