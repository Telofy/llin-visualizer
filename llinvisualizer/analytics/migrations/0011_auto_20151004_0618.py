# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0010_auto_20151004_0617'),
    ]

    operations = [
        migrations.AlterField(
            model_name='healthareacentroid',
            name='sd_distance',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
