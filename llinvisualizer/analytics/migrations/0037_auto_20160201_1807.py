# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0036_auto_20151114_1959'),
    ]

    operations = [
        migrations.AddField(
            model_name='distribution',
            name='amf_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distributioncampaign',
            name='amf_id',
            field=models.IntegerField(db_index=True, null=True, blank=True),
        ),
    ]
