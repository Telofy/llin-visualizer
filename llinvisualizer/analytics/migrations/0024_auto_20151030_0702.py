# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0023_auto_20151025_1114'),
    ]

    operations = [
        migrations.CreateModel(
            name='DistributionCampaign',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('url', models.CharField(max_length=2048, blank=True)),
                ('description', models.TextField(blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='distribution',
            name='image',
            field=models.CharField(max_length=2048, blank=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='url',
            field=models.CharField(max_length=2048, blank=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='video',
            field=models.CharField(max_length=2048, blank=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='campaign',
            field=models.ForeignKey(related_name='distributions', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.DistributionCampaign', null=True),
        ),
    ]
