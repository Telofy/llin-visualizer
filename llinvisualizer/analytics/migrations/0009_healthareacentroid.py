# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0008_surveyresponse_random'),
    ]

    operations = [
        migrations.CreateModel(
            name='HealthAreaCentroid',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3)),
                ('health_area', models.CharField(max_length=256, blank=True)),
                ('avg_distance', models.FloatField()),
                ('max_distance', models.FloatField()),
                ('sd_distance', models.FloatField()),
                ('count', models.IntegerField()),
            ],
        ),
    ]
