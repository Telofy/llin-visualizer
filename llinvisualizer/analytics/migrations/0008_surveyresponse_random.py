# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import llinvisualizer.analytics.models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0007_auto_20150930_1624'),
    ]

    operations = [
        migrations.AddField(
            model_name='surveyresponse',
            name='random',
            field=models.IntegerField(default=llinvisualizer.analytics.models.maxrandint, db_index=True),
        ),
    ]
