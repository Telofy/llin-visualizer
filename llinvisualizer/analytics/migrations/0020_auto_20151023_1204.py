# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django_countries import countries
from django.db import migrations, models
import django.db.models.deletion


def forwards_func(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    Country = apps.get_model('analytics', 'Country')
    db_alias = schema_editor.connection.alias
    Country.objects.using(db_alias).bulk_create([
        Country(name=name,
                numeric=countries.numeric(code),
                alpha2=countries.alpha2(code),
                alpha3=countries.alpha3(code))
        for code, name in countries
    ])


def reverse_func(apps, schema_editor):
    # forwards_func() creates two Country instances,
    # so reverse_func() should delete them.
    Country = apps.get_model('analytics', 'Country')
    db_alias = schema_editor.connection.alias
    Country.objects.using(db_alias).all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0019_auto_20151021_1343'),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('numeric', models.IntegerField()),
                ('alpha2', models.CharField(max_length=2)),
                ('alpha3', models.CharField(max_length=3)),
            ],
        ),
        migrations.RunPython(forwards_func, reverse_func),
        migrations.RenameField(
            model_name='irtest',
            old_name='country',
            new_name='country_name',
        ),
        migrations.AddField(
            model_name='irtest',
            name='country',
            field=models.ForeignKey(related_name='ir_tests', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Country', null=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='country',
            field=models.ForeignKey(related_name='distributions', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Country', null=True),
        ),
        migrations.AddField(
            model_name='healtharea',
            name='country',
            field=models.ForeignKey(related_name='health_areas', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Country', null=True),
        ),
        migrations.AddField(
            model_name='village',
            name='country',
            field=models.ForeignKey(related_name='villages', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Country', null=True),
        ),
    ]
