# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0034_auto_20151107_1815'),
    ]

    operations = [
        migrations.AlterField(
            model_name='statistic',
            name='aggregate',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
