# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Distribution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=256)),
                ('partner', models.CharField(max_length=256, blank=True)),
                ('region', models.CharField(max_length=256, blank=True)),
                ('start', models.DateTimeField(null=True, blank=True)),
                ('end', models.DateTimeField(null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='surveyresponse',
            name='accuracy',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='age',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='children_under_five',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='datetime',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='day_of_distribution',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='device_id',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='end',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='health_area_district',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='health_area_health_zone',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='health_area_province',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='health_area_type',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='house_number',
            field=models.CharField(max_length=32, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='household',
            field=models.ForeignKey(related_name='responses', blank=True, to='analytics.Household', null=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='instance_id',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='llins',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='llins_brand',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='llins_good',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='llins_installed',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='llins_packaging_returned',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='llins_photo',
            field=models.URLField(max_length=2048),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='llins_returned',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='llins_total',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(srid=4326, dim=3),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='name',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='net_source',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='other_net_source',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='people_in_household',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='people_under_net_last_night',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='phone',
            field=models.CharField(max_length=32, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='phone_id',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='pregnant_women',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='pregnant_women_under_net_last_night',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='sex',
            field=models.CharField(max_length=32, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='sleeping_spaces',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='start',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='summary',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='village',
            field=models.CharField(max_length=256, blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='surveyresponse',
            unique_together=set([]),
        ),
        migrations.AddField(
            model_name='surveyresponse',
            name='distribution',
            field=models.ForeignKey(related_name='responses', default=0, to='analytics.Distribution'),
            preserve_default=False,
        ),
    ]
