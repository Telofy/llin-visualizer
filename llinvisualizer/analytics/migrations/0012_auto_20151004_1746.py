# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0011_auto_20151004_0618'),
    ]

    operations = [
        migrations.CreateModel(
            name='Village',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('geom', django.contrib.gis.db.models.fields.PointField(srid=4326)),
                ('name', models.CharField(max_length=256, blank=True)),
                ('avg_distance', models.FloatField()),
                ('max_distance', models.FloatField()),
                ('sd_distance', models.FloatField(null=True, blank=True)),
                ('count', models.IntegerField()),
            ],
        ),
        migrations.RenameModel(
            old_name='HealthAreaCentroid',
            new_name='HealthArea',
        ),
        migrations.RenameField(
            model_name='healtharea',
            old_name='health_area',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='district',
            new_name='district_name',
        ),
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='health_area',
            new_name='health_area_name',
        ),
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='health_zone',
            new_name='health_zone_name',
        ),
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='province',
            new_name='province_name',
        ),
        migrations.RenameField(
            model_name='surveyresponse',
            old_name='village',
            new_name='village_name',
        ),
        migrations.AddField(
            model_name='village',
            name='health_area',
            field=models.ForeignKey(related_name='villages', to='analytics.HealthArea'),
        ),
    ]
