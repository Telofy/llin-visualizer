# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0028_auto_20151030_1729'),
    ]

    operations = [
        migrations.AlterField(
            model_name='surveyresponse',
            name='distribution',
            field=models.ForeignKey(related_name='responses', on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Distribution', null=True),
        ),
    ]
