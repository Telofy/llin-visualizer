# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0022_auto_20151023_1254'),
    ]

    operations = [
        migrations.RenameField(
            model_name='distribution',
            old_name='count',
            new_name='response_count',
        ),
        migrations.RenameField(
            model_name='healtharea',
            old_name='count',
            new_name='response_count',
        ),
        migrations.RenameField(
            model_name='village',
            old_name='count',
            new_name='response_count',
        ),
        migrations.AddField(
            model_name='distribution',
            name='net_count',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='distribution',
            name='status',
            field=models.CharField(max_length=256, blank=True),
        ),
    ]
