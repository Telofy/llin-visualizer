# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0035_auto_20151107_1816'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cluster',
            name='avg_distance',
        ),
        migrations.RemoveField(
            model_name='cluster',
            name='max_distance',
        ),
        migrations.RemoveField(
            model_name='cluster',
            name='response_count',
        ),
        migrations.RemoveField(
            model_name='cluster',
            name='sd_distance',
        ),
        migrations.RemoveField(
            model_name='distribution',
            name='avg_distance',
        ),
        migrations.RemoveField(
            model_name='distribution',
            name='max_distance',
        ),
        migrations.RemoveField(
            model_name='distribution',
            name='response_count',
        ),
        migrations.RemoveField(
            model_name='distribution',
            name='sd_distance',
        ),
        migrations.AlterField(
            model_name='statistic',
            name='cluster',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Cluster', null=True),
        ),
        migrations.AlterField(
            model_name='statistic',
            name='distribution',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='analytics.Distribution', null=True),
        ),
    ]
