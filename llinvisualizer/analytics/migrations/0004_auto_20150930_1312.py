# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0003_auto_20150923_1513'),
    ]

    operations = [
        migrations.AddField(
            model_name='surveyresponse',
            name='altitude',
            field=models.FloatField(default=0.0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='surveyresponse',
            name='latitude',
            field=models.FloatField(default=0.0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='surveyresponse',
            name='longitude',
            field=models.FloatField(default=0.0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='surveyresponse',
            name='accuracy',
            field=models.FloatField(default=0.0),
            preserve_default=False,
        ),
    ]
