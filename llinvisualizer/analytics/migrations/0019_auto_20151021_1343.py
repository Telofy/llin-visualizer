# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('analytics', '0018_auto_20151021_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='irtest',
            name='insecticide_dosage',
            field=models.CharField(default='', max_length=256, blank=True),
            preserve_default=False,
        ),
    ]
