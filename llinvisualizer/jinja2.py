# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import json
from jinja2 import Environment
from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage
from .analytics.templatetags import analytics_extras

def environment(**options):
    env = Environment(**options)
    env.globals.update({
        'static': staticfiles_storage.url,
        'settings': settings,
    })
    env.filters.update({
        'json': json.dumps,
        'add_class': analytics_extras.add_class,
    })
    return env
