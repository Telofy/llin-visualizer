# -*- encoding: utf-8 -*-
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
# pylint: disable=unused-wildcard-import,wildcard-import
from .settings import *


DEBUG = False
TEMPLATE_DEBUG = DEBUG

# I’ve given up on memcached:
# 1. python-memcached is a djungle of incomprehensible C-style (?) Python code
#    with comments like “silently do not store if value length exceeds maximum.”
#    memcache.SERVER_MAX_VALUE_LENGTH = 8 * 1024 * 1024 fixed one problem, but
#    it still didn’t work. Hopeless to debug.
# 2. pylibmc has this problem: https://github.com/lericson/pylibmc/issues/184.
#    When that is fixed, it’s worth another try.
#
# All caching option I’ve tried are very slow due to pickling or compression,
# so I’m leaving the caching to nginx now.

# For the cache-control header
CACHE_MIDDLEWARE_SECONDS = 60 * 60 * 24 * 30  # one month

# Otherwise the cache-control header is omitted
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}

ALLOWED_HOSTS = ['*']

try:
    from .settings_override import *
except ImportError:
    pass
