LLIN Visualizer
===============

The LLIN Visualizer visualizes net distribution data on a map.

The Against Malaria Foundation has always conducted predistribution registration surveys and postdistribution checkups in conjunction with its distributions of long-lasting insecticide-treated bed nets. Recently, however, they started using a smartphone- and Open Data Kit–based system for these distributions when their local partners agreed to it. Thanks to this new approach to data collection, it is now much easier to glean insights from those data.

The LLIN Visualizer is an extensible software solution for cleaning, aggregating, and visualizing these data so that researchers and donors can understand them at a glance.

A live instance can be found at [nets.claviger.net](http://nets.claviger.net/).

Setup
-----

Most of the fun derives from playing with the PDRS and PDCU data from AMF, but for reasons of data privacy, these data cannot be made public in their unaggregated form. So without them, you’ll probably derive little pleasure from deploying your own instance of the LLIN Visualizer. Nonetheless, here the deployment instructions.

The project is based on Buildout, Django, and Supervisor. I’m running it behind an Nginx reverse proxy, which also serves the static files.

Buildout instructions:

    python2.7 bootstrap.py
    bin/buildout -c production.cfg

Alternatively you can use `development.cfg` in the development environment.

The Django setup is completely standard, so you’ll need to execute:

    bin/django syncdb
    bin/django migrate
    bin/django collectstatic -l

You can then run Supervisor:

    bin/supervisord

If you run into problems, you may have to tweak the settings in `base.cfg`, `supervisord.conf`, or `llinvisualizer/settings.py` for your system.
